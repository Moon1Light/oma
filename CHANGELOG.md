# ????

- Added: Drivers for many IndustrialCraft machines.
- Added: Industrial cases for assembling robots, drones and microcontrollers.
- Added: Solar Panel upgrade can be installed in all devices except the adapter.
- Added: Configurable chatbox command prefix.

# v3.2.0

- Added: 'tell' method for creative chatbox component. It sends a private message to any player.

# v3.1.0

- Added: Furnace Generator.
- Fixed: the use of wands (Portable Hole and Dislocation) on the solar panel blocks caused the mod to crash.

# v3.0.0

- Fixed: The network card does not link to the panel by a right click.

# v3.0.0-alpha.3

- Added: The range parameter has been added to the tesla.attack method.
- Added: Spawn particles when executing tesla.attack.
- Added: The solar panel accepts commands only from a certain address (it is set by the setCommandSource command or use network card on the panel).
- Changed: Textures.
- Changed: Sound of the solar panel servomotor.
- Fixed: Localization.
- Fixed: Component documentation.
- Fixed: Incorrect Waila tooltips for Solar Panel.
- Fixed: Client crash caused by solar panel sounds.

# v3.0.0-alpha.2

- Added: Pet renderer from the OpenComputers can now be customized.
- Added: Robot Scanner
- Added: Tesla Upgrade
- Fixed: Bugs in the World Interface component
- Fixed: Lack of sound of the solar panel servomotor.

# v3.0.0-alpha.1

- Added: Antenna for long distance communications.
- Added: Wifi-controlled solar panel (generates eu-energy).
- Added: Radar (block/upgrade).
- Added: Chat box and creative chat box.
- Added: World interface.
- Added: Using an Advanced Geolyzer you can find slime chunks.

- **BREAKING CHANGE**: items id and modid have been changed

# v2.0.1
* Fixed: Advanced Geolyzer was broken.

# v2.0.0

- Fixed: The unlinked tesseract had no colored name.
- Fixed: The tesseract consumed power after turning off the device
- Added: Recipes for all items.
- Added: Cropnalyzer Upgrade.
- Added: Ic2 crop integration.
- Added: Render for Inventory Upgrades.
- Added: Creative Tab.
- Added: The dimension name in the tesseract tooltip.
- Added: Advanced Geolyzer Upgrade: The longRangeStore method has been added
- Added: Advanced Geolyzer Upgrade: The component inherited all the methods of the standard component.
- Added: Advanced Geolyzer Upgrade: The analyze method returns additional information (biome and light level).
- Changed: The tesseract.getDistance method returns math.huge when the connected inventory is in another dimension
- **BREAKING CHANGE**: Advanced Geolyzer Upgrade: The component has been renamed to 'geolyzer'.
- **BREAKING CHANGE**: Item Charger Upgrade: The component has been renamed to 'item_charger'.
- **BREAKING CHANGE**: Advanced Geolyzer Upgrade: The scan method has been renamed to longRangeAnalyze.
- **BREAKING CHANGE**: Tesseract Upgrade: The component api has been changed.
- **BREAKING CHANGE**: Configuration file format changed (requires manual intervention).

