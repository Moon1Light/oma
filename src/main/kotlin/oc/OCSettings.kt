package oc

import li.cil.oc.api.API
import li.cil.oc.api.Items
import net.minecraft.item.Item

object OCSettings {

    var ratioIndustrialCraft2 = 0.0

    // Items
    lateinit var ocItem: Item
    var wirelessNetworkCard1Item: Int = 0
    var wirelessNetworkCard2Item: Int = 0

    fun load() {
        ratioIndustrialCraft2 = API.config.getDouble("power.value.IndustrialCraft2") / 1000.0
        ocItem = Items.get("wlanCard1").item()
        wirelessNetworkCard1Item = Items.get("wlanCard1").createItemStack(1).itemDamage
        wirelessNetworkCard2Item = Items.get("wlanCard2").createItemStack(1).itemDamage
    }

}