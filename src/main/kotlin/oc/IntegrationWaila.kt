package oc

import mcp.mobius.waila.api.IWailaConfigHandler
import mcp.mobius.waila.api.IWailaDataAccessor
import mcp.mobius.waila.api.IWailaDataProvider
import mcp.mobius.waila.api.IWailaRegistrar
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import oc.block.BlockBound

@Suppress("Unused")
internal object IntegrationWaila {
    @JvmStatic
    fun register(reg: IWailaRegistrar) {
        reg.registerStackProvider(BlockBoundMap, BlockBound::class.java)
    }
}

internal object BlockBoundMap : IWailaDataProvider {

    override fun getWailaStack(accessor: IWailaDataAccessor, config: IWailaConfigHandler): ItemStack {
        val block = BlockBound.getMapping(accessor.metadata) ?: accessor.block
        return ItemStack(block)
    }

    override fun getWailaHead(itemStack: ItemStack, currentTip: MutableList<String>,
                              accessor: IWailaDataAccessor, config: IWailaConfigHandler): MutableList<String> {
        return currentTip
    }

    override fun getWailaBody(itemStack: ItemStack, currentTip: MutableList<String>,
                              accessor: IWailaDataAccessor, config: IWailaConfigHandler): MutableList<String> {
        return currentTip
    }

    override fun getWailaTail(itemStack: ItemStack, currentTip: MutableList<String>,
                              accessor: IWailaDataAccessor, config: IWailaConfigHandler): MutableList<String> {
        return currentTip
    }

    override fun getNBTData(player: EntityPlayerMP, te: TileEntity?, tag: NBTTagCompound?,
                            world: World, x: Int, y: Int, z: Int): NBTTagCompound? {
        return tag
    }

}