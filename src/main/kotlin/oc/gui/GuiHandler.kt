package oc.gui

import cpw.mods.fml.common.network.IGuiHandler
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.world.World
import oc.container.ContainerFurnaceGenerator
import oc.tile.TileFurnaceGenerator

class GuiHandler : IGuiHandler {

    override fun getServerGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): Any? {
        if (ID == 0) {
            val tileEntityFurnaceGenerator = world.getTileEntity(x, y, z) as TileFurnaceGenerator
            return ContainerFurnaceGenerator(player.inventory, tileEntityFurnaceGenerator)
        }
        return null
    }

    override fun getClientGuiElement(ID: Int, player: EntityPlayer, world: World, x: Int, y: Int, z: Int): Any? {
        if (ID == 0) {
            val tileEntityFurnaceGenerator = world.getTileEntity(x, y, z) as TileFurnaceGenerator
            return GuiFurnaceGenerator(player.inventory, tileEntityFurnaceGenerator)
        }
        return null
    }

}