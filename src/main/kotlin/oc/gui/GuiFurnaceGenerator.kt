package oc.gui

import codechicken.lib.gui.GuiDraw
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import li.cil.oc.client.gui.DynamicGuiContainer
import net.minecraft.entity.player.InventoryPlayer
import net.minecraft.util.ResourceLocation
import oc.OMA
import oc.container.ContainerFurnaceGenerator
import oc.tile.TileFurnaceGenerator
import oc.util.I18nUtil.localize
import kotlin.math.ceil

@SideOnly(Side.CLIENT)
class GuiFurnaceGenerator internal constructor(
        playerInventory: InventoryPlayer, tileFurnaceGenerator: TileFurnaceGenerator)
    : DynamicGuiContainer<ContainerFurnaceGenerator>(ContainerFurnaceGenerator(playerInventory, tileFurnaceGenerator)) {

    override fun drawSecondaryForegroundLayer(mouseX: Int, mouseY: Int) {
        super.drawSecondaryForegroundLayer(mouseX, mouseY)
        fontRendererObj.drawString(localize("tile.furnace_generator.name"), 8, 6, 0x404040)
    }

    override fun drawSecondaryBackgroundLayer() {
        super.drawSecondaryBackgroundLayer()
        GuiDraw.changeTexture(generatorFireTextures)
        val k = (width - xSize) / 2
        val l = (height - ySize) / 2
        drawTexturedModalRect(k + 80, l + 35 - 17, 16, 0, 14, 14)
        inventoryContainer()?.let { inventoryContainer ->
            if (inventoryContainer.isBurning) {
                val offset = ceil(inventoryContainer.remainingTicks * 12.0 / inventoryContainer.currentItemBurnTime).toInt()
                drawTexturedModalRect(k + 80, l + 35 - 17 + 12 - offset, 0, 12 - offset, 14, offset + 2)
            }
        }

    }

    companion object {
        private val generatorFireTextures = ResourceLocation(OMA.MOD_ID + ":textures/gui/generator-fire.png")
    }
}