package oc.face

import oc.event.TileStatusEvent

interface IEventReceiver {

  fun receiveEvent(event: TileStatusEvent)

}
