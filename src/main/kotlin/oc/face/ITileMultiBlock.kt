package oc.face

interface ITileMultiBlock {
    fun onPlace()
    fun onBreak()
}