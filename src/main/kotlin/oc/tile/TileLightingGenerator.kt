package oc.tile

import ic2.api.energy.EnergyNet
import ic2.api.energy.tile.IEnergySource
import ic2.api.item.IC2Items
import net.minecraft.block.Block
import net.minecraft.entity.effect.EntityLightningBolt
import net.minecraft.tileentity.TileEntity
import net.minecraftforge.common.util.ForgeDirection
import kotlin.math.min

class TileLightingGenerator: BaseTileWithIC2(), IEnergySource {

    private var storage: Double = 0.0
    private var isTowerComplete = true

    private val ironFence by lazy {
        Block.getBlockFromItem(IC2Items.getItem("ironFence").item)
    }

    override fun updateEntity() {
        super.updateEntity()

        if (worldObj.isRaining && worldObj.isThundering && isTowerComplete) {
            var rodSize = 0

            for (y in (yCoord + 1)..worldObj.height) {
                if (worldObj.getBlock(xCoord, y, zCoord) == ironFence) {
                    rodSize++
                } else
                    break
            }

            val chance = (yCoord + rodSize) / 256.0

            if (worldObj.totalWorldTime % (20 * 10) == 0L && worldObj.rand.nextDouble() < chance) {
                worldObj.addWeatherEffect(EntityLightningBolt(worldObj, xCoord.toDouble(),
                        yCoord.toDouble() + rodSize + 1,
                        zCoord.toDouble()))
                storage += 100000 + worldObj.rand.nextInt(100000)
            }
        }
    }

    override fun emitsEnergyTo(p0: TileEntity?, p1: ForgeDirection?): Boolean {
        return true
    }

    override fun getSourceTier(): Int {
        return 3
    }

    override fun getOfferedEnergy(): Double {
        return min(storage, EnergyNet.instance.getPowerFromTier(this.sourceTier))
    }

    override fun drawEnergy(p0: Double) {
        storage -= p0
    }

}