package oc.tile

import li.cil.oc.api.Network
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.Connector
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLiving
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.Packet
import net.minecraft.network.play.server.S35PacketUpdateTileEntity
import oc.settings
import oc.util.RadarUtils
import kotlin.math.abs

class TileRadar : BaseTileWithEnvironment() {

    override val node: Node? =
        Network.newNode(this, Visibility.Network).withComponent("radar").withConnector().create()

    var yaw = 0f
        private set
    private var target = 0f

    override fun updateEntity() {
        super.updateEntity()
        if (yaw != target) {
            val t = yaw - target
            if (t < 0) {
                if (yaw + ROTATION_SPEED >= target) yaw = target else yaw += ROTATION_SPEED
            } else {
                if (yaw - ROTATION_SPEED <= target) yaw = target else yaw -= ROTATION_SPEED
            }
            worldObj.markBlockForUpdate(xCoord, yCoord, zCoord)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([yaw:number]):boolean -- Set yaw.", direct = true)
    fun setYaw(machine: Context?, args: Arguments): Array<Any>? {
        target = (-args.checkDouble(0)).toFloat() % 360.0f
        if (target < 0.0f) target += 360.0f
        val t = yaw - target
        if (abs(t) > 180) {
            if (t < 0) {
                yaw += 360f
            } else if (t > 0) {
                yaw -= 360f
            }
        }
        return null
    }

    private fun getEntities(context: Context, args: Arguments, eClass: Class<out Entity?>?): Array<Any?> {
        val distance = args.optInteger(0, settings.blockRadar.maxRange)
            .coerceAtMost(settings.blockRadar.maxRange).coerceAtLeast(1)
        val height = args.optInteger(1, settings.blockRadar.maxHeight)
            .coerceAtMost(settings.blockRadar.maxHeight).coerceAtLeast(1)
        val angle = args.optDouble(2, settings.blockRadar.maxAngle.toDouble())
            .coerceAtMost(settings.blockRadar.maxAngle.toDouble()).coerceAtLeast(0.0)
        if ((node() as Connector).tryChangeBuffer(-settings.blockRadar.scanEnergy * distance)) {
            val entities = RadarUtils.getEntities(
                worldObj,
                xCoord.toDouble() + 0.5,
                yCoord.toDouble() + 0.5,
                zCoord.toDouble() + 0.5,
                distance.toDouble(),
                height.toDouble(),
                Math.toRadians(yaw + 180.toDouble()),
                Math.toRadians(angle),
                eClass
            )
            context.pause(settings.blockRadar.delay)
            return arrayOf(entities.toTypedArray())
        }
        return arrayOf(null, "not enough energy")
    }

    @Callback(
        doc = "function([distance:number][,height:number[,[angle:number]]]):table -- Returns a list of all entities " +
                "(players and mobs) detected within the specified or the maximum range.",
        direct = true
    )
    fun getEntities(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, EntityLivingBase::class.java)
    }

    @Callback(
        doc = "function([distance:number][,height:number[,[angle:number]]]):table -- Returns a list of all players " +
                "detected within the specified or the maximum range.",
        direct = true
    )
    fun getPlayers(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, EntityPlayer::class.java)
    }

    @Callback(
        doc = "function([distance:number][,height:number[,[angle:number]]]):table -- Returns a list of all mobs " +
                "detected within the specified or the maximum range.",
        direct = true
    )
    fun getMobs(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, EntityLiving::class.java)
    }

    @Callback(
        doc = "function([distance:number][,height:number[,[angle:number]]]):table -- Returns a list of all items " +
                "detected within the specified or the maximum range.",
        direct = true
    )
    fun getItems(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, EntityItem::class.java)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number", direct = true)
    fun getYaw(context: Context?, args: Arguments?): Array<Any> {
        return arrayOf(yaw)
    }

    val facing: Int
        get() = if (worldObj == null) 0 else getBlockMetadata()

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        yaw = nbt.getFloat("yaw")
        target = nbt.getFloat("target")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setFloat("yaw", yaw)
        nbt.setFloat("target", target)
    }

    override fun getDescriptionPacket(): Packet {
        val tagCompound = NBTTagCompound()
        tagCompound.setFloat("yaw", yaw)
        tagCompound.setFloat("target", target)
        return S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 3, tagCompound)
    }

    override fun onDataPacket(net: NetworkManager, pkt: S35PacketUpdateTileEntity) {
        val tagCompound = pkt.func_148857_g()
        yaw = tagCompound.getFloat("yaw")
        target = tagCompound.getFloat("target")
    }

}

private const val ROTATION_SPEED = 3.0f