package oc.tile

import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent
import li.cil.oc.api.Network
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.server.MinecraftServer
import net.minecraftforge.common.ForgeHooks
import net.minecraftforge.event.ServerChatEvent
import net.minecraftforge.event.entity.living.LivingDeathEvent
import oc.OMA
import oc.event.TileStatusEvent
import oc.face.IEventReceiver
import oc.settings
import org.apache.commons.lang3.text.StrSubstitutor

open class TileChatbox : BaseTileWithEnvironment(), IEventReceiver {

    @Suppress("LeakingThis")
    override val node: Node? = Network.newNode(this, Visibility.Network).withComponent("chatbox").create()

    private var currentRadius: Int = settings.chatbox.maxMessageRadius
        set(value) {
            field = value.coerceAtMost(settings.chatbox.maxMessageRadius).coerceAtLeast(0)
        }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(message:string): boolean -- Says some text.")
    fun say(context: Context, arguments: Arguments): Array<Any?>? {
        var message = arguments.checkString(0)
        if (message.length > settings.chatbox.maxMessageLength)
            message = message.substring(0, settings.chatbox.maxMessageLength)
        if (settings.chatbox.isLogging)
            OMA.info("say in: [{}, {}, {}]", xCoord, yCoord, zCoord)

        worldObj.playerEntities.forEach {
            val player = it as EntityPlayer
            if (canHandle(player)) {
                player.addChatMessage(
                    ForgeHooks.newChatWithLinks(
                        StrSubstitutor(mapOf("message" to message)).replace(settings.chatbox.messagePattern)
                    )
                )
            }
        }
        return null
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(radius:number):boolean -- Sets \"say\" radius.")
    fun setRadius(context: Context, arguments: Arguments): Array<Any?>? {
        currentRadius = arguments.checkInteger(0)
        return null
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns \"say\" radius.")
    fun getRadius(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(currentRadius)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns \"say\" max radius.")
    fun getMaxRadius(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(settings.chatbox.maxMessageRadius)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns true if it is creative.")
    open fun isCreative(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(false)
    }

    protected open fun canHandle(source: Entity): Boolean =
        source.getDistance(xCoord.toDouble(), yCoord.toDouble(), zCoord.toDouble()) < currentRadius

    private fun handleMessage(event: ServerChatEvent) {
        if (canHandle(event.player)) {
            node?.sendToReachable("computer.signal", "chat_message", event.player.displayName, event.message)
        }
    }

    private fun handleCommand(event: ServerChatEvent) {
        if (canHandle(event.player)) {
            node?.sendToReachable("computer.signal", "chat_command", event.player.displayName, event.message)
        }
    }

    private fun handleDeath(event: LivingDeathEvent) {
        val entity = event.entityLiving
        if (entity is EntityPlayer && canHandle(event.entityLiving)) {
            node?.sendToReachable("computer.signal", "player_death", entity.displayName, event.source.damageType)
        }
    }

    private fun handleLoggedIn(event: PlayerLoggedInEvent) {
        val player = event.player
        if (canHandle(player)) {
            node?.sendToReachable(
                "computer.signal", "player_join", player.worldObj.provider.dimensionId,
                player.posX, player.posY, player.posZ,
                getDistanceFrom(player.posX, player.posY, player.posZ), player.displayName
            )
        }
    }

    private fun handleLoggedOut(event: PlayerLoggedOutEvent) {
        val player = event.player
        if (canHandle(player)) {
            node?.sendToReachable(
                "computer.signal", "player_quit", player.worldObj.provider.dimensionId,
                player.posX, player.posY, player.posZ,
                getDistanceFrom(player.posX, player.posY, player.posZ), player.displayName
            )
        }
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        currentRadius = nbt.getInteger("radius")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setInteger("radius", currentRadius)
    }

    override fun receiveEvent(event: TileStatusEvent) {
        when (event) {
            TileStatusEvent.Validate -> CHATBOXES.add(this)
            TileStatusEvent.Invalidate, TileStatusEvent.Unload -> CHATBOXES.remove(this)
        }
    }

    internal companion object {

        val CHATBOXES: MutableSet<TileChatbox> = mutableSetOf()

        fun init() {
            CHATBOXES.clear()
        }

        fun cleanup() {
            CHATBOXES.clear()
        }

        fun handleEventMessage(event: ServerChatEvent) {
            if (event.message.startsWith(settings.chatbox.commandPrefix)) {
                CHATBOXES.forEach { chatBox -> chatBox.handleCommand(event) }
                event.isCanceled = true
            } else {
                CHATBOXES.forEach { chatBox -> chatBox.handleMessage(event) }
            }
        }

        fun handleEventPlayerDeath(event: LivingDeathEvent) {
            CHATBOXES.forEach { chatBox -> chatBox.handleDeath(event) }
        }

        fun handlePlayerLoggedIn(event: PlayerLoggedInEvent) {
            CHATBOXES.forEach { chatBox -> chatBox.handleLoggedIn(event) }
        }

        fun handlePlayerLoggedOut(event: PlayerLoggedOutEvent) {
            CHATBOXES.forEach { chatBox -> chatBox.handleLoggedOut(event) }
        }
    }

}

class TileCreativeChatbox : TileChatbox() {

    override fun canHandle(source: Entity): Boolean {
        return true
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns true if it is creative.")
    override fun isCreative(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(true)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(name:string, message:string):boolean -- Send a private message to one player.")
    fun tell(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val message = arguments.checkString(1)
        MinecraftServer.getServer().configurationManager.playerEntityList.forEach { player: Any? ->
            if (player is EntityPlayer) {
                if (player.displayName == name) {
                    player.addChatComponentMessage(ForgeHooks.newChatWithLinks(message))
                    return arrayOf(true)
                }
            }
        }

        return arrayOf(false)
    }

}