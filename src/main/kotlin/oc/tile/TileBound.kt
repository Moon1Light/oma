package oc.tile

import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.Vec3
import oc.face.ITileMultiBlock

class TileBound : TileEntity {

    var pos: Vec3

    constructor() {
        pos = Vec3.createVectorHelper(0.0, 0.0, 0.0)
    }

    constructor(pos: Vec3) {
        this.pos = pos
    }

    fun setRootTile(x: Int, y: Int, z: Int) {
        pos.xCoord = x.toDouble()
        pos.yCoord = y.toDouble()
        pos.zCoord = z.toDouble()
    }

    fun onBreak() {
        val tileMultiBlock = worldObj.getTileEntity(pos.xCoord.toInt(), pos.yCoord.toInt(), pos.zCoord.toInt()) as ITileMultiBlock
        tileMultiBlock.onBreak()
    }

    override fun readFromNBT(tagCompound: NBTTagCompound) {
        super.readFromNBT(tagCompound)
        pos.xCoord = tagCompound.getInteger("xCoord").toDouble()
        pos.yCoord = tagCompound.getInteger("yCoord").toDouble()
        pos.zCoord = tagCompound.getInteger("zCoord").toDouble()
    }

    override fun writeToNBT(tagCompound: NBTTagCompound) {
        super.writeToNBT(tagCompound)
        tagCompound.setInteger("xCoord", pos.xCoord.toInt())
        tagCompound.setInteger("yCoord", pos.yCoord.toInt())
        tagCompound.setInteger("zCoord", pos.zCoord.toInt())
    }

}