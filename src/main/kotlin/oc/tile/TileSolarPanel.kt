package oc.tile

import ic2.api.energy.EnergyNet
import ic2.api.energy.event.EnergyTileLoadEvent
import ic2.api.energy.tile.IEnergySource
import ic2.api.energy.tile.IEnergyTile
import ic2.core.IC2
import li.cil.oc.api.Network
import li.cil.oc.api.network.SidedComponent
import li.cil.oc.api.network.WirelessEndpoint
import net.minecraft.client.Minecraft
import net.minecraft.init.Blocks
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.Packet
import net.minecraft.network.play.server.S35PacketUpdateTileEntity
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.MathHelper
import net.minecraft.world.EnumSkyBlock
import net.minecraft.world.World
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.common.util.ForgeDirection
import oc.Resources
import oc.block.BlockBound
import oc.face.ITileMultiBlock
import oc.settings
import oc.sound.TileSolarPanelSound
import oc.util.BlockPos
import oc.util.BoundUtils
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.math.sqrt

class TileSolarPanel : TileEntity(), SidedComponent, IEnergyTile, IEnergySource, ITileMultiBlock, WirelessEndpoint {

    private val energyPerTick = settings.blockSolarPanel.energyPerTick.toDouble()
    private var usedPort = settings.blockSolarPanel.defaultPort
    private var addToEnergyNetwork = false
    var isRotate = false
        private set
    private var sound: TileSolarPanelSound? = null
    var pitch = 0f
        private set
    private var target = 0f
    private var commandSource: String = ""

    override fun updateEntity() {
        if (!addToEnergyNetwork && !worldObj.isRemote) {
            MinecraftForge.EVENT_BUS.post(EnergyTileLoadEvent(this))
            addToEnergyNetwork = true
        }

        if (abs(target - pitch) <= .1f) {
            isRotate = false
            pitch = target
        }

        if (isRotate) {
            if (target < pitch) pitch -= .1f else if (target > pitch) pitch += .1f
        }

        if (worldObj.isRemote) {
            updateSound()
        }
    }

    override fun getDescriptionPacket(): Packet {
        val tagCompound = NBTTagCompound()
        writeToNBT(tagCompound)
        return S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 0, tagCompound)
    }

    override fun onDataPacket(net: NetworkManager, pkt: S35PacketUpdateTileEntity) {
        readFromNBT(pkt.func_148857_g())
    }

    private fun updateSound() {
        if (sound != null) {
            if (Minecraft.getMinecraft().soundHandler.isSoundPlaying(sound)) return
            sound = null
        }
        if (isRotate) {
            sound = TileSolarPanelSound(Resources.servomotorSound, this)
            Minecraft.getMinecraft().soundHandler.playSound(sound)
        }
    }

    override fun onPlace() {
        BoundUtils.createBoundBox(worldObj, AxisAlignedBB.getBoundingBox(-1.0, 0.0, -1.0,
                1.0, 1.0, 1.0),
                BlockPos(xCoord, yCoord, zCoord, null),
                BlockBound.getMapping(oc.block.Blocks.solarPanel!!))
    }

    override fun onBreak() {
        for (y in 0..1) {
            for (x in -1..1) {
                for (z in -1..1) {
                    val xPos = x + xCoord
                    val yPos = y + yCoord
                    val zPos = z + zCoord
                    worldObj.setTileEntity(xPos, yPos, zPos, null)
                    worldObj.setBlock(xPos, yPos, zPos, Blocks.air)
                }
            }
        }
    }

    override fun canUpdate(): Boolean {
        return true
    }

    override fun getRenderBoundingBox(): AxisAlignedBB {
        return INFINITE_EXTENT_AABB
    }

    override fun canConnectNode(side: ForgeDirection): Boolean {
        return side == ForgeDirection.DOWN
    }

    override fun invalidate() {
        if (!worldObj.isRemote) Network.leaveWirelessNetwork(this)
        super.invalidate()
    }

    override fun validate() {
        super.validate()
        Network.joinWirelessNetwork(this)
    }

    override fun onChunkUnload() {
        if (IC2.platform.isSimulating) {
            MinecraftForge.EVENT_BUS.post(EnergyTileLoadEvent(this))
            addToEnergyNetwork = true
        }
        if (!worldObj.isRemote) Network.leaveWirelessNetwork(this)
        super.onChunkUnload()
    }

    private fun getSunAngle(): Double {
        val celestialValue = worldObj.getCelestialAngle(1.0f).toDouble()
        return if (celestialValue > 0.5) {
            1 - celestialValue
        } else -celestialValue
    }

    private fun canSeeTheSky(): Boolean {
        for (x in -1..1)
            for (z in -1..1) {
                if (!worldObj.canBlockSeeTheSky(xCoord + x, yCoord + 2, zCoord + z)) {
                    return false
                }
            }
        return true
    }

    private fun getEfficiency(): Double {
        if (worldObj.provider.hasNoSky || worldObj.isThundering
                || !canSeeTheSky()) {
            return 0.0
        }
        val sunAngle = getSunAngle()
        if (abs(sunAngle) > 0.25) {
            return 0.0
        }
        val angle = abs(sunAngle - pitch * .00277777777777777777) // 0.25/90
        return max(if (angle > 0.25 || worldObj.isRaining) 0.0 else 1 - angle * 3, 0.25)
    }

    private fun getLightLevel(): Double {
        var f = worldObj.getCelestialAngleRadians(1.0f)
        f += if (f < Math.PI.toFloat()) {
            (0.0f - f) * 0.2f
        } else {
            (Math.PI.toFloat() * 2f - f) * 0.2f
        }
        val skyLight =
                worldObj.getSavedLightValue(EnumSkyBlock.Sky, xCoord, yCoord, zCoord) - worldObj.skylightSubtracted
        val lightLevel = (skyLight.toFloat() * MathHelper.cos(f)).roundToInt().coerceAtLeast(0).
        coerceAtMost(15)
        return lightLevel * lightLevel / 225.0
    }

    private fun getPower(): Double {
        return energyPerTick * getLightLevel()
    }

    override fun getOfferedEnergy(): Double {
        return getPower() * getEfficiency()
    }

    override fun drawEnergy(v: Double) {}

    override fun getSourceTier(): Int {
        return EnergyNet.instance.getTierFromPower(energyPerTick)
    }

    override fun emitsEnergyTo(tileEntity: TileEntity, forgeDirection: ForgeDirection): Boolean {
        return ForgeDirection.DOWN == forgeDirection
    }

    override fun x(): Int {
        return xCoord
    }

    override fun y(): Int {
        return yCoord
    }

    override fun z(): Int {
        return zCoord
    }

    override fun world(): World {
        return worldObj
    }

    fun setSourceAddress(netAddress: String) {
        commandSource = netAddress
    }

    fun getSourceAddress(): String {
        return commandSource
    }

    fun hasNetworkAddress() = commandSource.isNotEmpty()

    override fun receivePacket(packet: li.cil.oc.api.network.Packet, sender: WirelessEndpoint) {
        if (!worldObj.isRemote) if (packet.port() == usedPort) {
            if (packet.data().isEmpty()) {
                return
            }
            val cmd = String(packet.data()[0] as? ByteArray ?: return)
            if (commandSource == ""
                    && (cmd != "setCommandSource"
                            || sqrt(getDistanceFrom(sender.x() + 0.5, sender.y() + 0.5,
                            sender.z() + 0.5)) > 3.0)
                    || commandSource != packet.source()) {
                return
            }
            when (cmd) {
                "setCommandSource" -> if (packet.data().size > 1) {
                    commandSource = String(packet.data()[1] as? ByteArray ?: return)
                }
                "setPort" -> if (packet.data().size > 1) {
                    val port = packet.data()[1] as? Int ?: return
                    if (port >= 0) usedPort = port and 0xffff
                }
                "getOfferedEnergy" -> Network.sendWirelessPacket(this, 256.0,
                        li.cil.oc.server.network.Network.Packet(
                        null, packet.source(), usedPort, arrayOf(offeredEnergy), 60))

                "setPitch" -> if (packet.data().size > 1) {
                    val tmpTarget = packet.data()[1] as? Double ?: return
                    if (abs(tmpTarget) <= settings.blockSolarPanel.maxPitch) {
                        worldObj.markBlockForUpdate(xCoord, yCoord, zCoord)
                        target = tmpTarget.toFloat()
                        isRotate = true
                    }
                }
            }
        }
    }

    override fun readFromNBT(tagCompound: NBTTagCompound) {
        super.readFromNBT(tagCompound)
        pitch = tagCompound.getFloat("pitch")
        target = tagCompound.getFloat("target")
        isRotate = tagCompound.getBoolean("isRotate")
        usedPort = tagCompound.getInteger("usedPort")
        commandSource = tagCompound.getString("commandSource")
    }

    override fun writeToNBT(tagCompound: NBTTagCompound) {
        super.writeToNBT(tagCompound)
        tagCompound.setFloat("pitch", pitch)
        tagCompound.setFloat("target", target)
        tagCompound.setBoolean("isRotate", isRotate)
        tagCompound.setInteger("usedPort", usedPort)
        tagCompound.setString("commandSource", commandSource)
    }

}