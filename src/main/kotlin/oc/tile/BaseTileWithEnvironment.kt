package oc.tile

import li.cil.oc.api.Network
import li.cil.oc.api.network.Analyzable
import li.cil.oc.api.network.Environment
import li.cil.oc.api.network.Message
import li.cil.oc.api.network.Node
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import oc.event.TileStatusEvent
import oc.face.IEventReceiver

abstract class BaseTileWithEnvironment : TileEntity(), Environment, Analyzable {

    private var addToNetwork = false

    abstract val node: Node?
    final override fun node(): Node? = node
    override fun onConnect(node: Node) {}
    override fun onDisconnect(node: Node) {}
    override fun onMessage(message: Message) {}

    override fun onAnalyze(player: EntityPlayer, side: Int, hitX: Float, hitY: Float, hitZ: Float): Array<Node> {
        return arrayOf(node!!)
    }

    override fun updateEntity() {
        if (!addToNetwork) {
            addToNetwork = true
            Network.joinOrCreateNetwork(this)
        }
    }

    override fun validate() {
        super.validate()
        if (this is IEventReceiver) {
            this.receiveEvent(TileStatusEvent.Validate)
        }
    }

    override fun invalidate() {
        super.invalidate()
        if (this is IEventReceiver) {
            this.receiveEvent(TileStatusEvent.Invalidate)
        }
        node?.remove()
    }

    override fun onChunkUnload() {
        super.onChunkUnload()
        if (this is IEventReceiver) {
            this.receiveEvent(TileStatusEvent.Unload)
        }
        node?.remove()
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        node?.let {
            if (node?.host() == this) {
                node?.load(nbt.getCompoundTag("oc:node"))
            }
        }
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        node?.let {
            if (node?.host() == this) {
                val nodeNbt = NBTTagCompound()
                node?.save(nodeNbt)
                nbt.setTag("oc:node", nodeNbt)
            }
        }
    }

}