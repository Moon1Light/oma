package oc.tile

import ic2.api.energy.event.EnergyTileLoadEvent
import ic2.api.energy.tile.IEnergyTile
import ic2.core.IC2
import net.minecraft.tileentity.TileEntity
import net.minecraftforge.common.MinecraftForge

open class BaseTileWithIC2 : TileEntity(), IEnergyTile {

    private var addToEnergyNetwork = false

    override fun updateEntity() {
        if (!addToEnergyNetwork && !worldObj.isRemote) {
            MinecraftForge.EVENT_BUS.post(EnergyTileLoadEvent(this))
            addToEnergyNetwork = true
        }
    }

    override fun onChunkUnload() {
        if (IC2.platform.isSimulating) {
            MinecraftForge.EVENT_BUS.post(EnergyTileLoadEvent(this))
            addToEnergyNetwork = true
        }
        super.onChunkUnload()
    }

}