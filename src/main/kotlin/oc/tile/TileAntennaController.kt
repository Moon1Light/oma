package oc.tile

import li.cil.oc.api.Network
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.effect.EntityLightningBolt
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.Packet
import net.minecraft.network.play.server.S35PacketUpdateTileEntity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World
import oc.block.BlockAntenna
import oc.block.BlockAntennaCell
import oc.block.BlockAntennaController
import oc.settings
import oc.util.BlockNotifyFlags
import oc.util.Utils
import oc.util.getDimensionId
import kotlin.math.floor
import kotlin.random.Random

class TileAntennaController : BaseTileWithEnvironment() {

    override val node: Node? =
        Network.newNode(this, Visibility.Network).withConnector().withComponent("antenna").create()

    private val msgQueue = mutableSetOf<AntennaMessage>()
    private val openChannels = mutableListOf<Short>()
    private var registered = false
    var isTowerComplete = false
        private set

    override fun updateEntity() {
        super.updateEntity()
        if (!worldObj.isRemote) {
            if (isTowerComplete) {
                if (!registered) {
                    registerAntenna(this)
                    registered = true
                }
                msgQueue.forEach {
                    it.timeCount--
                    if (it.timeCount <= 0) {
                        receiveMessage(it)
                    }
                }
                msgQueue.removeIf { it.timeCount <= 0 }
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback
    fun open(context: Context, args: Arguments): Array<Any> {
        val channel = args.checkInteger(0)
        if (channel.toShort() !in openChannels && openChannels.size < 16) {
            openChannels.add(channel.toShort())
            return arrayOf(true)
        }
        return arrayOf(false)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback
    fun close(context: Context, args: Arguments): Array<Any> {
        val channel = args.checkInteger(0)
        if (channel.toShort() in openChannels) {
            openChannels.remove(channel.toShort())
            return arrayOf(true)
        }
        return arrayOf(false)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback
    fun broadcast(context: Context, args: Arguments): Array<Any> {
        val channel = args.checkInteger(0)
        val msg = args.toMutableList()
        msg.removeAt(0)

        node?.let {
            sendMessage(this, channel.toShort(), msg.toTypedArray())
            //LOL :D
            if (false && worldObj.isRaining && Random(System.currentTimeMillis()).nextBoolean()) {
                worldObj.addWeatherEffect(
                    EntityLightningBolt(
                        worldObj, xCoord.toDouble(),
                        yCoord.toDouble() + 15, zCoord.toDouble()
                    )
                )
            }

            context.pause(0.5)
            return arrayOf(true)
        }

        return arrayOf(false)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback
    fun getMessageSpeed(context: Context, args: Arguments): Array<Any> {
        return arrayOf(AntennaMessage.MESSAGE_SPEED)
    }

    fun checkChannel(channel: Short): Boolean {
        return channel in openChannels
    }

    fun receiveMessage(msg: AntennaMessage) {
        node?.sendToReachable("computer.signal", "antenna_message", msg.sender, msg.distanceTo, *msg.msg)
    }

    fun addMessage(msg: AntennaMessage) {
        msgQueue.add(msg)
    }

    fun getTransmitDistance(): Float {
        return settings.antenna.minDistance + (yCoord * 15 * if (worldObj.isRaining)
            (Random(System.currentTimeMillis()).nextFloat() - 0.5F) + 0.5F else 1F)
    }

    override fun getMaxRenderDistanceSquared(): Double {
        return 65536.0
    }

    override fun getRenderBoundingBox(): AxisAlignedBB {
        return INFINITE_EXTENT_AABB
    }

    override fun getDescriptionPacket(): Packet {
        return S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 0, null)
    }

    override fun onDataPacket(net: NetworkManager, packet: S35PacketUpdateTileEntity) {
        super.onDataPacket(net, packet)
        checkStructure()
    }

    fun onBlockAdded() {
        isTowerComplete = false
        registered = false
        checkStructure()
    }

    fun onBlockRemoved() {
        for (y in 1..15) {
            if (worldObj.getBlock(xCoord, yCoord + y, zCoord) is BlockAntenna) {
                worldObj.setBlockMetadataWithNotify(xCoord, yCoord + y, zCoord, 0, BlockNotifyFlags.ALL)
            }
        }

        worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, BlockNotifyFlags.ALL)

        unRegisterAntenna(this)
        isTowerComplete = false
    }

    private fun checkStructure() {
        for (y in 1..12) {
            val block = worldObj.getBlock(xCoord, yCoord + y, zCoord)
            if (block !is BlockAntenna || block is BlockAntennaCell ||
                block is BlockAntennaController
            ) {
                return
            }
        }
        if (worldObj.getBlock(xCoord, yCoord + 14, zCoord) !is BlockAntennaCell) {
            return
        }
        if (worldObj.getBlock(xCoord, yCoord + 15, zCoord) !is BlockAntennaCell) {
            return
        }

        for (y in 1..15) {
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord + y, zCoord, 1, BlockNotifyFlags.ALL)
        }

        worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, BlockNotifyFlags.ALL)
        isTowerComplete = true
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        isTowerComplete = nbt.getBoolean("isTowerComplete")
        nbt.getIntArray("openChannels").forEach {
            openChannels.add(it.toShort())
        }
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        nbt.setBoolean("isTowerComplete", isTowerComplete)
        nbt.setIntArray("openChannels", openChannels.map { it.toInt() }.toIntArray())
    }

    companion object {

        val dimensions = HashMap<Int, Dimension>()

        fun getOrCreateDimension(world: World): Dimension {
            return if (world.getDimensionId() in dimensions) {
                dimensions[world.getDimensionId()]!!
            } else {
                val dimension = Dimension()
                dimensions[world.getDimensionId()] = dimension
                dimension
            }
        }

        fun sendMessage(sender: TileAntennaController, channel: Short, msg: Array<Any>) {
            val world = sender.worldObj
            getOrCreateDimension(world).antennas.filter { it.checkChannel(channel) }.forEach {
                val distanceTo = Utils.distance(sender.node!!, it.node!!).toFloat()
                if (distanceTo <= sender.getTransmitDistance() && it != sender) {
                    it.addMessage(
                        AntennaMessage(
                            sender.node.address(), distanceTo,
                            floor(distanceTo / AntennaMessage.MESSAGE_SPEED).toInt(), msg
                        )
                    )
                }
            }
        }

        fun registerAntenna(tile: TileAntennaController) {
            if (tile.worldObj.getDimensionId() in dimensions) {
                getOrCreateDimension(tile.worldObj).registerAntenna(tile)
            } else {
                getOrCreateDimension(tile.worldObj).registerAntenna(tile)
            }
        }

        fun unRegisterAntenna(tile: TileAntennaController) {
            if (tile.worldObj.getDimensionId() in dimensions) {
                dimensions[tile.worldObj.getDimensionId()]?.unRegisterAntenna(tile)
            }
        }

    }

    class Dimension {

        val antennas = mutableSetOf<TileAntennaController>()

        fun registerAntenna(tile: TileAntennaController) {
            antennas.add(tile)
        }

        fun unRegisterAntenna(tile: TileAntennaController) {
            antennas.remove(tile)
        }

    }

    class AntennaMessage(val sender: String, val distanceTo: Float, var timeCount: Int, val msg: Array<Any>) {
        companion object {
            val MESSAGE_SPEED: Float by lazy {
                settings.antenna.messageSpeed
            }
        }
    }

}