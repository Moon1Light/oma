package oc.tile

import codechicken.lib.inventory.InventoryUtils
import codechicken.lib.vec.Vector3
import li.cil.oc.api.Network
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.Connector
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntityFurnace
import net.minecraft.world.EnumSkyBlock
import oc.settings
import oc.util.BlockNotifyFlags

class TileFurnaceGenerator : BaseTileWithEnvironment(), IInventory {

    override val node: Node? =
        Network.newNode(this, Visibility.Network).withComponent("generator").withConnector().create()

    private var fuelStack: ItemStack? = null
    var remainingTicks = 0
        private set
    var currentItemBurnTime = 0
        private set
    private val efficiency: Double = settings.furnaceGenerator.efficiency

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns whether the generator is currently active.")
    fun isBurning(context: Context?, args: Arguments?): Array<Any> {
        return arrayOf(isBurning)
    }

    val isBurning: Boolean
        get() = remainingTicks > 0

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- The number of ticks that the generator will keep burning from the last consumed fuel.")
    fun getBurnTime(context: Context?, args: Arguments?): Array<Any> {
        return arrayOf(currentItemBurnTime - remainingTicks)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- The number of ticks that the currently burning fuel lasts in total.")
    fun getCurrentItemBurnTime(context: Context?, args: Arguments?): Array<Any> {
        return arrayOf(currentItemBurnTime)
    }

    override fun canUpdate(): Boolean {
        return true
    }

    override fun updateEntity() {
        super.updateEntity()
        if (worldObj.isRemote) {
            return
        }
        var markDirty = false
        val wasBurning = isBurning
        if (isBurning) {
            remainingTicks--
        }
        if (!isBurning) {
            currentItemBurnTime = 0
            fuelStack?.let {
                currentItemBurnTime = TileEntityFurnace.getItemBurnTime(it)
                if (currentItemBurnTime > 0) {
                    markDirty = true
                    remainingTicks = currentItemBurnTime
                    it.stackSize -= 1
                    if (it.stackSize <= 0) {
                        fuelStack = if (it.item.hasContainerItem(it)) {
                            it.item.getContainerItem(it)
                        } else {
                            null
                        }
                    }
                }
            }
            if (wasBurning != isBurning) {
                markDirty = true
                updateBlockMetadata()
                worldObj.updateLightByType(EnumSkyBlock.Block, xCoord, yCoord, zCoord)
            }
        }
        if (isBurning) {
            (node as Connector).changeBuffer(efficiency)
        }
        if (markDirty) {
            markDirty()
        }
    }

    private fun updateBlockMetadata() {
        val burn = if (isBurning) 8 else 0
        val v = worldObj.getBlockMetadata(xCoord, yCoord, zCoord)
        if (v and 0x8 != burn) {
            worldObj.setBlockMetadataWithNotify(
                xCoord, yCoord, zCoord, v and 0x3 or burn, BlockNotifyFlags.SEND_TO_CLIENTS
            )
        }
    }

    override fun readFromNBT(nbt: NBTTagCompound) {
        super.readFromNBT(nbt)
        if (nbt.hasKey("fuelStack")) {
            fuelStack = ItemStack.loadItemStackFromNBT(nbt.getCompoundTag("fuelStack"))
        }
        remainingTicks = nbt.getInteger("remainingTicks")
        currentItemBurnTime = nbt.getInteger("currentItemBurnTime")
    }

    override fun writeToNBT(nbt: NBTTagCompound) {
        super.writeToNBT(nbt)
        if (fuelStack != null) {
            val nbtFuelStack = NBTTagCompound()
            nbt.setTag("fuelStack", nbtFuelStack)
            fuelStack!!.writeToNBT(nbtFuelStack)
        }
        nbt.setInteger("remainingTicks", remainingTicks)
        nbt.setInteger("currentItemBurnTime", currentItemBurnTime)
    }

    override fun getSizeInventory(): Int {
        return 1
    }

    override fun getStackInSlot(slot: Int): ItemStack? {
        assert(slot == 0)
        return fuelStack
    }

    override fun decrStackSize(slot: Int, n: Int): ItemStack? {
        return InventoryUtils.decrStackSize(this, slot, n)
    }

    override fun getStackInSlotOnClosing(slot: Int): ItemStack? {
        return InventoryUtils.getStackInSlotOnClosing(this, slot)
    }

    override fun setInventorySlotContents(slot: Int, itemStack: ItemStack?) {
        assert(slot == 0)
        fuelStack = itemStack
        if (itemStack != null && itemStack.stackSize > inventoryStackLimit) {
            itemStack.stackSize = inventoryStackLimit
        }
    }

    override fun getInventoryName(): String {
        return "Furnace Generator"
    }

    override fun hasCustomInventoryName(): Boolean {
        return false
    }

    override fun getInventoryStackLimit(): Int {
        return 64
    }

    override fun isUseableByPlayer(player: EntityPlayer): Boolean {
        return (worldObj.getTileEntity(xCoord, yCoord, zCoord) === this
                && player.getDistanceSq(
            xCoord.toDouble() + 0.5,
            yCoord.toDouble() + 0.5,
            zCoord.toDouble() + 0.5
        )
                <= 64.0)
    }

    override fun openInventory() {}

    override fun closeInventory() {}

    override fun isItemValidForSlot(slot: Int, itemStack: ItemStack): Boolean {
        return slot == 0 && TileEntityFurnace.isItemFuel(itemStack)
    }

    fun onBreak() {
        if (fuelStack != null) {
            val f = worldObj.rand.nextFloat() * 0.6f + 0.1f
            val f1 = worldObj.rand.nextFloat() * 0.6f + 0.1f
            val f2 = worldObj.rand.nextFloat() * 0.6f + 0.1f
            val dropLoc = Vector3((xCoord + f).toDouble(), (yCoord + f1).toDouble(), (zCoord + f2).toDouble())
            InventoryUtils.dropItem(fuelStack, worldObj, dropLoc)
        }
    }

}