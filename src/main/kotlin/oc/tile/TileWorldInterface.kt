package oc.tile

import codechicken.lib.inventory.InventoryRange
import codechicken.lib.inventory.InventoryUtils
import codechicken.lib.vec.Vector3
import li.cil.oc.api.Network
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import li.cil.oc.util.ExtendedNBT
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.nbt.JsonToNBT
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.potion.PotionEffect
import net.minecraft.server.MinecraftServer
import net.minecraft.util.DamageSource
import oc.util.Utils

private const val PLAYER_NOT_FOUND = "player not found"
private const val INVALID_SLOT = "invalid slot"

class TileWorldInterface : BaseTileWithEnvironment() {

    override val node: Node? = Network.newNode(this, Visibility.Network).withComponent("world_interface").create()

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, slot:number):table" +
                " -- Returns a description of the stack in the player's inventory."
    )
    fun getStackInSlot(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val slot = arguments.checkInteger(1)
        val player = Utils.findPlayer(name)
        if (player != null) {
            return if (slot >= 1 && slot <= player.inventory.sizeInventory) {
                return arrayOf(player.inventory.getStackInSlot(slot - 1)?.toStringTable())
            } else {
                arrayOf(null, INVALID_SLOT)
            }
        }
        return arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string):table" +
                " -- Returns a description of all stacks in the player's inventory."
    )
    fun getAllStacks(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        if (player != null) {
            val stacks = arrayOfNulls<ItemStack>(player.inventory.sizeInventory)
            for (i in 0 until player.inventory.sizeInventory) {
                stacks[i] = player.inventory.getStackInSlot(i)
            }
            return arrayOf(stacks.map { it?.toStringTable() })
        }
        return arrayOf(null, PLAYER_NOT_FOUND)
    }

    private fun ItemStack.toStringTable() = Utils.createStringTable(
        "name", Utils.getForgeName(item),
        "damage", itemDamage,
        "maxDamage", maxDamage,
        "size", stackSize,
        "label", item.getItemStackDisplayName(this),
        "hasTag", hasTagCompound(),
        "tag", tagCompound?.toString()
    )

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, itemName:string, itemMetadata:number," +
                " itemCount:number[, jsonNbt:string]):boolean -- Inserts item into the player's inventory." +
                " Returns true if the item was inserted."
    )
    fun insertItem(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val item = Item.itemRegistry.getObject(arguments.checkString(1)) as? Item
            ?: return arrayOf(false, "invalid item id")
        val metadata = arguments.checkInteger(2)
        val count = arguments.checkInteger(3).coerceAtLeast(0)
        val tag = arguments.optString(4, null)?.let {
            JsonToNBT.func_150315_a(it) as? NBTTagCompound ?: return arrayOf(false, "not a valid tag")
        }
        val player = Utils.findPlayer(name)
        if (player != null) {
            val inv = InventoryRange(player.inventory, 0, player.inventory.mainInventory.size)
            fun insert(simulate: Boolean): Boolean {
                val stack = ItemStack(item, 1, metadata).apply { tagCompound = tag }
                var inserted = 0
                while (inserted < count) {
                    stack.stackSize = (count - inserted).coerceAtMost(stack.maxStackSize)
                    if (InventoryUtils.insertItem(inv, stack, simulate) == 0) {
                        inserted += stack.stackSize
                    } else {
                        return false
                    }
                }
                return true
            }
            if (insert(simulate = true)) {
                insert(simulate = false)
                return arrayOf(true)
            }
            return arrayOf(false, "no enough space")
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, itemName:string, itemMetadata:number," +
                " itemCount:number[, nbt:string]):boolean -- Destroys an item in the player’s inventory." +
                " Returns true if the item was destroyed."
    )
    fun clearItem(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val item = Item.itemRegistry.getObject(arguments.checkString(1)) as? Item
            ?: return arrayOf(false, "invalid item id")
        val metadata = arguments.checkInteger(2)
        val count = arguments.checkInteger(3).coerceAtLeast(0)
        val targetTag = arguments.optString(4, null)?.let {
            JsonToNBT.func_150315_a(it) as? NBTTagCompound ?: return arrayOf(false, "not a valid tag")
        }
        val player = Utils.findPlayer(name)
        if (player != null) {
            fun clear(simulate: Boolean): Boolean {
                val targetStack = ItemStack(item, 1, metadata)
                var destroyed = 0
                for (i in 0 until player.inventory.sizeInventory) {
                    val stack = player.inventory.getStackInSlot(i)
                    if (stack != null && stack.isItemEqual(targetStack) && (targetTag == null || stack.stackTagCompound == targetTag)) {
                        val n = (count - destroyed).coerceAtMost(stack.stackSize)
                        destroyed += n
                        if (!simulate) {
                            stack.stackSize -= n
                            if (stack.stackSize == 0) {
                                player.inventory.setInventorySlotContents(i, null)
                            }
                        }
                        if (destroyed == count) {
                            break
                        }
                    }
                }
                return destroyed == count
            }
            if (clear(simulate = true)) {
                clear(simulate = false)
                return arrayOf(true)
            }
            return arrayOf(false, "no enough count")
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, itemName:string, itemMetadata:number," +
                " itemCount:number[, nbt:string]):boolean -- Drops an item next to a player." +
                " Returns true if the item was dropped."
    )
    fun dropItem(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val item = Item.itemRegistry.getObject(arguments.checkString(1)) as? Item
            ?: return arrayOf(false, "invalid item id")
        val metadata = arguments.checkInteger(2)
        val count = arguments.checkInteger(3).coerceAtLeast(0)
        val tag = arguments.optString(4, null)?.let {
            JsonToNBT.func_150315_a(it) as? NBTTagCompound ?: return arrayOf(false, "not a valid tag")
        }
        val player = Utils.findPlayer(name)
        if (player != null) {
            var dropped = 0
            while (dropped < count) {
                val stack = ItemStack(item, 0, metadata).apply { tagCompound = tag }
                stack.stackSize = (count - dropped).coerceAtMost(stack.maxStackSize)
                InventoryUtils.dropItem(stack, player.worldObj, Vector3.fromEntityCenter(player))
                dropped += stack.stackSize
            }
            return arrayOf(true)
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, slot:integer):boolean" +
                " -- Destroys an item stack in a player’s inventory. Returns true if the item was destroyed."
    )
    fun destroyStackInSlot(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val slot = arguments.checkInteger(1)
        val player = Utils.findPlayer(name)
        if (player != null) {
            return if (slot >= 1 && slot <= player.inventory.sizeInventory) {
                val stack = player.inventory.getStackInSlot(slot - 1)
                player.inventory.setInventorySlotContents(slot - 1, null)
                arrayOf(true, stack.stackSize)
            } else {
                arrayOf(false, INVALID_SLOT)
            }
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string):number, number" +
                " -- Returns the current and maximum health of the player."
    )
    fun getPlayerHealth(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        return if (player != null) {
            arrayOf(player.health, player.maxHealth)
        } else arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string):boolean -- Heals a player.")
    fun heal(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        if (player != null) {
            val health = arguments.checkDouble(1)
            player.heal(health.toFloat())
            return arrayOf(true)
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string): boolean -- Kills a player.")
    fun kill(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        if (player != null) {
            player.attackEntityFrom(DamageSource.outOfWorld, Float.MAX_VALUE)
            return arrayOf(true)
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string, dimension:integer):number, number, number" +
                " -- Returns the player specific spawn location for the dimension."
    )
    fun getSpawnLocation(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        if (player != null) {
            val dimension = arguments.checkInteger(1)
            val chunkCoordinates = player.getBedLocation(dimension)
            return if (chunkCoordinates != null)
                arrayOf(chunkCoordinates.posX, chunkCoordinates.posY, chunkCoordinates.posZ)
            else
                arrayOf(null, "spawn location not found")
        }
        return arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string): boolean, string -- Returns player food stats.")
    fun getPlayerFoodStats(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        return if (player != null) {
            arrayOf(player.foodStats.needFood(), player.foodStats.foodLevel, player.foodStats.saturationLevel)
        } else arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string): boolean, string -- Checks that the player is flying.")
    fun isFlying(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        return if (player != null) {
            arrayOf(player.capabilities.isFlying)
        } else arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(
        doc = "function(playerName:string): (x:number, y:number, z:number, dimension:number)" +
                " -- Returns player position and dimension."
    )
    fun getPlayerPosition(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        return if (player != null) {
            arrayOf(player.posX, player.posY, player.posZ, player.dimension)
        } else arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string):table -- get current potion effects.")
    fun getActiveEffects(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val player = Utils.findPlayer(name)
        if (player != null) {
            return arrayOf(player.activePotionEffects.map {
                val potionEffect = it as PotionEffect
                mapOf<String, Any>(
                    "potionId" to potionEffect.potionID,
                    "duration" to potionEffect.duration,
                    "amplifier" to potionEffect.amplifier,
                    "name" to potionEffect.effectName
                )
            }.toTypedArray())
        }
        return arrayOf(null, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():table -- get online players.")
    fun getOnlinePlayers(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(MinecraftServer.getServer().configurationManager.allUsernames)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(playerName:string, message:string):boolean -- Kicks player.")
    fun kick(context: Context, arguments: Arguments): Array<Any?>? {
        val name = arguments.checkString(0)
        val message = arguments.checkString(1)
        val player = Utils.findPlayer(name)
        if (player != null) {
            player.playerNetServerHandler.kickPlayerFromServer(message)
            return arrayOf(true)
        }
        return arrayOf(false, PLAYER_NOT_FOUND)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(direct = true, doc = "function(name:string):table -- return nbt data from player.")
    fun getPlayerNBT(context: Context, arguments: Arguments): Array<Any?>? {
        val player = Utils.findPlayer(arguments.checkString(0))
        player?.let {
            return arrayOf(ExtendedNBT.ExtendedNBTBase(NBTTagCompound().also { player.writeToNBT(it) }).toTypedMap())
        }
        return arrayOf(null)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(direct = true, doc = "unstable function")
    fun setPlayerNBT(context: Context, args: Arguments): Array<Any> {
        return arrayOf(false)
    }

}