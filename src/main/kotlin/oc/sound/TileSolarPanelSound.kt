package oc.sound

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.client.Minecraft
import net.minecraft.client.audio.ISound.AttenuationType
import net.minecraft.client.audio.MovingSound
import net.minecraft.util.ResourceLocation
import oc.tile.TileSolarPanel

@SideOnly(Side.CLIENT)
class TileSolarPanelSound(resourceLocation: ResourceLocation?, private var solarPanel: TileSolarPanel) :
        MovingSound(resourceLocation) {

    override fun isDonePlaying(): Boolean {
        val tileEntity = Minecraft.getMinecraft().theWorld.getTileEntity(solarPanel.xCoord, solarPanel.yCoord,
                solarPanel.zCoord)
        return tileEntity == null || !solarPanel.isRotate || donePlaying
    }

    override fun update() {
        if (!solarPanel.isRotate) donePlaying = true
    }

    init {
        xPosF = solarPanel.xCoord.toFloat()
        yPosF = solarPanel.yCoord.toFloat()
        zPosF = solarPanel.zCoord.toFloat()
        repeat = true
        field_147666_i = AttenuationType.LINEAR
    }

}