package oc.init

import cpw.mods.fml.common.registry.GameRegistry
import li.cil.oc.api.API
import li.cil.oc.api.driver.EnvironmentProvider
import net.minecraft.item.Item
import oc.OMA
import oc.init.Init.makeRegistryName
import oc.item.BaseItemWithDriver
import oc.item.Items
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

private var loadCount = 0
private var regStage = false

internal object Items {

    internal fun register() {
        regStage = true
        Items
        regStage = false
        OMA.info("Successfully loaded {} items", loadCount)
    }

}

internal fun regItem(enabled: Boolean, provider: () -> Item): ItemRegistrar =
        ItemRegistrar(enabled, provider)

internal class ItemRegistrar(private val enabled: Boolean, private val provider: () -> Item) {

    operator fun provideDelegate(thisRef: Items, prop: KProperty<*>): PropItem {
        val registryName = makeRegistryName(prop.name)
        if (!regStage) {
            throw RuntimeException("Initializer error")
        }
        if (enabled) {
            OMA.info("register item: $registryName")
            val item = provider()
            GameRegistry.registerItem(item, registryName)
            item.unlocalizedName = registryName
            item.setTextureName(OMA.MOD_ID + ":" + registryName)
            if (item is BaseItemWithDriver) {
                if (item.isEnabled) {
                    API.driver.add(item as li.cil.oc.api.driver.Item)
                    API.driver.add(item as EnvironmentProvider)
                } else {
                    Init.hide(item)
                    item.creativeTab = null
                }
            }
            loadCount++
            return PropItem(item)
        }
        return PropItem(null)
    }

}

internal class PropItem(val item: Item?) : ReadOnlyProperty<Items, Item?> {

    override fun getValue(thisRef: Items, property: KProperty<*>): Item? = item

}