package oc.init

import ic2.api.item.IC2Items
import net.minecraft.init.Items
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import oc.Mods
import oc.block.Blocks
import oc.settings
import oc.util.RecipeUtils

object Recipes {

    fun register() {
        val enderEye = Items.ender_eye
        val map: Item = Items.map
        if (settings.ironInventory.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeIronInventory),
                    "xxx",
                    "xix",
                    "xxx",
                    'x',
                    "ingotIron",
                    'i',
                    "oc:inventoryUpgrade")
        }
        if (settings.diamondInventory.hasRecipe()) {
            val i: Any = if (settings.ironInventory.hasRecipe()) oc.item.Items.upgradeIronInventory!! else "oc:inventoryUpgrade"
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeDiamondInventory), "xxx", "xix", "xxx", 'x', "gemDiamond", 'i', i)
        }
        if (Mods.advancedSolarPanels()) {
            val blockAlloyGlass = RecipeUtils.getItem("IC2:blockAlloyGlass")
            val advancedSolarPanel = RecipeUtils.makeItemStack("AdvancedSolarPanel:BlockAdvSolarPanel", 0)
            val hybridSolarPanel = RecipeUtils.makeItemStack("AdvancedSolarPanel:BlockAdvSolarPanel", 1)
            val ultimateHybridSolarPanel = RecipeUtils.makeItemStack("AdvancedSolarPanel:BlockAdvSolarPanel", 2)
            val quantumSolarPanel = RecipeUtils.makeItemStack("AdvancedSolarPanel:BlockAdvSolarPanel", 3)
            if (settings.advancedSolarPanel.hasRecipe()) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.upgradeAdvancedSolarPanel),
                        "ggg",
                        "cpc",
                        "zbz",
                        'g',
                        blockAlloyGlass,
                        'c',
                        "oc:circuitChip3",
                        'p',
                        advancedSolarPanel,
                        'z',
                        "blockSteel",
                        'b',
                        "oc:materialCircuitBoardPrinted")
                if (settings.hybridSolarPanel.hasRecipe()) {
                    RecipeUtils.addShapedRecipe(
                            ItemStack(oc.item.Items.upgradeHybridSolarPanel),
                            "ggg",
                            "cpc",
                            "zbz",
                            'g',
                            blockAlloyGlass,
                            'c',
                            "oc:circuitChip3",
                            'p',
                            hybridSolarPanel,
                            'z',
                            "blockSteel",
                            'b',
                            "oc:materialCircuitBoardPrinted")
                    if (settings.ultimateSolarPanel.hasRecipe()) {
                        RecipeUtils.addShapedRecipe(
                                ItemStack(oc.item.Items.upgradeUltimateSolarPanel),
                                "ggg",
                                "cpc",
                                "zbz",
                                'g',
                                blockAlloyGlass,
                                'c',
                                "oc:circuitChip3",
                                'p',
                                ultimateHybridSolarPanel,
                                'z',
                                "blockSteel",
                                'b',
                                "oc:materialCircuitBoardPrinted")
                        if (settings.quantumSolarPanel.hasRecipe()) {
                            RecipeUtils.addShapedRecipe(
                                    ItemStack(oc.item.Items.upgradeQuantumSolarPanel),
                                    "ggg",
                                    "cpc",
                                    "zbz",
                                    'g',
                                    blockAlloyGlass,
                                    'c',
                                    "oc:circuitChip3",
                                    'p',
                                    quantumSolarPanel,
                                    'z',
                                    "blockSteel",
                                    'b',
                                    "oc:materialCircuitBoardPrinted")
                        }
                    }
                }
            }
        } else {
            if (settings.advancedSolarPanel.hasRecipe()) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.upgradeAdvancedSolarPanel),
                        "ppp",
                        "pxp",
                        "ppp",
                        'p',
                        "oc:solarGeneratorUpgrade",
                        'x',
                        "blockGold")
                if (settings.hybridSolarPanel.hasRecipe()) {
                    RecipeUtils.addShapedRecipe(
                            ItemStack(oc.item.Items.upgradeHybridSolarPanel),
                            "ppp",
                            "pxp",
                            "ppp",
                            'p',
                            ItemStack(oc.item.Items.upgradeAdvancedSolarPanel),
                            'x',
                            "blockDiamond")
                    if (settings.ultimateSolarPanel.hasRecipe()) {
                        RecipeUtils.addShapedRecipe(
                                ItemStack(oc.item.Items.upgradeUltimateSolarPanel),
                                "ppp",
                                "pxp",
                                "ppp",
                                'p',
                                ItemStack(oc.item.Items.upgradeHybridSolarPanel),
                                'x',
                                RecipeUtils.makeItemStack("minecraft:skull", 1))
                        if (settings.quantumSolarPanel.hasRecipe()) {
                            RecipeUtils.addShapedRecipe(
                                    ItemStack(oc.item.Items.upgradeQuantumSolarPanel),
                                    "ppp",
                                    "pxp",
                                    "ppp",
                                    'p',
                                    ItemStack(oc.item.Items.upgradeUltimateSolarPanel),
                                    'x',
                                    RecipeUtils.makeItemStack("minecraft:nether_star", 0))
                        }
                    }
                }
            }
        }
        if (settings.geolyzer.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeAdvancedGeolyzer),
                    "xgx",
                    "cic",
                    "xpx",
                    'g',
                    "oc:geolyzer",
                    'x',
                    "ingotGold",
                    'c',
                    "oc:circuitChip3",
                    'i',
                    "oc:materialInterweb",
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.tesseract.hasRecipe()) {
            val tesseractIngredient = if (settings.enderlink.hasRecipe()) oc.item.Items.upgradeEnderlink else Items.blaze_rod
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeTesseract),
                    "xgx",
                    "cic",
                    "xpx",
                    'g',
                    enderEye,
                    'x',
                    "gemDiamond",
                    'c',
                    "oc:circuitChip3",
                    'i',
                    tesseractIngredient,
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.navigation.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeAdvancedNavigation),
                    "xgx",
                    "cic",
                    "xpx",
                    'g',
                    map,
                    'x',
                    "ingotGold",
                    'c',
                    "oc:circuitChip2",
                    'i',
                    enderEye,
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.enderlink.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeEnderlink),
                    "xgx",
                    "cic",
                    "xpx",
                    'g',
                    "oc:transposer",
                    'x',
                    enderEye,
                    'c',
                    "oc:circuitChip3",
                    'i',
                    "oc:materialInterweb",
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.itemCharger.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeItemCharger),
                    "x x",
                    "cic",
                    "xpx",
                    'x',
                    "gemDiamond",
                    'c',
                    "oc:circuitChip3",
                    'i',
                    "oc:charger",
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.cropnalyzer.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeCropnalyzer),
                    "x x",
                    "cic",
                    "xpx",
                    'x',
                    "ingotGold",
                    'c',
                    "oc:circuitChip2",
                    'i',
                    RecipeUtils.makeItemStackWithWildCard("IC2:itemCropnalyzer"),
                    'p',
                    "oc:materialCircuitBoardPrinted")
        }
        if (settings.blockSolarPanel.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(Blocks.solarPanel),
                    "ggg",
                    "mli",
                    "cpc",
                    'g', RecipeUtils.getItem("IC2:blockAlloyGlass"),
                    'l', "plateDenseLapis",
                    'i', RecipeUtils.getItem("IC2:itemPartIridium"),
                    'c', RecipeUtils.getItem("IC2:itemPartCircuitAdv"),
                    'p', RecipeUtils.makeItemStack("IC2:blockGenerator", 3),
                    'm', IC2Items.getItem("elemotor")
            )
        }
        if (settings.blockRadar.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(Blocks.radar),
                    "iei",
                    "cpc",
                    "ibi",
                    'i', "ingotIron",
                    'p', "oc:cpu3",
                    'c', "oc:circuitChip3",
                    'b', "oc:materialCircuitBoardPrinted",
                    'e', enderEye
            )
        }
        if (settings.furnaceGenerator.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(Blocks.furnaceGenerator),
                    "ipi",
                    "rrr",
                    "ifi",
                    'i', "ingotIron",
                    'p', "craftingPiston",
                    'r', "dustRedstone",
                    'f', RecipeUtils.getItem("minecraft:furnace")
            )
        }
        if (settings.upgradeRadar.hasRecipe()) {
            if (Blocks.radar == null) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.upgradeRadar),
                        "iei",
                        "cpc",
                        "ibi",
                        'i', "ingotIron",
                        'p', "oc:cpu3",
                        'c', "oc:circuitChip3",
                        'b', "oc:materialCircuitBoardPrinted",
                        'e', enderEye
                )
            } else {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.upgradeRadar),
                        "ere",
                        "ece",
                        "ebe",
                        'r', Blocks.radar,
                        'c', "oc:circuitChip3",
                        'b', "oc:materialCircuitBoardPrinted",
                        'e', enderEye
                )
            }
        }
        if (settings.chatbox.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(Blocks.chatbox),
                    "gsg",
                    "ckc",
                    "gbg",
                    's', "stickWood",
                    'g', "ingotGold",
                    'k', "oc:keyboard",
                    'c', "oc:circuitChip2",
                    'b', "oc:materialCircuitBoardPrinted"
            )
        }
        if (settings.scanner.hasRecipe()) {
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.scanner),
                    "iai",
                    "cgc",
                    "ibi",
                    'a', "oc:analyzer",
                    'i', "ingotGold",
                    'g', "oc:geolyzer",
                    'c', "oc:circuitChip2",
                    'b', "oc:materialCircuitBoardPrinted"
            )
        }
        if (settings.upgradeTesla.hasRecipe()) {
            val teslaCraftIngredient: Any? = if (Mods.industrialCraft2())
                IC2Items.getItem("teslaCoil")
            else
                RecipeUtils.getItem("minecraft:nether_star")
            RecipeUtils.addShapedRecipe(
                    ItemStack(oc.item.Items.upgradeTesla),
                    "iti",
                    "cxc",
                    "ibi",
                    'x', "oc:capacitor",
                    'i', "gemEmerald",
                    't', teslaCraftIngredient,
                    'c', "oc:circuitChip2",
                    'b', "oc:materialCircuitBoardPrinted"
            )
        }
        if (settings.industrialMicrocontrollerCase.hasRecipe()) {
            if (Mods.industrialCraft2()) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialMicrocontrollerCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "oc:microcontrollerCase2",
                        's', "plateDenseSteel",
                        'g', "plateDenseGold",
                        'l', "plateDenseLapis"
                )
            } else {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialMicrocontrollerCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "oc:microcontrollerCase2",
                        's', "blockIron",
                        'g', "blockGold",
                        'l', "blockLapis"
                )
            }
        }
        if (settings.industrialDroneCase.hasRecipe()) {
            if (Mods.industrialCraft2()) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialDroneCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "oc:droneCase2",
                        's', "plateDenseSteel",
                        'g', "plateDenseGold",
                        'l', "plateDenseLapis"
                )
            } else {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialDroneCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "oc:droneCase2",
                        's', "blockIron",
                        'g', "blockGold",
                        'l', "blockLapis"
                )
            }
        }
        if (settings.industrialRobotCase.hasRecipe()) {
            if (Mods.industrialCraft2()) {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialRobotCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "oc:case3",
                        's', RecipeUtils.getItem("IC2:itemPartIridium"),
                        'g', "plateDenseGold",
                        'l', "plateDenseLapis"
                )
            } else {
                RecipeUtils.addShapedRecipe(
                        ItemStack(oc.item.Items.industrialRobotCase),
                        "sls",
                        "gxg",
                        "sls",
                        'x', "case3",
                        's',  RecipeUtils.makeItemStack("minecraft:nether_star", 0),
                        'g', "blockGold",
                        'l', "blockLapis"
                )
            }
        }
    }
}