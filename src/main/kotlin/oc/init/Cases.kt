package oc.init

import oc.settings
import oc.template.IndustrialDroneTemplate
import oc.template.IndustrialMicrocontrollerTemplate
import oc.template.IndustrialRobotTemplate

internal object Cases {
    fun register() {
        if (settings.industrialMicrocontrollerCase.isEnabled) {
            IndustrialMicrocontrollerTemplate.register()
        }
        if (settings.industrialDroneCase.isEnabled) {
            IndustrialDroneTemplate.register()
        }
        if (settings.industrialRobotCase.isEnabled) {
            IndustrialRobotTemplate.register()
        }
    }
}