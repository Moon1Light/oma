package oc.init

import cpw.mods.fml.common.registry.GameRegistry
import net.minecraft.block.Block
import net.minecraft.item.Item
import oc.OMA
import oc.block.Blocks
import oc.init.Init.makeRegistryName
import oc.item.DefaultItemBlock
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

private var loadCount = 0
private var regStage = false

internal object Blocks {

    internal fun register() {
        regStage = true
        Blocks
        regStage = false
        OMA.info("Successfully loaded {} blocks", loadCount)
    }

}

internal fun regBlock(enabled: Boolean, provider: () -> Block): BlockRegistrar =
        BlockRegistrar(enabled, provider)

internal class BlockRegistrar(private val enabled: Boolean, private val provider: () -> Block) {

    operator fun provideDelegate(thisRef: Blocks, prop: KProperty<*>): PropBlock {
        val registryName = makeRegistryName(prop.name)
        if (!regStage) {
            throw RuntimeException("Initializer error")
        }
        if (enabled) {
            OMA.info("register block: $registryName")
            val block = provider()
            block.setBlockName(registryName)
            val itemBlockClass = if (block is IBlockCustomItem) {
                block.customItem
            } else {
                DefaultItemBlock::class.java
            }
            GameRegistry.registerBlock(block, itemBlockClass, registryName)
            Item.getItemFromBlock(block)?.let { item ->
                item.unlocalizedName = registryName
                item.setTextureName(OMA.MOD_ID + ":" + registryName)
            }
            if (block is IHidden) {
                Init.hide(block)
            }
            loadCount++
            return PropBlock(block)
        }
        return PropBlock(null)
    }

}

internal class PropBlock(val block: Block?) : ReadOnlyProperty<Blocks, Block?> {

    override fun getValue(thisRef: Blocks, property: KProperty<*>): Block? = block

}