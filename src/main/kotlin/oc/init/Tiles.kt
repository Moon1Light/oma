package oc.init

import cpw.mods.fml.common.registry.GameRegistry
import net.minecraft.tileentity.TileEntity
import oc.init.Init.makeRegistryName
import oc.settings
import oc.tile.*

internal object Tiles {

    internal fun register() {
        if (settings.antenna.isEnabled)
            regTile(TileAntennaController::class.java)

        if (settings.chatbox.isEnabled)
            regTile(TileChatbox::class.java)

        if (settings.creativeChatbox.isEnabled)
            regTile(TileCreativeChatbox::class.java)

        if (settings.worldInterface.isEnabled)
            regTile(TileWorldInterface::class.java)

        if (settings.blockRadar.isEnabled)
            regTile(TileRadar::class.java)

        if (settings.blockSolarPanel.isEnabled)
            regTile(TileSolarPanel::class.java)

        regTile(TileBound::class.java)

        if (settings.furnaceGenerator.isEnabled)
            regTile(TileFurnaceGenerator::class.java)
    }

}

private fun regTile(clazz: Class<out TileEntity>) {
    val registryName = makeRegistryName(clazz.name)
    GameRegistry.registerTileEntity(clazz, registryName)
}