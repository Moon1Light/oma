package oc.init

import com.google.common.base.CaseFormat
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import oc.Mods
import oc.item.BaseItemBlock

internal object Init {
    internal val hidedItems = mutableListOf<ItemStack>()

    internal fun hide(item: Item) {
        if (Mods.nei()) hidedItems.add(ItemStack(item))
    }

    internal fun hide(block: Block) {
        if (Mods.nei()) hidedItems.add(ItemStack(block))
    }

    internal fun makeRegistryName(fieldName: String) = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldName)
}

internal interface IBlockCustomItem {
    val customItem: Class<out BaseItemBlock?>?
}

internal interface IHidden