package oc.init

import cpw.mods.fml.client.registry.ClientRegistry
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler
import cpw.mods.fml.client.registry.RenderingRegistry
import oc.settings
import oc.renderer.*
import oc.tile.TileAntennaController
import oc.tile.TileRadar
import oc.tile.TileSolarPanel

object Render {

    var renderIdAntenna = 0
    var renderIdAntennaController = 0
    var renderRadar = 0
    var renderSolarPanel = 0

    fun register() {
        if (settings.antenna.isEnabled) {
            renderIdAntenna = registerModel(RendererBlockAntenna())
            renderIdAntennaController = registerModel(RendererAntennaController())
            ClientRegistry.bindTileEntitySpecialRenderer(TileAntennaController::class.java, RendererTileAntenna())
        }
        if (settings.blockRadar.isEnabled) {
            renderRadar = registerModel(RendererRadar())
            ClientRegistry.bindTileEntitySpecialRenderer(TileRadar::class.java, RendererTileRadar())
        }
        if (settings.blockSolarPanel.isEnabled) {
            renderSolarPanel = registerModel(RendererBlockSolarPanel())
            ClientRegistry.bindTileEntitySpecialRenderer(TileSolarPanel::class.java, RendererTileSolarPanel())
        }
    }

    private fun registerModel(handler: ISimpleBlockRenderingHandler?): Int {
        val renderId = RenderingRegistry.getNextAvailableRenderId()
        RenderingRegistry.registerBlockHandler(renderId, handler)
        return renderId
    }

}