package oc.init

import li.cil.oc.client.renderer.`PetRenderer$`
import oc.OMA
import oc.settings
import oc.util.Reflection
import scala.Predef
import scala.Tuple3
import scala.collection.JavaConversions
import scala.collection.JavaConverters
import scala.collection.Map
import java.util.*

private typealias ScalaPetColor = Tuple3<Double, Double, Double>

object OCPetRenderer {
    private val entitledPlayers = HashMap<String, ScalaPetColor>()

    fun add(uuid: String, r: Double, g: Double, b: Double) {
        entitledPlayers[uuid] = Tuple3(r, g, b)
    }

    internal fun init() {
        add("6629a41e-b1a0-40f0-95d1-92b8098449fc", 0.2, 0.53, 0.66) // svitoos (official account, Scrag)
        add("0e6d26bf-76f9-c201-1031-608614f7139b", 0.2, 0.53, 0.66) // svitoos (computercraft.ru, vx13)
        settings.petRenderer.entitledPlayers.asIterable().forEach { e ->
            val data = e.split(" ")
            try {
                add(data[0], data[1].toDouble(), data[2].toDouble(), data[3].toDouble())
            } catch (e: RuntimeException) {
                OMA.warn("Invalid pet_renderer entry: $e")
            }
        }
        val t = Reflection.getInternal(`PetRenderer$`.`MODULE$`, "entitledPlayers")
        if (t is Map<*, *>) {
            JavaConversions.mapAsJavaMap(t).forEach { e ->
                val uuid = e.key
                @Suppress("UNCHECKED_CAST") val value = e.value as ScalaPetColor
                entitledPlayers[uuid.toString()] = value
            }
        }
        Reflection.setInternal(`PetRenderer$`.`MODULE$`, "entitledPlayers",
                JavaConverters.mapAsScalaMapConverter(entitledPlayers).asScala().toMap(
                        Predef.conforms()
                ))
    }

}
