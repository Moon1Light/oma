package oc.init

import li.cil.oc.api.API
import oc.Mods
import oc.block.BlockEnvironmentProvider
import oc.driver.ic2.*
import oc.settings

internal object Drivers {

    internal fun register() {
        API.driver.add(BlockEnvironmentProvider)
        if (Mods.industrialCraft2()) {
            if (settings.cropnalyzer.allowItemStackInspection) {
                API.driver.add(ConvertCropSeedItem)
            }

            with(settings.ic2Drivers) {
                if (isEnabled) {
                    if (electricMachine) API.driver.add(DriverElectricMachine)
                    if (standardMachine) API.driver.add(DriverStandardMachine)
                    if (advancedMiner) API.driver.add(DriverAdvMiner)
                    if (blastFurnace) API.driver.add(DriverBlastFurnace)
                    if (canningMachine) API.driver.add(DriverCanningMachine)
                    if (condenser) API.driver.add(DriverCondenser)
                    if (liquidHeatExchanger) API.driver.add(DriverEntityLiquidHeatExchanger)
                    if (fermenter) API.driver.add(DriverFermenter)
                    if (fluidRegulator) API.driver.add(DriverFluidRegulator)
                    if (heatSourceInventory) API.driver.add(DriverHeatSourceInventory)
                    if (inductionFurnace) API.driver.add(DriverInductionFurnace)
                    if (ironFurnace) API.driver.add(DriverIronFurnace)
                    if (metalFormer) API.driver.add(DriverMetalFormer)
                    if (pump) API.driver.add(DriverPump)
                    if (steamGenerator) API.driver.add(DriverSteamGenerator)
                    if (thermalCentrifuge) API.driver.add(DriverThermalCentrifuge)
                    if (replicator) {
                        API.driver.add(DriverReplicator)
                        API.driver.add(DriverPatternStorage)
                        API.driver.add(DriverScanner)
                        API.driver.add(ConvertPattern)
                    }
                    if (lathe) {
                        API.driver.add(DriverLathe)
                    }
                    if (blockCutter) {
                        API.driver.add(DriverBlockCutter)
                        API.driver.add(ConvertBlockCuttingBlade)
                    }
                    if (fluidDistributor) API.driver.add(DriverFluidDistributor)
                }
            }
        }
    }

}