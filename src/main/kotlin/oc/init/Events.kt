package oc.init

import net.minecraftforge.common.MinecraftForge
import oc.settings
import oc.driver.ic2.EventHandlerIndustrialCraft2

internal object Events {

    internal fun register() {
        if (settings.cropnalyzer.allowGeolyzerAnalyze) {
            MinecraftForge.EVENT_BUS.register(EventHandlerIndustrialCraft2())
        }
    }

}