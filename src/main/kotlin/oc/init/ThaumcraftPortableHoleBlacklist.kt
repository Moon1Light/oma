package oc.init

import codechicken.core.ReflectionManager
import cpw.mods.fml.common.Loader
import net.minecraft.block.Block
import oc.Mods
import oc.OMA
import oc.block.Blocks

internal object ThaumcraftPortableHoleBlacklist {
    internal fun init() {
        if (Loader.isModLoaded(Mods.Ids.thaumcraft)) {
            getPortableHoleBlackList()?.let { portableHoleBlackList ->
                Blocks.solarPanel?.let {
                    OMA.info("Add solar panel block to Thaumcraft Portable Hole blacklist")
                    portableHoleBlackList.add(it)
                }
                Blocks.bound?.let {
                    OMA.info("Add bound block to Thaumcraft Portable Hole blacklist")
                    portableHoleBlackList.add(it)
                }
            }
        }
    }

    private fun getPortableHoleBlackList(): ArrayList<Block>? {
        ReflectionManager.findClass("thaumcraft.api.ThaumcraftApi")?.let { tcApiClass ->
            try {
                ReflectionManager.getField(tcApiClass, ArrayList::class.java, null, "portableHoleBlackList")?.let { e ->
                    @Suppress("UNCHECKED_CAST") return e as ArrayList<Block>
                }
            } catch (e: Exception) {
                return null
            }
        }
        return null
    }
}