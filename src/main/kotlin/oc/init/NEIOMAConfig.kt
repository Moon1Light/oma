package oc.init

import codechicken.nei.api.API
import codechicken.nei.api.IConfigureNEI
import cpw.mods.fml.common.Mod
import cpw.mods.fml.common.Optional
import oc.OMA

class NEIOMAConfig : IConfigureNEI {

    @Optional.Method(modid = "NotEnoughItems")
    override fun getName(): String? {
        return OMA::class.java.getAnnotation(Mod::class.java).name
    }

    @Optional.Method(modid = "NotEnoughItems")
    override fun getVersion(): String? {
        return OMA::class.java.getAnnotation(Mod::class.java).version
    }

    @Optional.Method(modid = "NotEnoughItems")
    override fun loadConfig() {
        Init.hidedItems.forEach { API.hideItem(it) }
        Init.hidedItems.clear()
    }

}