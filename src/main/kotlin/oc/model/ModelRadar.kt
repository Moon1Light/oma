package oc.model

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.client.model.ModelBase
import net.minecraft.client.model.ModelRenderer

/**
 * Created by Avaja on 07.12.2016.
 */
@SideOnly(Side.CLIENT)
class ModelRadar : ModelBase() {

    private val sensorBase: ModelRenderer = ModelRenderer(this, 0, 0).setTextureSize(64, 64).apply {
        addBox(-8.0f, -8.0f, -8.0f, 16, 4, 16, 0.0f)
    }
    private val sensorAxel: ModelRenderer = ModelRenderer(this, 0, 0).setTextureSize(64, 64).apply {
        setRotationPoint(0.0f, -4.0f, 0.0f)
    }
    private val sensorDishCenter: ModelRenderer = ModelRenderer(this, 0, 24).setTextureSize(64, 64).apply {
        addBox(0f, 0f, 0f, 7, 6, 1)
        setRotationPoint(-3.0f, 4.0f, -0.5f)
    }
    private val sensorDishLeft: ModelRenderer = ModelRenderer(this, 32, 24).setTextureSize(64, 64).apply {
        addBox(0.0f, 0.0f, 0.0f, 4, 6, 1, 0.0f)
        setRotationPoint(0.0f, 0.0f, 1.0f)
        rotateAngleY = -(7.0f / 6.0f) * Math.PI.toFloat()
    }
    private val sensorDishRight: ModelRenderer = ModelRenderer(this, 48, 24).setTextureSize(64, 64).apply {
        addBox(3.5f, 0.0f, 0.0f, 4, 6, 1, 0.0f)
        setRotationPoint(3.5f, 0.0f, 2.0f)
        rotateAngleY = (Math.PI / 6).toFloat()
    }

    init {
        sensorAxel.addChild(sensorDishCenter)
        sensorDishCenter.addChild(sensorDishLeft)
        sensorDishCenter.addChild(sensorDishRight)
    }

    fun renderSensor(degrees: Float) {
        sensorBase.render(0.0625f)
        sensorAxel.rotateAngleY = degrees * Math.PI.toFloat() / 180f % 360
        sensorAxel.render(0.0625f)
    }
}