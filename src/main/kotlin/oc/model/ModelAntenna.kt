/**
 * Copyright 2014-2015 Joshua Asbury (@theoriginalbit)
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package oc.model

import net.minecraft.client.model.ModelBase
import net.minecraft.client.model.ModelRenderer
import net.minecraft.entity.Entity

class ModelAntenna : ModelBase() {

    private var poleBottom: ModelRenderer
    private var poleMiddle: ModelRenderer
    private var poleTop: ModelRenderer
    private var cellAnchor: ModelRenderer
    private var modemSupport1: ModelRenderer
    private var modemSupport2: ModelRenderer
    private var cellSupport1: ModelRenderer
    private var cellSupport2: ModelRenderer
    private var cellSupport3: ModelRenderer
    private var cellSupport4: ModelRenderer
    private var cellSupport5: ModelRenderer
    private var cellSupport6: ModelRenderer
    private var cellSupport7: ModelRenderer
    private var cellSupport8: ModelRenderer
    private var cell1: ModelRenderer
    private var cell3: ModelRenderer
    private var cell5: ModelRenderer
    private var cell8: ModelRenderer
    private var cell6: ModelRenderer
    private var cell2: ModelRenderer
    private var cell7: ModelRenderer
    private var cell4: ModelRenderer
    private var structSupport1: ModelRenderer
    private var structSupport2: ModelRenderer
    private var structSupport3: ModelRenderer
    private var structSupport4: ModelRenderer
    private var anchorSupport1: ModelRenderer
    private var anchorSupport2: ModelRenderer
    private var base: ModelRenderer
    private var miniAntenna1: ModelRenderer
    private var miniAntenna2: ModelRenderer
    private var miniAntenna3: ModelRenderer
    private var miniAntenna4: ModelRenderer
    override fun render(entity: Entity?, f: Float, f1: Float, f2: Float, f3: Float, f4: Float, f5: Float) {
        super.render(entity, f, f1, f2, f3, f4, f5)
        setRotationAngles(f, f1, f2, f3, f4, f5, entity)
        poleBottom.render(f5)
        poleMiddle.render(f5)
        poleTop.render(f5)
        cellAnchor.render(f5)
        modemSupport1.render(f5)
        modemSupport2.render(f5)
        cellSupport1.render(f5)
        cellSupport2.render(f5)
        cellSupport3.render(f5)
        cellSupport4.render(f5)
        cellSupport5.render(f5)
        cellSupport6.render(f5)
        cellSupport7.render(f5)
        cellSupport8.render(f5)
        cell1.render(f5)
        cell3.render(f5)
        cell5.render(f5)
        cell8.render(f5)
        cell6.render(f5)
        cell2.render(f5)
        cell7.render(f5)
        cell4.render(f5)
        structSupport1.render(f5)
        structSupport2.render(f5)
        structSupport3.render(f5)
        structSupport4.render(f5)
        anchorSupport1.render(f5)
        anchorSupport2.render(f5)
        base.render(f5)
        miniAntenna1.render(f5)
        miniAntenna2.render(f5)
        miniAntenna3.render(f5)
        miniAntenna4.render(f5)
    }

    private fun setRotation(model: ModelRenderer, x: Float, y: Float, z: Float) {
        model.rotateAngleX = x
        model.rotateAngleY = y
        model.rotateAngleZ = z
    }

    init {
        textureWidth = 256
        textureHeight = 128
        poleBottom = ModelRenderer(this, 0, 0)
        poleBottom.addBox(-6f, -48f, -6f, 12, 92, 12)
        poleBottom.setRotationPoint(0f, -24f, 0f)
        poleBottom.setTextureSize(256, 128)
        poleBottom.mirror = true
        setRotation(poleBottom, 0f, 0f, 0f)
        poleMiddle = ModelRenderer(this, 49, 0)
        poleMiddle.addBox(-4f, -40f, -4f, 8, 80, 8)
        poleMiddle.setRotationPoint(0f, -112f, 0f)
        poleMiddle.setTextureSize(256, 128)
        poleMiddle.mirror = true
        setRotation(poleMiddle, 0f, 0f, 0f)
        poleTop = ModelRenderer(this, 82, 0)
        poleTop.addBox(-2f, -40f, -2f, 4, 84, 4)
        poleTop.setRotationPoint(0f, -196f, 0f)
        poleTop.setTextureSize(256, 128)
        poleTop.mirror = true
        setRotation(poleTop, 0f, 0f, 0f)
        cellAnchor = ModelRenderer(this, 99, 37)
        cellAnchor.addBox(-5f, 0f, -5f, 10, 28, 10)
        cellAnchor.setRotationPoint(0f, -230f, 0f)
        cellAnchor.setTextureSize(256, 128)
        cellAnchor.mirror = true
        setRotation(cellAnchor, 0f, 0f, 0f)
        modemSupport1 = ModelRenderer(this, 49, 89)
        modemSupport1.addBox(-7f, -1f, -1f, 12, 2, 2)
        modemSupport1.setRotationPoint(1f, -193f, 0f)
        modemSupport1.setTextureSize(256, 128)
        modemSupport1.mirror = true
        setRotation(modemSupport1, 0f, 0f, 0f)
        modemSupport2 = ModelRenderer(this, 49, 89)
        modemSupport2.addBox(-7f, -1f, -1f, 12, 2, 2)
        modemSupport2.setRotationPoint(0f, -193f, 1f)
        modemSupport2.setTextureSize(256, 128)
        modemSupport2.mirror = true
        setRotation(modemSupport2, 0f, -1.570796f, 0f)
        cellSupport1 = ModelRenderer(this, 49, 99)
        cellSupport1.addBox(-11f, -1f, 0f, 22, 2, 1)
        cellSupport1.setRotationPoint(-12f, -224f, -12f)
        cellSupport1.setTextureSize(256, 128)
        cellSupport1.mirror = true
        setRotation(cellSupport1, 0f, 0.7853982f, 0f)
        cellSupport2 = ModelRenderer(this, 49, 99)
        cellSupport2.addBox(-11f, -1f, 0f, 22, 2, 1)
        cellSupport2.setRotationPoint(-12f, -209f, -12f)
        cellSupport2.setTextureSize(256, 128)
        cellSupport2.mirror = true
        setRotation(cellSupport2, 0f, 0.7853982f, 0f)
        cellSupport3 = ModelRenderer(this, 49, 99)
        cellSupport3.addBox(-11f, -1f, -1f, 22, 2, 1)
        cellSupport3.setRotationPoint(12f, -209f, 12f)
        cellSupport3.setTextureSize(256, 128)
        cellSupport3.mirror = true
        setRotation(cellSupport3, 0f, 0.7853982f, 0f)
        cellSupport4 = ModelRenderer(this, 49, 99)
        cellSupport4.addBox(-11f, -1f, -1f, 22, 2, 1)
        cellSupport4.setRotationPoint(12f, -224f, 12f)
        cellSupport4.setTextureSize(256, 128)
        cellSupport4.mirror = true
        setRotation(cellSupport4, 0f, 0.7853982f, 0f)
        cellSupport5 = ModelRenderer(this, 49, 99)
        cellSupport5.addBox(-11f, -1f, -1f, 22, 2, 1)
        cellSupport5.setRotationPoint(-12f, -224f, 12f)
        cellSupport5.setTextureSize(256, 128)
        cellSupport5.mirror = true
        setRotation(cellSupport5, 0f, -0.7853982f, 0f)
        cellSupport6 = ModelRenderer(this, 49, 99)
        cellSupport6.addBox(-11f, -1f, -1f, 22, 2, 1)
        cellSupport6.setRotationPoint(-12f, -209f, 12f)
        cellSupport6.setTextureSize(256, 128)
        cellSupport6.mirror = true
        setRotation(cellSupport6, 0f, -0.7853982f, 0f)
        cellSupport7 = ModelRenderer(this, 49, 99)
        cellSupport7.addBox(-11f, -1f, 0f, 22, 2, 1)
        cellSupport7.setRotationPoint(12f, -209f, -12f)
        cellSupport7.setTextureSize(256, 128)
        cellSupport7.mirror = true
        setRotation(cellSupport7, 0f, -0.7853982f, 0f)
        cellSupport8 = ModelRenderer(this, 49, 99)
        cellSupport8.addBox(-11f, -1f, 0f, 22, 2, 1)
        cellSupport8.setRotationPoint(12f, -224f, -12f)
        cellSupport8.setTextureSize(256, 128)
        cellSupport8.mirror = true
        setRotation(cellSupport8, 0f, -0.7853982f, 0f)
        cell1 = ModelRenderer(this, 99, 0)
        cell1.addBox(-5f, -16f, -2f, 8, 32, 4)
        cell1.setRotationPoint(-6f, -216f, -21f)
        cell1.setTextureSize(256, 128)
        cell1.mirror = true
        setRotation(cell1, 0f, 0.7853982f, 0f)
        cell3 = ModelRenderer(this, 99, 0)
        cell3.addBox(-3f, -16f, -2f, 8, 32, 4)
        cell3.setRotationPoint(-21f, -216f, 6f)
        cell3.setTextureSize(256, 128)
        cell3.mirror = true
        setRotation(cell3, 0f, -0.7853982f, 0f)
        cell5 = ModelRenderer(this, 99, 0)
        cell5.addBox(-3f, -16f, -2f, 8, 32, 4)
        cell5.setRotationPoint(6f, -216f, 21f)
        cell5.setTextureSize(256, 128)
        cell5.mirror = true
        setRotation(cell5, 0f, 0.7853982f, 0f)
        cell8 = ModelRenderer(this, 99, 0)
        cell8.addBox(-3f, -16f, -2f, 8, 32, 4)
        cell8.setRotationPoint(6f, -216f, -21f)
        cell8.setTextureSize(256, 128)
        cell8.mirror = true
        setRotation(cell8, 0f, -0.7853982f, 0f)
        cell6 = ModelRenderer(this, 99, 0)
        cell6.addBox(-5f, -16f, -2f, 8, 32, 4)
        cell6.setRotationPoint(21f, -216f, 6f)
        cell6.setTextureSize(256, 128)
        cell6.mirror = true
        setRotation(cell6, 0f, 0.7853982f, 0f)
        cell2 = ModelRenderer(this, 99, 0)
        cell2.addBox(-3f, -16f, -2f, 8, 32, 4)
        cell2.setRotationPoint(-21f, -216f, -6f)
        cell2.setTextureSize(256, 128)
        cell2.mirror = true
        setRotation(cell2, 0f, 0.7853982f, 0f)
        cell7 = ModelRenderer(this, 99, 0)
        cell7.addBox(-5f, -16f, -2f, 8, 32, 4)
        cell7.setRotationPoint(21f, -216f, -6f)
        cell7.setTextureSize(256, 128)
        cell7.mirror = true
        setRotation(cell7, 0f, -0.7853982f, 0f)
        cell4 = ModelRenderer(this, 99, 0)
        cell4.addBox(-5f, -16f, -2f, 8, 32, 4)
        cell4.setRotationPoint(-6f, -216f, 21f)
        cell4.setTextureSize(256, 128)
        cell4.mirror = true
        setRotation(cell4, 0f, -0.7853982f, 0f)
        structSupport1 = ModelRenderer(this, 126, 76)
        structSupport1.addBox(-1f, -5f, 0f, 2, 13, 1)
        structSupport1.setRotationPoint(-12f, -218f, -12f)
        structSupport1.setTextureSize(256, 128)
        structSupport1.mirror = true
        setRotation(structSupport1, 0f, 0.7853982f, 0f)
        structSupport2 = ModelRenderer(this, 126, 76)
        structSupport2.addBox(-1f, -5f, -1f, 2, 13, 1)
        structSupport2.setRotationPoint(12f, -218f, 12f)
        structSupport2.setTextureSize(256, 128)
        structSupport2.mirror = true
        setRotation(structSupport2, 0f, 0.7853982f, 0f)
        structSupport3 = ModelRenderer(this, 126, 76)
        structSupport3.addBox(-1f, -5f, 0f, 2, 13, 1)
        structSupport3.setRotationPoint(12f, -218f, -12f)
        structSupport3.setTextureSize(256, 128)
        structSupport3.mirror = true
        setRotation(structSupport3, 0f, -0.7853982f, 0f)
        structSupport4 = ModelRenderer(this, 126, 76)
        structSupport4.addBox(-1f, -5f, -1f, 2, 13, 1)
        structSupport4.setRotationPoint(-12f, -218f, 12f)
        structSupport4.setTextureSize(256, 128)
        structSupport4.mirror = true
        setRotation(structSupport4, 0f, -0.7853982f, 0f)
        anchorSupport1 = ModelRenderer(this, 49, 94)
        anchorSupport1.addBox(-16f, -1f, -1f, 32, 2, 2)
        anchorSupport1.setRotationPoint(0f, -218f, 0f)
        anchorSupport1.setTextureSize(256, 128)
        anchorSupport1.mirror = true
        setRotation(anchorSupport1, 0f, -0.7853982f, 0f)
        anchorSupport2 = ModelRenderer(this, 49, 94)
        anchorSupport2.addBox(-16f, -1f, -1f, 32, 2, 2)
        anchorSupport2.setRotationPoint(0f, -218f, 0f)
        anchorSupport2.setTextureSize(256, 128)
        anchorSupport2.mirror = true
        setRotation(anchorSupport2, 0f, 0.7853982f, 0f)
        base = ModelRenderer(this, 0, 105)
        base.addBox(-8f, -2f, -8f, 16, 4, 16)
        base.setRotationPoint(0f, 22f, 0f)
        base.setTextureSize(256, 128)
        base.mirror = true
        setRotation(base, 0f, 0f, 0f)
        miniAntenna1 = ModelRenderer(this, 66, 103)
        miniAntenna1.addBox(0f, 0f, 0f, 4, 20, 2)
        miniAntenna1.setRotationPoint(6f, -198f, -1f)
        miniAntenna1.setTextureSize(256, 128)
        miniAntenna1.mirror = true
        setRotation(miniAntenna1, 0f, 0f, 0f)
        miniAntenna2 = ModelRenderer(this, 66, 103)
        miniAntenna2.addBox(0f, 0f, 0f, 4, 20, 2)
        miniAntenna2.setRotationPoint(-6f, -199f, 1f)
        miniAntenna2.setTextureSize(256, 128)
        miniAntenna2.mirror = true
        setRotation(miniAntenna2, 0f, 3.141593f, 0f)
        miniAntenna3 = ModelRenderer(this, 66, 103)
        miniAntenna3.addBox(0f, 0f, 0f, 4, 20, 2)
        miniAntenna3.setRotationPoint(1f, -198f, 6f)
        miniAntenna3.setTextureSize(256, 128)
        miniAntenna3.mirror = true
        setRotation(miniAntenna3, 0f, -1.570796f, 0f)
        miniAntenna4 = ModelRenderer(this, 66, 103)
        miniAntenna4.addBox(0f, 0f, 0f, 4, 20, 2)
        miniAntenna4.setRotationPoint(-1f, -199f, -6f)
        miniAntenna4.setTextureSize(256, 128)
        miniAntenna4.mirror = true
        setRotation(miniAntenna4, 0f, 1.570796f, 0f)
    }

}