package oc

import cpw.mods.fml.common.Loader

object Mods {
    fun advancedSolarPanels(): Boolean {
        return Loader.isModLoaded("AdvancedSolarPanel") && industrialCraft2()
    }

    fun industrialCraft2(): Boolean {
        return settings.integrationIndustrialCraft2 && Loader.isModLoaded("IC2")
    }

    fun nei() = Loader.isModLoaded("NotEnoughItems")

    object Ids {
        const val waila = "Waila"
        const val thaumcraft = "Thaumcraft"
    }
}