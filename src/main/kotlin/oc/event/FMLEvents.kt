package oc.event

import cpw.mods.fml.common.eventhandler.SubscribeEvent
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent
import oc.tile.TileChatbox.Companion.handlePlayerLoggedIn
import oc.tile.TileChatbox.Companion.handlePlayerLoggedOut

class FMLEvents {
    @SubscribeEvent
    fun playerLoggedIn(loggedInEvent: PlayerLoggedInEvent?) {
        handlePlayerLoggedIn(loggedInEvent!!)
    }

    @SubscribeEvent
    fun playerLoggedOut(playerLoggedOutEvent: PlayerLoggedOutEvent?) {
        handlePlayerLoggedOut(playerLoggedOutEvent!!)
    }
}