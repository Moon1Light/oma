package oc.event

enum class TileStatusEvent {

    Validate,
    Invalidate,
    Unload

}