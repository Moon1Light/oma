package oc.event

import cpw.mods.fml.common.eventhandler.SubscribeEvent
import net.minecraft.entity.player.EntityPlayer
import net.minecraftforge.event.ServerChatEvent
import net.minecraftforge.event.entity.living.LivingDeathEvent
import oc.tile.TileChatbox.Companion.handleEventMessage
import oc.tile.TileChatbox.Companion.handleEventPlayerDeath

class CommonEvents {

    @SubscribeEvent
    fun chatMessage(event: ServerChatEvent) {
        try {
            if (!event.player.worldObj.isRemote) handleEventMessage(event)
        } catch (e: Exception) {
        }
    }

    @SubscribeEvent
    fun playerDeath(event: LivingDeathEvent) {
        if (!event.entity.worldObj.isRemote && event.entityLiving is EntityPlayer) {
            handleEventPlayerDeath(event)
        }
    }
}