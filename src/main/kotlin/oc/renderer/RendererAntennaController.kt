package oc.renderer

import net.minecraft.block.Block
import net.minecraft.client.renderer.RenderBlocks
import net.minecraft.world.IBlockAccess
import oc.init.Render
import oc.tile.TileAntennaController

class RendererAntennaController : RendererBlockAntenna() {

    override fun renderWorldBlock(world: IBlockAccess, x: Int, y: Int, z: Int, block: Block, modelId: Int, renderer: RenderBlocks): Boolean {
        val tile = world.getTileEntity(x, y, z) as TileAntennaController
        if (!tile.isTowerComplete) {
            renderer.renderStandardBlock(block, x, y, z)
        }
        return true
    }

    override fun getRenderId(): Int {
        return Render.renderIdAntennaController
    }

}