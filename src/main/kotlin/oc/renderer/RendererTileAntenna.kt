package oc.renderer

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity
import oc.Resources
import oc.model.ModelAntenna
import oc.tile.TileAntennaController
import org.lwjgl.opengl.GL11

class RendererTileAntenna : TileEntitySpecialRenderer() {

    var model = ModelAntenna()

    override fun renderTileEntityAt(tileEntity: TileEntity, x: Double, y: Double, z: Double, t: Float) {
        val tile = tileEntity as TileAntennaController
        if (tile.isTowerComplete) {
            GL11.glPushMatrix()
            GL11.glTranslatef(x.toFloat() + 0.5f, y.toFloat() + 0.5f, z.toFloat() + 0.5f)
            GL11.glRotatef(180f, 0f, 0f, 1f)
            GL11.glTranslatef(0.0f, -1.0f, 0.0f)
            GL11.glPushMatrix()
            bindTexture(Resources.modelAntennaTexture)
            model.render(null, 0f, 0f, 0f, 0f, 0f, 0.0625f)
            GL11.glPopMatrix()
            GL11.glPopMatrix()
        }
    }

}