package oc.renderer

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import oc.OMA
import oc.tile.TileSolarPanel
import org.lwjgl.opengl.GL11

class RendererTileSolarPanel : TileEntitySpecialRenderer() {

    private val texture: ResourceLocation = ResourceLocation(OMA.MOD_ID.toLowerCase(), "textures/models/solarPanel.png")
    private val top: IModelCustom = AdvancedModelLoader.loadModel(
        ResourceLocation(
            OMA.MOD_ID.toLowerCase(),
            "textures/models/top.obj"
        )
    )
    private val bottom: IModelCustom = AdvancedModelLoader.loadModel(
        ResourceLocation(
            OMA.MOD_ID.toLowerCase(),
            "textures/models/bottom.obj"
        )
    )

    override fun renderTileEntityAt(tileEntity: TileEntity, posX: Double, posY: Double, posZ: Double, v3: Float) {
        bindTexture(texture)
        val solarPanel = tileEntity as TileSolarPanel
        GL11.glPushMatrix()
        GL11.glTranslated(posX + .5, posY, posZ + .5)
        GL11.glScalef(1f, 1f, 1f)
        GL11.glPushMatrix()
        GL11.glRotatef(90f, 0f, 1f, 0f)
        bottom.renderAll()
        GL11.glTranslated(0.0, 0.8, 0.0)
        GL11.glRotatef(solarPanel.pitch, 1f, 0f, 0f)
        top.renderAll()
        GL11.glPopMatrix()
        GL11.glPopMatrix()
    }

}