package oc.renderer

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler
import net.minecraft.block.Block
import net.minecraft.client.renderer.RenderBlocks
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher
import net.minecraft.world.IBlockAccess
import oc.block.Blocks
import oc.init.Render
import oc.tile.TileRadar
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL12

class RendererRadar : ISimpleBlockRenderingHandler {

    override fun getRenderId(): Int {
        return Render.renderRadar
    }

    override fun renderInventoryBlock(block: Block, metadata: Int, modelID: Int, renderer: RenderBlocks) {
        GL11.glRotatef(-90.0f, 0.0f, 1.0f, 0.0f)
        GL11.glTranslatef(-0.5f, -0.5f, -0.5f)
        if (block === Blocks.radar) {
            TileEntityRendererDispatcher.instance.renderTileEntityAt(TileRadar(), 0.0, 0.0,
                    0.0, 0.0f)
            GL11.glEnable(GL12.GL_RESCALE_NORMAL)
        }
    }

    override fun renderWorldBlock(world: IBlockAccess, x: Int, y: Int, z: Int, block: Block, modelId: Int, renderer: RenderBlocks): Boolean {
        return false
    }

    override fun shouldRender3DInInventory(modelId: Int): Boolean {
        return true
    }

}