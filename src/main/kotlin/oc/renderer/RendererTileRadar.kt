package oc.renderer

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer
import net.minecraft.tileentity.TileEntity
import oc.Resources
import oc.model.ModelRadar
import oc.tile.TileRadar
import org.lwjgl.opengl.GL11

class RendererTileRadar : TileEntitySpecialRenderer() {

    private val radarModel: ModelRadar = ModelRadar()

    override fun renderTileEntityAt(tileEntity: TileEntity, x: Double, y: Double, z: Double, partialTick: Float) {
        val radar = tileEntity as TileRadar
        GL11.glPushMatrix()
        GL11.glTranslatef(x.toFloat() + 0.5f, y.toFloat() + 0.5f, z.toFloat() + 0.5f)
        bindTexture(Resources.modelRadar)
        GL11.glPushMatrix()
        radarModel.renderSensor(radar.yaw)
        val placing = (radar.facing + 1) * 90
        GL11.glPopMatrix()
        GL11.glRotatef(placing.toFloat(), 0f, 1f, 0f)
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f)
        GL11.glPopMatrix()
    }

}