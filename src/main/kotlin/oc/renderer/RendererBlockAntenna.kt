package oc.renderer

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler
import net.minecraft.block.Block
import net.minecraft.client.renderer.RenderBlocks
import net.minecraft.client.renderer.Tessellator
import net.minecraft.world.IBlockAccess
import oc.init.Render

open class RendererBlockAntenna : ISimpleBlockRenderingHandler {

    override fun renderInventoryBlock(block: Block, metadata: Int, modelID: Int, renderer: RenderBlocks) {
        val tessellator = Tessellator.instance
        renderer.setRenderBounds(0.0, 0.0, 0.0, 1.0, 1.0, 1.0)
        tessellator.startDrawingQuads()
        tessellator.setNormal(1.0f, 0.0f, 0.0f)
        renderer.renderFaceXPos(block, 0.0, -0.1, 0.0, block.getIcon(3, 0))
        tessellator.draw()
        tessellator.startDrawingQuads()
        tessellator.setNormal(-1.0f, 0.0f, 0.0f)
        renderer.renderFaceXNeg(block, 0.0, -0.1, 0.0, block.getIcon(2, 0))
        tessellator.draw()
        tessellator.startDrawingQuads()
        tessellator.setNormal(0.0f, 1.0f, 0.0f)
        renderer.renderFaceYPos(block, 0.0, -0.1, 0.0, block.getIcon(1, 0))
        tessellator.draw()
        tessellator.startDrawingQuads()
        tessellator.setNormal(0.0f, -1.0f, 0.0f)
        renderer.renderFaceYNeg(block, 0.0, -0.1, 0.0, block.getIcon(0, 0))
        tessellator.draw()
        tessellator.startDrawingQuads()
        tessellator.setNormal(0.0f, 0.0f, 1.0f)
        renderer.renderFaceZPos(block, 0.0, -0.1, 0.0, block.getIcon(4, 0))
        tessellator.draw()
        tessellator.startDrawingQuads()
        tessellator.setNormal(0.0f, 0.0f, -1.0f)
        renderer.renderFaceZNeg(block, 0.0, -0.1, 0.0, block.getIcon(5, 0))
        tessellator.draw()
    }

    override fun renderWorldBlock(world: IBlockAccess, x: Int, y: Int, z: Int, block: Block, modelId: Int, renderer: RenderBlocks): Boolean { // render as normal block
        if (world.getBlockMetadata(x, y, z) == 0) {
            renderer.renderStandardBlock(block, x, y, z)
        }
        // else invisible!
        return true
    }

    override fun shouldRender3DInInventory(modelId: Int): Boolean {
        return true
    }

    override fun getRenderId(): Int {
        return Render.renderIdAntenna
    }

}