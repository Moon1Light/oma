package oc.renderer

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler
import net.minecraft.block.Block
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.RenderBlocks
import net.minecraft.util.ResourceLocation
import net.minecraft.world.IBlockAccess
import net.minecraftforge.client.model.AdvancedModelLoader
import net.minecraftforge.client.model.IModelCustom
import oc.OMA
import oc.init.Render
import org.lwjgl.opengl.GL11

class RendererBlockSolarPanel : ISimpleBlockRenderingHandler {

    private val texture: ResourceLocation = ResourceLocation(OMA.MOD_ID.toLowerCase(), "textures/models/solarPanel.png")
    private val top: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(OMA.MOD_ID.toLowerCase(),
            "textures/models/top.obj"))
    private val bottom: IModelCustom = AdvancedModelLoader.loadModel(ResourceLocation(OMA.MOD_ID.toLowerCase(),
            "textures/models/bottom.obj"))

    private fun bindTexture(resourceLocation: ResourceLocation?) {
        val textureManager = Minecraft.getMinecraft().textureManager
        textureManager?.bindTexture(resourceLocation)
    }

    override fun renderInventoryBlock(block: Block, i: Int, i1: Int, renderBlocks: RenderBlocks) {
        bindTexture(texture)
        GL11.glPushMatrix()
        GL11.glScalef(.3f, .3f, .3f)
        GL11.glTranslated(.5, 0.0, .5)
        GL11.glPushMatrix()
        //GL11.glRotatef(180, 0F, 1F, 0);
        bottom.renderAll()
        GL11.glTranslated(0.0, 0.8, 0.0)
        GL11.glRotatef(30f, 1f, 0f, 0f)
        top.renderAll()
        GL11.glPopMatrix()
        GL11.glPopMatrix()
    }

    override fun renderWorldBlock(iBlockAccess: IBlockAccess, i: Int, i1: Int, i2: Int, block: Block, i3: Int, renderBlocks: RenderBlocks): Boolean {
        return true
    }

    override fun shouldRender3DInInventory(i: Int): Boolean {
        return true
    }

    override fun getRenderId(): Int {
        return Render.renderSolarPanel
    }

}