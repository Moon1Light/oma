package oc.container

import codechicken.lib.inventory.ContainerExtended
import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.entity.player.InventoryPlayer
import net.minecraft.inventory.ICrafting
import net.minecraft.inventory.Slot
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntityFurnace
import oc.tile.TileFurnaceGenerator

class ContainerFurnaceGenerator(
        playerInventory: InventoryPlayer?, private val tileFurnaceGenerator: TileFurnaceGenerator) : ContainerExtended() {

    init {
        addSlotToContainer(object : Slot(tileFurnaceGenerator, 0, 80, 35) {
            override fun isItemValid(itemStack: ItemStack): Boolean {
                return TileEntityFurnace.isItemFuel(itemStack)
            }
        })
        bindPlayerInventory(playerInventory)
    }

    var remainingTicks = 0
    var currentItemBurnTime = 0
    var isBurning = false

    override fun addCraftingToCrafters(craft: ICrafting) {
        super.addCraftingToCrafters(craft)
        craft.sendProgressBarUpdate(this, 0, tileFurnaceGenerator.remainingTicks)
        craft.sendProgressBarUpdate(this, 1, tileFurnaceGenerator.currentItemBurnTime)
        craft.sendProgressBarUpdate(this, 2, if (tileFurnaceGenerator.isBurning) 1 else 0)
    }

    override fun doMergeStackAreas(slotIndex: Int, stack: ItemStack?): Boolean {
        return if (slotIndex < 1) mergeItemStack(stack, 1, 37, true) else mergeItemStack(stack, 0, 1, false)
    }

    override fun detectAndSendChanges() {
        super.detectAndSendChanges()
        crafters.forEach {
            val crafter = it as ICrafting
            if (remainingTicks != tileFurnaceGenerator.remainingTicks) {
                crafter.sendProgressBarUpdate(this, 0, tileFurnaceGenerator.remainingTicks)
            }
            if (currentItemBurnTime != tileFurnaceGenerator.currentItemBurnTime) {
                crafter.sendProgressBarUpdate(this, 1, tileFurnaceGenerator.currentItemBurnTime)
            }
            if (isBurning != tileFurnaceGenerator.isBurning) {
                crafter.sendProgressBarUpdate(this, 2, if (tileFurnaceGenerator.isBurning) 1 else 0)
            }
        }
        remainingTicks = tileFurnaceGenerator.remainingTicks
        currentItemBurnTime = tileFurnaceGenerator.currentItemBurnTime
        isBurning = tileFurnaceGenerator.isBurning
    }

    @SideOnly(Side.CLIENT)
    override fun updateProgressBar(id: Int, value: Int) {
        when (id) {
            0 -> remainingTicks = value
            1 -> currentItemBurnTime = value
            2 -> isBurning = value != 0
        }
    }

}