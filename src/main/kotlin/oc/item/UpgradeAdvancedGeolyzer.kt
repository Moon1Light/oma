package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.driver.DriverAdvancedGeolyzer
import oc.settings

class UpgradeAdvancedGeolyzer : BaseItemWithDriver(settings.geolyzer) {

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (worksWith(stack)) DriverAdvancedGeolyzer::class.java else null
    }

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverAdvancedGeolyzer(host)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack) && isRotatable(host)
    }

    override val tooltipData: Array<Any?>
        get() = arrayOf(settings.geolyzer.scanCost, settings.geolyzer.scanRadius)

}