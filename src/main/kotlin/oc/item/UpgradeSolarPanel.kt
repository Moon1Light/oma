package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.Settings.SolarPanel
import oc.driver.DriverSolarPanel
import oc.settings
import oc.util.I18nUtil

abstract class UpgradeAbstractSolarPanel(cfg: SolarPanel) : BaseItemWithDriver(cfg) {
    private val dayEnergyPerTick: Int = cfg.dayPerTick
    private val nightEnergyPerTick: Int = cfg.nightPerTick
    private val canChargeTool: Boolean = cfg.chargeTool

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (this.worksWith(stack)) DriverSolarPanel::class.java else null
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return this.worksWith(stack) && !isAdapter(host)
    }

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverSolarPanel(host, canChargeTool, nightEnergyPerTick, dayEnergyPerTick)
    }

    override val extendedTooltip: String?
        get() = I18nUtil.localize("lore.solar_panel", dayEnergyPerTick, nightEnergyPerTick,
                if (canChargeTool) I18nUtil.localize("lore.solar_panel.can_charge") else "")
}

class UpgradeAdvancedSolarPanel : UpgradeAbstractSolarPanel(settings.advancedSolarPanel)

class UpgradeHybridSolarPanel : UpgradeAbstractSolarPanel(settings.hybridSolarPanel)

class UpgradeUltimateSolarPanel : UpgradeAbstractSolarPanel(settings.ultimateSolarPanel)

class UpgradeQuantumSolarPanel : UpgradeAbstractSolarPanel( settings.quantumSolarPanel)