package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.driver.UpgradeEnderlink
import oc.settings

class UpgradeEnderlink : BaseItemWithDriver(settings.enderlink) {

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return UpgradeEnderlink(host)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack) && (isRobot(host) || isDrone(host))
    }

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (worksWith(stack)) UpgradeEnderlink::class.java else null
    }

}