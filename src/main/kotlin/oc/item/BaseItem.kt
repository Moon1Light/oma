package oc.item

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import oc.OMA
import oc.util.OCUtils

abstract class BaseItem : Item() {

    init {
        creativeTab = OMA.creativeTab
    }

    // Mostly stolen from Sangar
    @SideOnly(Side.CLIENT)
    @Suppress("UNCHECKED_CAST")
    override fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        OCUtils.addTooltip(stack, extendedTooltip, tooltip as MutableList<String>, tooltipData, advanced)
    }

    open val extendedTooltip: String?
        get() = null

    open val tooltipData: Array<Any?>
        get() = arrayOf()

}