package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.driver.DriverAdvancedNavigation
import oc.settings

class UpgradeAdvancedNavigation : BaseItemWithDriver(settings.navigation) {

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverAdvancedNavigation(host)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack) && isRotatable(host)
    }

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (worksWith(stack)) DriverAdvancedNavigation::class.java else null
    }

}