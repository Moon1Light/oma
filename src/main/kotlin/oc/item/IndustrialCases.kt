package oc.item

import li.cil.oc.common.Tier
import net.minecraft.item.EnumRarity
import net.minecraft.item.ItemStack
import oc.util.OCUtils

open class IndustrialCase : BaseItem() {

    override fun getRarity(stack: ItemStack): EnumRarity {
        return OCUtils.getRarityByTier(Tier.Three())
    }

}

class IndustrialMicrocontrollerCase : IndustrialCase()

class IndustrialDroneCase : IndustrialCase()

class IndustrialRobotCase : IndustrialCase()