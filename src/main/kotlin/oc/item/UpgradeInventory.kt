package oc.item

import li.cil.oc.api.driver.item.Inventory
import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.driver.item.UpgradeRenderer
import li.cil.oc.api.driver.item.UpgradeRenderer.MountPointName
import li.cil.oc.api.event.RobotRenderEvent.MountPoint
import li.cil.oc.api.internal.Robot
import li.cil.oc.api.network.EnvironmentHost
import net.minecraft.client.Minecraft
import net.minecraft.item.ItemStack
import oc.RenderUtils
import oc.Resources
import oc.Settings.StuffWithTier
import oc.settings
import oc.util.I18nUtil

abstract class UpgradeAbstractInventory(cfg: StuffWithTier, private val capacity: Int) :
           BaseItemWithDriver(cfg), Inventory, UpgradeRenderer {

    override fun inventoryCapacity(stack: ItemStack): Int {
        return capacity
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return this.worksWith(stack) && isRobot(host)
    }

    override fun computePreferredMountPoint(stack: ItemStack, robot: Robot, availableMountPoints: Set<String>): String {
        return MountPointName.Any
    }

    override fun render(stack: ItemStack, mountPoint: MountPoint, robot: Robot, pt: Float) {
        val tm = Minecraft.getMinecraft().textureManager
        if (stack.item is UpgradeIronInventory) {
            tm.bindTexture(Resources.IronInventoryTexture)
            RenderUtils.drawSimpleBlock(mountPoint, 0f)
        } else if (stack.item is UpgradeDiamondInventory) {
            tm.bindTexture(Resources.DiamondInventoryTexture)
            RenderUtils.drawSimpleBlock(mountPoint, 0f)
        }
    }

    override val extendedTooltip: String?
        get() = I18nUtil.localize("lore.inventory", capacity)
}

class UpgradeIronInventory : UpgradeAbstractInventory(settings.ironInventory, 32)

class UpgradeDiamondInventory : UpgradeAbstractInventory(settings.diamondInventory, 64)
