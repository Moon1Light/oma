package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.driver.DriverItemCharger
import oc.settings

class UpgradeItemCharger : BaseItemWithDriver(settings.itemCharger) {

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverItemCharger(host)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack) && (isRobot(host) || isDrone(host))
    }

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (worksWith(stack)) DriverItemCharger::class.java else null
    }

}