package oc.item

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemBlock
import net.minecraft.item.ItemStack
import oc.block.BaseBlock

abstract class BaseItemBlock(block: Block) : ItemBlock(block) {
    private val specialBlock = block as? BaseBlock

    @SideOnly(Side.CLIENT)
    @Suppress("UNCHECKED_CAST")
    override fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        specialBlock?.addInformation(stack, player, tooltip, advanced)
    }

}

class DefaultItemBlock(block: Block) : BaseItemBlock(block)