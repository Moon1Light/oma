package oc.item

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.client.renderer.texture.IIconRegister
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.inventory.IInventory
import net.minecraft.item.EnumRarity
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.ChatComponentText
import net.minecraft.util.IIcon
import net.minecraft.world.World
import oc.driver.DriverTesseract
import oc.settings
import oc.util.I18nUtil
import oc.util.OCUtils

class UpgradeTesseract : BaseItemWithDriver(settings.tesseract) {

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (super.worksWith(stack)) DriverTesseract::class.java else null
    }

    override fun onItemUse(stack: ItemStack, player: EntityPlayer, world: World, x: Int, y: Int, z: Int, p_77648_7_: Int, p_77648_8_: Float, p_77648_9_: Float, p_77648_10_: Float): Boolean {
        if (world.isRemote) {
            return false
        }
        val tileEntity = world.getTileEntity(x, y, z)
        var compound = stack.tagCompound
        if (tileEntity is IInventory) {
            val link = DriverTesseract.Link(world.provider.dimensionId, x, y, z,
                    world.worldInfo.worldName + " / " + world.provider.dimensionName)
            if (compound == null) {
                compound = NBTTagCompound()
                stack.tagCompound = compound
            }
            link.save(compound)
            player.addChatComponentMessage(ChatComponentText(I18nUtil.localize("chat.tesseract_bind")))
            return true
        } else {
            if (compound != null) {
                DriverTesseract.Link.clear(compound)
                player.addChatComponentMessage(
                        ChatComponentText(I18nUtil.localize("chat.tesseract_unbind")))
                return true
            }
        }
        return false
    }

    override fun registerIcons(register: IIconRegister) {
        unbind = register.registerIcon("oma:upgrade_tesseract_unbind")
        bind = register.registerIcon("oma:upgrade_tesseract_bind")
    }

    override fun getIconFromDamage(p_77617_1_: Int): IIcon {
        return unbind
    }

    override fun getIconIndex(stack: ItemStack): IIcon {
        return this.getIcon(stack, 0)
    }

    override fun getIcon(stack: ItemStack, pass: Int): IIcon {
        return if (DriverTesseract.Link.isLinked(stack)) {
            bind
        } else unbind
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return this.worksWith(stack) && (isRobot(host) || isDrone(host))
    }

    override fun worksWith(stack: ItemStack): Boolean {
        return super.worksWith(stack) && DriverTesseract.Link.isLinked(stack)
    }

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverTesseract(host, DriverTesseract.Link.load(stack)!!)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    @SideOnly(Side.CLIENT)
    override fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        super.addInformation(stack, player, tooltip, advanced)
        val link = DriverTesseract.Link.load(stack)
        val info = if (link != null) {
            I18nUtil.localize("lore.tesseract.link") + "\n" +
                    I18nUtil.localize("lore.tesseract.pos", link.dimName, link.x, link.y, link.z)
        } else {
            I18nUtil.localize("lore.tesseract.no_link")
        }
        @Suppress("UNCHECKED_CAST") OCUtils.addTooltip(info, tooltip as MutableList<String>)
    }

    // unlinked tesseract does not have a driver. therefore, we set the rarity manually.
    override fun getRarity(stack: ItemStack): EnumRarity {
        return OCUtils.getRarityByTier(settings.tesseract.tier - 1)
    }

    companion object {
        private lateinit var unbind: IIcon
        private lateinit var bind: IIcon
    }

}