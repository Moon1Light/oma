package oc.item

import net.minecraft.block.Block
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World
import oc.util.BlockPos
import oc.util.BoundUtils

class ItemSolarPanel(block: Block) : BaseItemBlock(block) {

    override fun onItemUse(itemStack: ItemStack, entityPlayer: EntityPlayer, world: World, x: Int, y: Int, z: Int,
                           side: Int, xOffset: Float, yOffset: Float, zOffset: Float): Boolean {
        val boundBox = BoundUtils.isEmptyBoundBox(world, AxisAlignedBB.getBoundingBox(-1.0,
                0.0, -1.0, 1.0, 1.0, 1.0),
                BlockPos(x, y + 1, z, null))
        return if (boundBox) {
            super.onItemUse(itemStack, entityPlayer, world, x, y, z, side, xOffset, yOffset, zOffset)
        } else false
    }

}