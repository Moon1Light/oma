package oc.item

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import li.cil.oc.api.driver.EnvironmentProvider
import li.cil.oc.api.driver.item.HostAware
import li.cil.oc.api.internal.*
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.EnumRarity
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.EnumChatFormatting
import oc.Settings.StuffWithTier
import oc.util.I18nUtil
import oc.util.OCUtils

abstract class BaseItemWithDriver(cfg: StuffWithTier) : BaseItem(), HostAware, EnvironmentProvider {

    val isEnabled: Boolean = cfg.isEnabled
    private val _tier: Int = cfg.tier - 1

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return null
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack)
    }

    override fun worksWith(stack: ItemStack): Boolean {
        return stack.item == this
    }

    final override fun createEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        host.world()?.let { world ->
            if (!world.isRemote) {
                return createServerEnvironment(stack, host)
            }
        }
        return null
    }

    open fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return null
    }

    override fun slot(stack: ItemStack): String? {
        return null
    }

    override fun tier(stack: ItemStack): Int {
        return _tier
    }

    override fun dataTag(stack: ItemStack): NBTTagCompound? {
        return null
    }

    fun isAdapter(host: Class<out EnvironmentHost>): Boolean {
        return Adapter::class.java.isAssignableFrom(host)
    }

    fun isComputer(host: Class<out EnvironmentHost>): Boolean {
        return Case::class.java.isAssignableFrom(host)
    }

    fun isRobot(host: Class<out EnvironmentHost>): Boolean {
        return Robot::class.java.isAssignableFrom(host)
    }

    fun isRotatable(host: Class<out EnvironmentHost>): Boolean {
        return Rotatable::class.java.isAssignableFrom(host)
    }

    fun isServer(host: Class<out EnvironmentHost>): Boolean {
        return Server::class.java.isAssignableFrom(host)
    }

    fun isTablet(host: Class<out EnvironmentHost>): Boolean {
        return Tablet::class.java.isAssignableFrom(host)
    }

    fun isDrone(host: Class<out EnvironmentHost>): Boolean {
        return Drone::class.java.isAssignableFrom(host)
    }

    override fun getRarity(stack: ItemStack): EnumRarity {
        return OCUtils.getRarityByTier(stack)
    }

    fun isRemote(host: EnvironmentHost): Boolean {
        return host.world()?.isRemote ?: true
    }

    // Mostly stolen from Sangar
    @SideOnly(Side.CLIENT)
    @Suppress("UNCHECKED_CAST")
    override fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        if (isEnabled) {
            super.addInformation(stack, player, tooltip, advanced)
        } else {
            (tooltip as MutableList<String>).add(EnumChatFormatting.RED.toString() + I18nUtil.localize("tooltip.item_disabled"))
        }
    }
}