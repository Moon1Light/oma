package oc.item

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import li.cil.oc.api.driver.item.Chargeable
import net.minecraft.block.Block
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.world.World
import oc.settings
import oc.util.I18nUtil
import oc.util.OCUtils

class ItemScanner : BaseItem(), Chargeable {

    init {
        maxDamage = 100
        setHasSubtypes(true)
    }

    override fun showDurabilityBar(stack: ItemStack) = true

    override fun onItemUse(stack: ItemStack, entityPlayer: EntityPlayer, world: World, x: Int, y: Int, z: Int, side: Int,
                           dx: Float, dy: Float, dz: Float): Boolean {
        if (world.isRemote) {
            return true
        }
        if (getEnergy(stack) < settings.scanner.scanCost) {
            entityPlayer.addChatComponentMessage(I18nUtil.chatComponentText("chat.scanner.no_energy"))
            return true
        }
        setEnergy(stack, getEnergy(stack) - settings.scanner.scanCost)
        val minX: Int = x - settings.scanner.range
        val minY: Int = y - settings.scanner.range
        val minZ: Int = z - settings.scanner.range
        val maxX: Int = x + settings.scanner.range
        val maxY: Int = y + settings.scanner.range
        val maxZ: Int = z + settings.scanner.range
        var notFound = true
        for (ix in minX..maxX) {
            for (iz in minZ..maxZ) {
                for (iy in minY..maxY) {
                    val block: Block = world.getBlock(ix, iy, iz)
                    if (block.localizedName == "Robot.name") {
                        notFound = false
                        entityPlayer.addChatComponentMessage(I18nUtil.chatComponentText(
                                "chat.scanner", mapOf<String, Any>("x" to ix, "y" to iy, "z" to iz)))
                    }
                }
            }
        }
        if (notFound) {
            entityPlayer.addChatComponentMessage(I18nUtil.chatComponentText("chat.scanner.not_found"))
        }
        return true
    }

    override fun getDurabilityForDisplay(stack: ItemStack): Double {
        return (settings.scanner.energyBufferSize - getEnergy(stack)) / settings.scanner.energyBufferSize.toDouble()
    }

    private fun getEnergy(stack: ItemStack): Int {
        if (stack.hasTagCompound() && stack.tagCompound.hasKey("energy")) {
            return stack.tagCompound.getInteger("energy")
        }
        stack.tagCompound = NBTTagCompound().also { it.setInteger("energy", 0) }
        return 0
    }

    private fun setEnergy(stack: ItemStack, energyCount: Int) {
        if (stack.hasTagCompound() && stack.tagCompound.hasKey("energy")) {
            stack.tagCompound.setInteger("energy", energyCount)
        } else {
            stack.tagCompound = NBTTagCompound().also { it.setInteger("energy", energyCount) }
        }
    }

    override fun getSubItems(item: Item, creativeTabs: CreativeTabs?, list: MutableList<in ItemStack>) {
        super.getSubItems(item, creativeTabs, list)
        val charged = ItemStack(this, 1, 0)
        setEnergy(charged, settings.scanner.energyBufferSize)
        list.add(charged)
    }

    override fun charge(stack: ItemStack, amount: Double, simulate: Boolean): Double {
        val energy: Int = settings.scanner.energyBufferSize - getEnergy(stack)
        return if (energy >= amount) {
            setEnergy(stack, (getEnergy(stack) + amount).toInt())
            0.0
        } else {
            setEnergy(stack, settings.scanner.energyBufferSize)
            amount - energy
        }
    }

    override fun canCharge(stack: ItemStack) = true

    @SideOnly(Side.CLIENT)
    override fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        super.addInformation(stack, player, tooltip, advanced)
        val info =I18nUtil.localize("lore.scanner.charge",
                getEnergy(stack).toString(), settings.scanner.energyBufferSize.toString())
        @Suppress("UNCHECKED_CAST") OCUtils.addTooltip(info, tooltip as MutableList<String>)
    }

    override val tooltipData: Array<Any?> = arrayOf(settings.scanner.range)

}