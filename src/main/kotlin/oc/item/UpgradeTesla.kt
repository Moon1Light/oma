package oc.item

import li.cil.oc.api.driver.item.Slot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.ManagedEnvironment
import net.minecraft.item.ItemStack
import oc.driver.DriverTeslaUpgrade
import oc.settings

class UpgradeTesla : BaseItemWithDriver(settings.upgradeTesla) {

    override fun createServerEnvironment(stack: ItemStack, host: EnvironmentHost): ManagedEnvironment? {
        return DriverTeslaUpgrade(host)
    }

    override fun slot(stack: ItemStack): String {
        return Slot.Upgrade
    }

    override fun worksWith(stack: ItemStack, host: Class<out EnvironmentHost>): Boolean {
        return worksWith(stack) && (isRobot(host))
    }

    override fun getEnvironment(stack: ItemStack): Class<*>? {
        return if (worksWith(stack)) DriverTeslaUpgrade::class.java else null
    }

}