package oc.item

import oc.init.regItem
import oc.settings

@Suppress("unused")
object Items {

    val upgradeAdvancedGeolyzer by regItem(true) { UpgradeAdvancedGeolyzer() }
    val upgradeAdvancedSolarPanel by regItem(true) { UpgradeAdvancedSolarPanel() }
    val upgradeHybridSolarPanel by regItem(true) { UpgradeHybridSolarPanel() }
    val upgradeUltimateSolarPanel by regItem(true) { UpgradeUltimateSolarPanel() }
    val upgradeQuantumSolarPanel by regItem(true) { UpgradeQuantumSolarPanel() }
    val upgradeIronInventory by regItem(true) { UpgradeIronInventory() }
    val upgradeDiamondInventory by regItem(true) { UpgradeDiamondInventory() }
    val upgradeTesseract by regItem(true) { UpgradeTesseract() }
    val upgradeAdvancedNavigation by regItem(true) { UpgradeAdvancedNavigation() }
    val upgradeItemCharger by regItem(true) { UpgradeItemCharger() }
    val upgradeEnderlink by regItem(true) { UpgradeEnderlink() }
    val upgradeCropnalyzer by regItem(true) { UpgradeCropnalyzer() }
    val upgradeRadar by regItem(true) { UpgradeRadar() }
    val scanner by regItem(settings.scanner.isEnabled) { ItemScanner() }
    val upgradeTesla by regItem(true) { UpgradeTesla() }
    val industrialMicrocontrollerCase by regItem(settings.industrialMicrocontrollerCase.isEnabled) {
        IndustrialMicrocontrollerCase()
    }
    val industrialDroneCase by regItem(settings.industrialDroneCase.isEnabled) {
        IndustrialDroneCase()
    }
    val industrialRobotCase by regItem(settings.industrialRobotCase.isEnabled) {
        IndustrialRobotCase()
    }
}