package oc

import li.cil.oc.api.event.RobotRenderEvent.MountPoint
import net.minecraft.util.AxisAlignedBB
import org.lwjgl.opengl.GL11

object RenderUtils {

    private val bounds = AxisAlignedBB.getBoundingBox(-0.1, -0.1, -0.1, 0.1,
            0.1, 0.1)

    fun drawSimpleBlock(mountPoint: MountPoint, frontOffset: Float) {
        GL11.glRotatef(mountPoint.rotation.getW(), mountPoint.rotation.getX(), mountPoint.rotation.getY(),
                mountPoint.rotation.getZ())
        GL11.glTranslatef(mountPoint.offset.getX(), mountPoint.offset.getY(), mountPoint.offset.getZ())
        GL11.glBegin(GL11.GL_QUADS)
        // Front.
        GL11.glNormal3f(0f, 0f, 1f)
        GL11.glTexCoord2f(frontOffset, 0.5f)
        GL11.glVertex3d(bounds.minX, bounds.minY, bounds.maxZ)
        GL11.glTexCoord2f(frontOffset + 0.5f, 0.5f)
        GL11.glVertex3d(bounds.maxX, bounds.minY, bounds.maxZ)
        GL11.glTexCoord2f(frontOffset + 0.5f, 0f)
        GL11.glVertex3d(bounds.maxX, bounds.maxY, bounds.maxZ)
        GL11.glTexCoord2f(frontOffset, 0f)
        GL11.glVertex3d(bounds.minX, bounds.maxY, bounds.maxZ)
        // Top.
        GL11.glNormal3f(0f, 1f, 0f)
        GL11.glTexCoord2f(1f, 0.5f)
        GL11.glVertex3d(bounds.maxX, bounds.maxY, bounds.maxZ)
        GL11.glTexCoord2f(1f, 1f)
        GL11.glVertex3d(bounds.maxX, bounds.maxY, bounds.minZ)
        GL11.glTexCoord2f(0.5f, 1f)
        GL11.glVertex3d(bounds.minX, bounds.maxY, bounds.minZ)
        GL11.glTexCoord2f(0.5f, 0.5f)
        GL11.glVertex3d(bounds.minX, bounds.maxY, bounds.maxZ)
        // Bottom.
        GL11.glNormal3f(0f, -1f, 0f)
        GL11.glTexCoord2f(0.5f, 0.5f)
        GL11.glVertex3d(bounds.minX, bounds.minY, bounds.maxZ)
        GL11.glTexCoord2f(0.5f, 1f)
        GL11.glVertex3d(bounds.minX, bounds.minY, bounds.minZ)
        GL11.glTexCoord2f(1f, 1f)
        GL11.glVertex3d(bounds.maxX, bounds.minY, bounds.minZ)
        GL11.glTexCoord2f(1f, 0.5f)
        GL11.glVertex3d(bounds.maxX, bounds.minY, bounds.maxZ)
        // Left.
        GL11.glNormal3f(1f, 0f, 0f)
        GL11.glTexCoord2f(0f, 0.5f)
        GL11.glVertex3d(bounds.maxX, bounds.maxY, bounds.maxZ)
        GL11.glTexCoord2f(0f, 1f)
        GL11.glVertex3d(bounds.maxX, bounds.minY, bounds.maxZ)
        GL11.glTexCoord2f(0.5f, 1f)
        GL11.glVertex3d(bounds.maxX, bounds.minY, bounds.minZ)
        GL11.glTexCoord2f(0.5f, 0.5f)
        GL11.glVertex3d(bounds.maxX, bounds.maxY, bounds.minZ)
        // Right.
        GL11.glNormal3f(-1f, 0f, 0f)
        GL11.glTexCoord2f(0f, 1f)
        GL11.glVertex3d(bounds.minX, bounds.minY, bounds.maxZ)
        GL11.glTexCoord2f(0f, 0.5f)
        GL11.glVertex3d(bounds.minX, bounds.maxY, bounds.maxZ)
        GL11.glTexCoord2f(0.5f, 0.5f)
        GL11.glVertex3d(bounds.minX, bounds.maxY, bounds.minZ)
        GL11.glTexCoord2f(0.5f, 1f)
        GL11.glVertex3d(bounds.minX, bounds.minY, bounds.minZ)
        GL11.glEnd()
    }

}