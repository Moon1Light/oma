package oc.command

import net.minecraft.command.ICommand
import net.minecraft.command.ICommandSender
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.server.MinecraftServer
import oc.block.Blocks
import oc.util.getLookBlockAt


object OMACommand: ICommand {

    override fun addTabCompletionOptions(sender: ICommandSender?, strings: Array<out String>?): MutableList<Any?> {
        return mutableListOf()
    }

    override fun getCommandName(): String {
        return "oma_spawn_antenna"
    }

    override fun getCommandUsage(sender: ICommandSender?): String {
        return "oma.commands.spawn_antenna"
    }

    override fun compareTo(other: Any?): Int {
        return 0
    }

    override fun canCommandSenderUseCommand(sender: ICommandSender?): Boolean {
        return sender is EntityPlayerMP &&
                MinecraftServer.getServer().configurationManager.func_152596_g(sender.gameProfile)
    }

    override fun getCommandAliases(): MutableList<Any?> {
        return arrayListOf("oma_st")
    }

    override fun isUsernameIndex(p_82358_1_: Array<out String>?, p_82358_2_: Int): Boolean {
        return true
    }

    override fun processCommand(sender: ICommandSender?, strings: Array<out String>?) {
        if (sender is EntityPlayerMP) {
            val pos = sender.getLookBlockAt()
            val world = sender.entityWorld
            pos?.let {
                val y = pos.y + 1
                world.setBlock(pos.x, y, pos.z, Blocks.antennaController)
                Blocks.antennaController?.onBlockPlacedBy(world, pos.x, y, pos.z, sender, sender.currentEquippedItem)
                for (i in 1..13) {
                    world.setBlock(pos.x, y + i, pos.z, Blocks.antenna)
                    Blocks.antenna?.onBlockPlacedBy(world, pos.x, y + i, pos.z, sender,
                            sender.currentEquippedItem)
                }
                for (i in 1..2) {
                    world.setBlock(pos.x, y + 13 + i, pos.z, Blocks.antennaCell)
                    Blocks.antenna?.onBlockPlacedBy(world, pos.x, y + 13 + i, pos.z,
                            sender, sender.currentEquippedItem)
                }
            }
        }
    }

}