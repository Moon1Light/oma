package oc

import net.minecraft.util.ResourceLocation

object Resources {

    var IronInventoryTexture: ResourceLocation? = null
    var DiamondInventoryTexture: ResourceLocation? = null
    var modelAntennaTexture: ResourceLocation? = null
    var modelRadar: ResourceLocation? = null
    var servomotorSound: ResourceLocation? = null

    fun init() {
        IronInventoryTexture = ResourceLocation(OMA.MOD_ID.toLowerCase(),
                "textures/models/UpgradeIronInventory.png")
        DiamondInventoryTexture = ResourceLocation(OMA.MOD_ID.toLowerCase(),
                "textures/models/UpgradeDiamondInventory.png")
        modelAntennaTexture = getModel("antenna.png")
        modelRadar = getModel("radar.png")
        servomotorSound = ResourceLocation(OMA.MOD_ID.toLowerCase(), "servomotor")
    }

    private fun getModel(name: String): ResourceLocation {
        return ResourceLocation(OMA.MOD_ID.toLowerCase(), "textures/models/$name")
    }

}