package oc.util

import net.minecraft.init.Blocks
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World
import oc.tile.TileBound

object BoundUtils {

    fun isEmptyBoundBox(world: World, box: AxisAlignedBB, pos: BlockPos): Boolean {
        var y = box.minY.toInt()
        while (y <= box.maxX) {
            var x = box.minX.toInt()
            while (x <= box.maxX) {
                var z = box.minZ.toInt()
                while (z <= box.maxZ) {
                    if (x == 0 && y == 0 && z == 0) {
                        z++
                        continue
                    }
                    val xCoord = pos.x + x
                    val yCoord = pos.y + y
                    val zCoord = pos.z + z
                    val block = world.getBlock(xCoord, yCoord, zCoord)
                    if (block !== Blocks.air && !block.isReplaceable(world, xCoord, yCoord, zCoord)) return false
                    z++
                }
                x++
            }
            y++
        }
        return true
    }

    fun createBoundBox(world: World, box: AxisAlignedBB, pos: BlockPos, metadata: Int) {
        if (!isEmptyBoundBox(world, box, pos)) return
        var y = box.minY.toInt()
        while (y <= box.maxX) {
            var x = box.minX.toInt()
            while (x <= box.maxX) {
                var z = box.minZ.toInt()
                while (z <= box.maxZ) {
                    if (x == 0 && y == 0 && z == 0) {
                        z++
                        continue
                    }
                    val xCoord = pos.x + x
                    val yCoord = pos.y + y
                    val zCoord = pos.z + z
                    world.setBlock(xCoord, yCoord, zCoord, oc.block.Blocks.bound)
                    world.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, metadata, BlockNotifyFlags.ALL)
                    val tileBound = world.getTileEntity(xCoord, yCoord, zCoord) as TileBound
                    tileBound.setRootTile(pos.x, pos.y, pos.z)
                    z++
                }
                x++
            }
            y++
        }
    }

    fun destroyBoundBox(world: World, box: AxisAlignedBB, root: TileEntity) {
        var y = box.minY.toInt()
        while (y <= box.maxX) {
            var x = box.minX.toInt()
            while (x <= box.maxX) {
                var z = box.minZ.toInt()
                while (z <= box.maxZ) {
                    val xCoord = root.xCoord + x
                    val yCoord = root.yCoord + y
                    val zCoord = root.zCoord + z
                    world.setTileEntity(xCoord, yCoord, zCoord, null)
                    world.setBlock(xCoord, yCoord, zCoord, Blocks.air)
                    z++
                }
                x++
            }
            y++
        }
    }

}