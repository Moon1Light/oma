package oc.util

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import li.cil.oc.api.Driver
import li.cil.oc.client.KeyBindings
import li.cil.oc.util.ItemCosts
import net.minecraft.client.Minecraft
import net.minecraft.item.EnumRarity
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.EnumChatFormatting

// derived on
// https://github.com/Vexatos/Computronics/blob/master/src/main/java/pl/asie/computronics/util/OCUtils.java
object OCUtils {
    private const val maxWidth = 220
    private val rarities = arrayOf(EnumRarity.common, EnumRarity.uncommon, EnumRarity.rare, EnumRarity.epic)

    @Suppress("UNUSED")
    fun dataTag(stack: ItemStack): NBTTagCompound {
        if (!stack.hasTagCompound()) {
            stack.tagCompound = NBTTagCompound()
        }
        val nbt = stack.tagCompound
        // This is the suggested key under which to store item component data.
        // You are free to change this as you please.
        if (!nbt.hasKey("oc:data")) {
            nbt.setTag("oc:data", NBTTagCompound())
        }
        return nbt.getCompoundTag("oc:data")
    }

    @SideOnly(Side.CLIENT)
    fun addTooltip(stack: ItemStack, extTip: String?, tooltip: MutableList<String>, tooltipData: Array<Any?>, advanced: Boolean) {
        addTooltip(stack.unlocalizedName + ".tip", extTip, tooltip, tooltipData)
        if (ItemCosts.hasCosts(stack)) {
            if (KeyBindings.showMaterialCosts()) {
                ItemCosts.addTooltip(stack, tooltip)
            } else {
                tooltip.add(
                        I18nUtil.localize(
                                "oc:tooltip.MaterialCosts",
                                KeyBindings.getKeyBindingName(KeyBindings.materialCosts())))
            }
        }
        if (stack.hasTagCompound() && stack.tagCompound.hasKey("oc:data")) {
            val data = stack.tagCompound.getCompoundTag("oc:data")
            if (data.hasKey("node") && data.getCompoundTag("node").hasKey("address")) {
                tooltip.add(
                        EnumChatFormatting.DARK_GRAY
                                .toString() + data.getCompoundTag("node").getString("address").substring(0, 13)
                                + "..."
                                + EnumChatFormatting.GRAY)
            }
        }
        if (advanced) {
            Driver.driverFor(stack)?.let {
                tooltip.add(I18nUtil.localize("oc:tooltip.Tier", it.tier(stack)))
            }
        }
    }

    // Mostly stolen from Sangar
    @SideOnly(Side.CLIENT)
    fun addTooltip(key: String, extTip: String?, tooltip: MutableList<String>, tooltipData: Array<Any?>) {
        run {
            val font = Minecraft.getMinecraft().fontRenderer
            var tip = if (I18nUtil.canTranslate(key)) I18nUtil.localize(key, *tooltipData) else null
            if (extTip != null) {
                tip = if (tip == null) extTip else tip + extTip
            }
            if (tip != null) {
                val lines = tip.split("\n")
                if (font == null) {
                    tooltip.addAll(lines)
                } else {
                    val shouldShorten = font.getStringWidth(tip) > maxWidth && !KeyBindings.showExtendedTooltips()
                    if (shouldShorten) {
                        tooltip.add(
                                I18nUtil.localize(
                                        "oc:tooltip.TooLong",
                                        KeyBindings.getKeyBindingName(KeyBindings.extendedTooltip())))
                    } else {
                        for (line in lines) {
                            val list = font.listFormattedStringToWidth(line, maxWidth)
                            @Suppress("UNCHECKED_CAST") tooltip.addAll(list as List<String>)
                        }
                    }
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    fun addTooltip(info: String, tooltip: MutableList<String>) {
        tooltip.addAll(info.split("\n"))
    }

    fun getRarityByTier(stack: ItemStack): EnumRarity {
        val tier = Driver.driverFor(stack)?.tier(stack)?.coerceAtLeast(0)?.coerceAtMost(rarities.size - 1) ?: 0
        return rarities[tier]
    }

    fun getRarityByTier(tier: Int): EnumRarity {
        return rarities[tier.coerceAtLeast(0).coerceAtMost(rarities.size - 1)]
    }

    object Vendors {
        const val Scrag = "Scrag Technologies"
        const val IC2 = "IndustrialCraft, Inc."
        const val OMATechnologies = "OMA Technologies"
    }
}