package oc.util

import com.google.gson.GsonBuilder
import net.minecraft.nbt.NBTBase
import net.minecraft.nbt.NBTTagCompound

object NBTToJson {

    fun convertToJson(nbt: NBTBase): String {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(NBTTagCompound::class.java, NBTAdapter)
        val json = gsonBuilder.create()
        return json.toJson(nbt, NBTTagCompound::class.java)
    }

}