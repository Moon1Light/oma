package oc.util

import net.minecraft.block.Block
import net.minecraft.util.Vec3
import net.minecraft.world.World

class BlockPos(x: Int, y: Int, z: Int, val world: World?) : Vec3(x.toDouble(), y.toDouble(), z.toDouble()) {
    val block: Block
        get() = world!!.getBlock(x, y, z)

    val x: Int
        get() = xCoord.toInt()

    val y: Int
        get() = yCoord.toInt()

    val z: Int
        get() = zCoord.toInt()

}