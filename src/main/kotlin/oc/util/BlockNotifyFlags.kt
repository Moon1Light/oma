package oc.util

object BlockNotifyFlags {
    const val NONE = 0
    /** Cause a block update  */
    const val BLOCK_UPDATE = 1
    /** Send the change to clients  */
    const val SEND_TO_CLIENTS = 2
    const val ALL = BLOCK_UPDATE or SEND_TO_CLIENTS
    /** prevents the block from being re-rendered, if this is a client world  */
    const val NO_RENDER = 4
}