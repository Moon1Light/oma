package oc.util

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import net.minecraft.nbt.*

object NBTAdapter: TypeAdapter<NBTTagCompound>() {

    private fun writeValue(writer: JsonWriter, value: NBTTagString) {
        writer.value(value.func_150285_a_())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagInt) {
        writer.value(value.func_150287_d())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagByte) {
        writer.value(value.func_150290_f().toInt())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagFloat) {
        writer.value(value.func_150288_h())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagDouble) {
        writer.value(value.func_150286_g())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagShort) {
        writer.value(value.func_150289_e())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagLong) {
        writer.value(value.func_150291_c())
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagIntArray) {
        writer.beginArray()

        value.func_150302_c().forEach {
            writer.value(it)
        }

        writer.endArray()
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagByteArray) {
        writer.beginArray()

        value.func_150292_c().forEach {
            writer.value(it)
        }

        writer.endArray()
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagList) {
        writer.beginArray()

        val field = NBTTagList::class.java.getDeclaredField("tagList")
        field.isAccessible = true

        val list = field.get(value) as ArrayList<*>

        list.forEach {
            writeValue(writer, it as NBTBase)
        }

        writer.endArray()
    }

    private fun writeValue(writer: JsonWriter, value: NBTTagCompound) {
        writer.beginObject()

        value.func_150296_c().forEach { key ->
            run {
                val tag = value.getTag(key as String)
                writer.name(key)
                writeValue(writer, tag)
            }
        }

        writer.endObject()
    }

    private fun writeValue(writer: JsonWriter, value: NBTBase) {
        when(value) {
            is NBTTagCompound -> writeValue(writer, value)
            is NBTTagList -> writeValue(writer, value)
            is NBTTagString -> writeValue(writer, value)
            is NBTTagInt -> writeValue(writer, value)
            is NBTTagByte -> writeValue(writer, value)
            is NBTTagFloat -> writeValue(writer, value)
            is NBTTagDouble -> writeValue(writer, value)
            is NBTTagShort -> writeValue(writer, value)
            is NBTTagLong -> writeValue(writer, value)
            is NBTTagIntArray -> writeValue(writer, value)
            is NBTTagByteArray -> writeValue(writer, value)
        }
    }

    override fun write(writer: JsonWriter?, value: NBTTagCompound?) {
        try {
            value?.let {
                writeValue(writer!!, value)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun read(reader: JsonReader?): NBTTagCompound {
        return NBTTagCompound()
    }
}