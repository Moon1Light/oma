package oc.util

import net.minecraft.util.ChatComponentText
import net.minecraft.util.StatCollector
import org.apache.commons.lang3.text.StrSubstitutor

/**
 * @author Vexatos
 * https://github.com/Vexatos/Computronics/blob/master/src/main/kotlin/pl/asie/computronics/util/StringUtil.java
 */
object I18nUtil {
    fun localize(key: String, vararg formatting: Any?): String {
        return StatCollector.translateToLocalFormatted(key, *formatting).replace("\\n", "\n")
    }

    fun localize(key: String, namedValues: Map<String, Any>): String {
        return StrSubstitutor(namedValues).replace(localize(key))
    }

    fun canTranslate(key: String): Boolean {
        return StatCollector.canTranslate(key)
    }

    fun chatComponentText(key: String, vararg formatting: Any?) : ChatComponentText {
        return ChatComponentText(localize(key, *formatting))
    }

    fun chatComponentText(key: String, namedValues: Map<String, Any>) : ChatComponentText {
        return ChatComponentText(localize(key, namedValues))
    }
}