package oc.util

import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.MovingObjectPosition
import net.minecraft.util.Vec3
import net.minecraft.world.World

fun World.getDimensionId(): Int {
    return this.provider.dimensionId
}

fun EntityPlayer.getLookBlockAt(): BlockPos? {
    val maxDistance = 16
    val world = entityWorld
    val origin = Vec3.createVectorHelper(posX, posY + eyeHeight, posZ)
    val direction = lookVec
    val lookAt = origin.addVector(direction.xCoord * maxDistance,
            direction.yCoord * maxDistance, direction.zCoord * maxDistance)
    when (val result = world.rayTraceBlocks(origin, lookAt)) {
        is MovingObjectPosition -> {
            return BlockPos(result.blockX, result.blockY, result.blockZ, null)
        }
    }

    return null
}