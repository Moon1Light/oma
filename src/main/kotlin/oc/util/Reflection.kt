package oc.util

import oc.OMA

object Reflection {
    fun getInternal(instance: Any, fieldName: String?): Any? {
        return try {
            val field = instance.javaClass.getDeclaredField(fieldName)
            field.isAccessible = true
            field[instance]
        } catch (ignored: IllegalAccessException) {
            OMA.info("IllegalAccessException: $instance $fieldName")
            null
        } catch (ignored: NoSuchFieldException) {
            OMA.info("NoSuchFieldException: $instance $fieldName")
            null
        }
    }

    fun setInternal(
            instance: Any, fieldName: String?, value: Any?) {
        try {
            val field = instance.javaClass.getDeclaredField(fieldName)
            field.isAccessible = true
            field[instance] = value
        } catch (ignored: IllegalAccessException) {
        } catch (ignored: NoSuchFieldException) {
        }
    }
}