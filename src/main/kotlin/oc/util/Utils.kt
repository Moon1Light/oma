package oc.util

import cpw.mods.fml.common.registry.GameData
import li.cil.oc.api.network.Node
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.inventory.IInventory
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.server.MinecraftServer
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.MathHelper
import net.minecraft.util.Vec3
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import java.util.*
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Created by Avaja on 07.05.2016.
 */
object Utils {
    fun getEntitiesInBound(c: Class<*>?, world: World,
                           minX: Int, minY: Int, minZ: Int, maxX: Int, maxY: Int, maxZ: Int): List<*> {
        return world.getEntitiesWithinAABB(c, AxisAlignedBB.getBoundingBox(
                minX.toDouble(), minY.toDouble(), minZ.toDouble(), maxX.toDouble(), maxY.toDouble(), maxZ.toDouble()))
    }

    fun getForgeName(item: Item?): String {
        val items = GameData.getItemRegistry()
        return items.getNameForObject(item)
    }

    fun distance(n1: Node, n2: Node): Double {
        val te1 = n1.host() as? TileEntity
        val te2 = n2.host() as? TileEntity
        if (te1 != null && te2 != null) {
            val dx = (te2.xCoord - te1.xCoord.toDouble()).pow(2.0)
            val dy = (te2.yCoord - te1.yCoord.toDouble()).pow(2.0)
            val dz = (te2.zCoord - te1.zCoord.toDouble()).pow(2.0)
            val res = dx + dy + dz
            return abs(sqrt(res))
        }
        return 0.0
    }

    fun distance(x1: Double, y1: Double, z1: Double, x2: Double, y2: Double, z2: Double): Double {
        val dx = (x2 - x1).pow(2.0)
        val dy = (y2 - y1).pow(2.0)
        val dz = (z2 - z1).pow(2.0)
        val res = dx + dy + dz
        return abs(sqrt(res))
    }

    fun addToIInventory(inventory: IInventory, stack: ItemStack?): Pair<ItemStack?, Boolean> {
        stack?.let {
            val stack = stack.copy()
            val size = inventory.sizeInventory
            val tmp = arrayOfNulls<ItemStack>(inventory.sizeInventory)

            for (i in 0 until inventory.sizeInventory) {
                tmp[i] = inventory.getStackInSlot(i)
            }

            for (i in 0 until size) {
                val inventoryStack = tmp[i]
                inventoryStack?.let {
                    stack?.let {
                        if (inventoryStack.isItemEqual(stack)) {
                            val delta = inventoryStack.maxStackSize - inventoryStack.stackSize
                            if (delta >= stack.stackSize) {
                                inventoryStack.stackSize += stack.stackSize
                                for (q in tmp.indices) {
                                    inventory.setInventorySlotContents(q, tmp[q])
                                }
                                return Pair(stack, true)
                            } else {
                                inventoryStack.stackSize = inventoryStack.maxStackSize
                                stack.stackSize -= delta
                            }
                        }
                    }
                }

                if (inventoryStack == null) {
                    tmp[i] = stack
                    for (q in tmp.indices) {
                        inventory.setInventorySlotContents(q, tmp[q])
                    }
                    return Pair(stack, true)
                }
            }

            return Pair(stack, false)
        }
        return Pair(null, false)
    }

    fun getForgeDirectionId(forgeDirection: ForgeDirection): Int {
        for ((id, direction) in ForgeDirection.values().withIndex()) {
            if (direction == forgeDirection) return id
        }
        return 7
    }

    fun getDirection(side: Int): ForgeDirection? {
        when (side) {
            0 -> return ForgeDirection.SOUTH
            1 -> return ForgeDirection.WEST
            2 -> return ForgeDirection.NORTH
            3 -> return ForgeDirection.EAST
        }
        return null
    }

    fun getLookDirection(livingBase: EntityLivingBase): ForgeDirection? {
        val side = (MathHelper.floor_double((livingBase.rotationYaw * 4.0f / 360.0f).toDouble() + 0.5) and 3.toDouble().toInt()).toDouble()
        when (side.toInt()) {
            0 -> return ForgeDirection.SOUTH
            1 -> return ForgeDirection.WEST
            2 -> return ForgeDirection.NORTH
            3 -> return ForgeDirection.EAST
        }
        return null
    }

    fun getLook(livingBase: EntityLivingBase): Vec3 {
        val f1: Float = MathHelper.cos(-livingBase.rotationYaw * 0.017453292f - Math.PI.toFloat())
        val f2: Float = MathHelper.sin(-livingBase.rotationYaw * 0.017453292f - Math.PI.toFloat())
        val f3: Float = -MathHelper.cos(-livingBase.rotationPitch * 0.017453292f)
        val f4: Float = MathHelper.sin(-livingBase.rotationPitch * 0.017453292f)
        return Vec3.createVectorHelper(f2 * f3.toDouble(), f4.toDouble(), f1 * f3.toDouble())
    }

    fun addToIInventory(inventory: IInventory, min: Int, stack: ItemStack): Boolean {
        val size = inventory.sizeInventory
        val tmp = arrayOfNulls<ItemStack>(inventory.sizeInventory)
        for (i in 0 until inventory.sizeInventory) {
            tmp[i] = inventory.getStackInSlot(i)
        }
        for (i in min until size) {
            val inventoryStack = tmp[i]
            if (inventoryStack != null && inventoryStack.isItemEqual(stack)) {
                val delta = inventoryStack.maxStackSize - inventoryStack.stackSize
                if (delta >= stack.stackSize) {
                    inventoryStack.stackSize += stack.stackSize
                    for (q in tmp.indices) {
                        inventory.setInventorySlotContents(q, tmp[q])
                    }
                    return true
                } else {
                    inventoryStack.stackSize = inventoryStack.maxStackSize
                    stack.stackSize -= delta
                }
            } else if (inventoryStack == null) {
                tmp[i] = stack
                for (q in tmp.indices) {
                    inventory.setInventorySlotContents(q, tmp[q])
                }
                return true
            }
        }
        return false
    }

    fun createStringTable(vararg objects: Any?): HashMap<*, *>? {
        if (objects.size >= 2) {
            val hashMap = HashMap<String, Any>()
            var i = 0
            while (i < objects.size) {
                hashMap[objects[i] as String] = objects[i + 1] ?: ""
                i += 2
            }
            return hashMap
        }
        return null
    }

    fun createIndexTable(vararg objects: Any): Map<*, *>? {
        if (objects.isNotEmpty()) {
            val hashMap = HashMap<Int, Any>()
            for (i in objects.indices) {
                hashMap[i] = objects[i]
            }
            return hashMap
        }
        return null
    }

    fun findPlayer(name: String): EntityPlayerMP? {
        val iterator: Iterator<*> = MinecraftServer.getServer().configurationManager.playerEntityList.iterator()
        while (iterator.hasNext()) {
            val player = iterator.next() as EntityPlayerMP
            if (player.commandSenderName == name) return player
        }
        return null
    }
}