package oc.util

import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.DamageSource

class TeslaDamage(val player: EntityPlayer) : DamageSource("electricity")