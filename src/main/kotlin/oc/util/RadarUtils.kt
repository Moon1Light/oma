package oc.util

import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLiving
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World
import kotlin.math.*

/** Created by Avaja on 21.05.2016. Edited by svitoos on 15.02.2020.  */
object RadarUtils {
    fun getEntities(world: World, cx: Double, cy: Double, cz: Double,
                    radius: Double, height: Double, yaw: Double, angle: Double,
                    eClass: Class<out Entity?>?): List<Map<String, Any>> {
        val entities = mutableListOf<Map<String, Any>>()
        val vz = radius * cos(yaw)
        val vx = radius * sin(yaw)
        val f1 = yaw + angle / 2
        val f2 = yaw - angle / 2
        val v1z = radius * cos(f1)
        val v1x = radius * sin(f1)
        val v2z = radius * cos(f2)
        val v2x = radius * sin(f2)
        val minX = min(min(min(v1x, v2x), vx), 0.0)
        val minZ = min(min(min(v1z, v2z), vz), 0.0)
        val maxX = max(max(max(v1x, v2x), vx), 0.0)
        val maxZ = max(max(max(v1z, v2z), vz), 0.0)
        val cosAngle = cos(angle / 2)
        val bounds = AxisAlignedBB.getBoundingBox(
                cx + minX, cy - 3, cz + minZ, cx + maxX, cy + height, cz + maxZ)
        for (entity in world.getEntitiesWithinAABB(eClass, bounds)) {
            if (entity is Entity) {
                val dx = entity.posX - cx
                val dy = entity.posY - cy
                val dz = entity.posZ - cz
                val eDistSq = dx * dx + dz * dz
                val vDistSq = vx * vx + vz * vz
                if (dy > -3 && dy < height && eDistSq <= vDistSq && (dx * vx + dz * vz) / (sqrt(eDistSq) * sqrt(vDistSq)) >= cosAngle) {
                    val entry = mutableMapOf<String, Any>()
                    entry["x"] = dx
                    entry["y"] = dy
                    entry["z"] = dz
                    addData(entity, entry)
                    entities.add(entry)
                }
            }
        }
        return entities
    }

    fun getEntities(
            world: World, cx: Double, cy: Double, cz: Double, radius: Int, eClass: Class<out Entity?>?): List<Map<String, Any>> {
        val entities = mutableListOf<Map<String, Any>>()
        val bounds = AxisAlignedBB.getBoundingBox(
                cx - radius, cy - radius, cz - radius, cx + radius, cy + radius, cz + radius)
        for (entity in world.getEntitiesWithinAABB(eClass, bounds)) {
            if (entity is Entity) {
                val dx = entity.posX - cx
                val dy = entity.posY - cy
                val dz = entity.posZ - cz
                val distance = sqrt(dx * dx + dy * dy + dz * dz)
                if (distance < radius) {
                    val entry = mutableMapOf<String, Any>()
                    entry["x"] = dx
                    entry["y"] = dy
                    entry["z"] = dz
                    addData(entity, entry)
                    entities.add(entry)
                }
            }
        }
        return entities
    }

    private fun addData(entity: Entity, entry: MutableMap<String, Any>) {
        if (entity is EntityLivingBase) {
            entry["health"] = entity.health
            entry["maxHealth"] = entity.maxHealth
            entry["armor"] = entity.totalArmorValue
            entry["height"] = entity.height
            entry["eyeHeight"] = entity.eyeHeight
            if (entity is EntityPlayer) {
                entity.displayName?.let { entry["name"] = it }
            } else if (entity is EntityLiving) {
                if (entity.hasCustomNameTag()) {
                    entry["name"] = entity.customNameTag
                }
            }
            if (!entry.containsKey("name")) {
                entry["name"] = entity.getCommandSenderName()
            }
        } else if (entity is EntityItem) {
            entry["item"] = entity.entityItem
        }
    }
}