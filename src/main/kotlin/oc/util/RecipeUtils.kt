package oc.util

import cpw.mods.fml.common.registry.GameRegistry
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.oredict.ShapedOreRecipe
import net.minecraftforge.oredict.ShapelessOreRecipe
import oc.OMA

object RecipeUtils {
    fun addShapedRecipe(result: ItemStack?, vararg recipe: Any?) {
        if (validate(recipe, result))
            GameRegistry.addRecipe(ShapedOreRecipe(result, *recipe))
    }

    @Suppress("UNUSED")
    fun addShapelessRecipe(result: ItemStack?, vararg recipe: Any?) {
        if (validate(recipe, result))
            GameRegistry.addRecipe(ShapelessOreRecipe(result, *recipe))
    }

    private fun validate(recipe: Array<out Any?>, result: ItemStack?): Boolean {
        if (result?.item == null) {
            warnCrafting(result, recipe)
            return false
        }
        for (o in recipe) {
            if (
                    o == null ||
                    (o is Block && Block.getIdFromBlock(o) < 0) ||
                    (o is Item && Item.getIdFromItem(o) < 0) ||
                    (o is ItemStack && o.item == null)
            ) {
                warnCrafting(result, recipe)
                return false
            }
        }
        return true
    }

    private fun warnCrafting(result: ItemStack?, recipe: Array<out Any?>) {
        OMA.warn("Invalid recipe: {} -> {}",
                recipe.toList().map { if (it is ItemStack && it.item == null) null else it }, result
        )
    }

    fun getItem(id: String): Item? {
        return Item.itemRegistry.getObject(id) as? Item
    }

    fun makeItemStack(id: String, meta: Int): ItemStack? {
        return getItem(id)?.let { ItemStack(it, 1, meta) }
    }

    fun makeItemStackWithWildCard(name: String): ItemStack? {
        return makeItemStack(name, Short.MAX_VALUE.toInt())
    }
}