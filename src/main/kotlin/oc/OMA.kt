package oc

import cpw.mods.fml.common.Mod
import cpw.mods.fml.common.ModMetadata
import cpw.mods.fml.common.SidedProxy
import cpw.mods.fml.common.event.*
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import oc.command.OMACommand
import oc.driver.UpgradeEnderlink
import oc.item.Items
import oc.proxy.CommonProxy
import oc.tile.TileChatbox
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.Logger

@Mod(modid = OMA.MOD_ID, useMetadata = true,
        dependencies = "required-after:OpenComputers@[1.7.5,);" +
                "after:IC2@[2.2.827,);" +
                "required-after:CodeChickenCore@[1.0.7,);" +
                "after:Waila@[1.5.10,);" +
                "after:Thaumcraft")
object OMA {

    const val MOD_ID = "oma"
    val MOD_NAME by lazy { modMetadata!!.name!! }

    lateinit var creativeTab: CreativeTabs
    internal lateinit var logger: Logger

    @JvmStatic
    @Mod.InstanceFactory
    internal fun instance() = OMA

    @JvmField
    @Mod.Metadata
    internal var modMetadata: ModMetadata? = null

    @JvmStatic
    @SidedProxy(clientSide = "oc.proxy.ClientProxy", serverSide = "oc.proxy.ServerProxy")
    private lateinit var proxy: CommonProxy

    fun info(message: String, vararg params: Any?) {
        logger.log(Level.INFO, message, *params)
    }

    fun warn(message: String, vararg params: Any?) {
        logger.log(Level.WARN, message, *params)
    }

    @Mod.EventHandler
    fun preLoad(event: FMLPreInitializationEvent) {
        logger = event.modLog
        Settings.init(event.suggestedConfigurationFile)
        creativeTab = object : CreativeTabs(MOD_NAME) {
            override fun getTabIconItem(): Item {
                return Items.upgradeEnderlink!!
            }
        }
        proxy.preLoad(event)
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent?) {
        proxy.init(event)
    }

    @Suppress("UNUSED_PARAMETER")
    @Mod.EventHandler
    fun postInit(event: FMLPostInitializationEvent?) {
        proxy.postInit(event)
    }

    @Mod.EventHandler
    fun serverStarting(event: FMLServerStartingEvent?) {
        event?.registerServerCommand(OMACommand)
    }

    @Suppress("UNUSED_PARAMETER")
    @Mod.EventHandler
    fun serverStart(event: FMLServerAboutToStartEvent?) {
        UpgradeEnderlink.init()
        TileChatbox.init()
    }

    @Suppress("UNUSED_PARAMETER")
    @Mod.EventHandler
    fun serverStop(event: FMLServerStoppedEvent?) {
        UpgradeEnderlink.cleanup()
        TileChatbox.cleanup()
    }

}

