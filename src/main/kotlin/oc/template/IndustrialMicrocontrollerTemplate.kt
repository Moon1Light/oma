package oc.template

import li.cil.oc.Settings
import li.cil.oc.api.IMC
import li.cil.oc.api.internal.Microcontroller
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.common.Slot
import li.cil.oc.common.Tier
import li.cil.oc.common.template.MicrocontrollerTemplate
import li.cil.oc.common.template.Template
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import oc.item.Items
import org.apache.commons.lang3.tuple.Pair
import scala.Function1
import scala.Tuple2

internal object IndustrialMicrocontrollerTemplate : Template() {

    override fun hostClass(): Class<out EnvironmentHost> = Microcontroller::class.java

    override fun caseTier(inventory: IInventory): Int =
            inventory.getStackInSlot(0)?.let { if (select(it)) Tier.Two() else Tier.None() } ?: Tier.None()

    override fun suggestedComponents(): Array<Tuple2<String, Function1<IInventory, Any>>> = emptyArray()

    override fun maxComplexity(inventory: IInventory): Int = 16

    @Suppress("UNUSED")
    @JvmStatic
    fun select(stack: ItemStack): Boolean = Items.industrialMicrocontrollerCase === stack.item

    @Suppress("UNUSED")
    @JvmStatic
    fun validate(inventory: IInventory): Array<Any> = validateComputer(inventory)

    @Suppress("UNUSED")
    @JvmStatic
    fun assemble(inventory: IInventory): Array<Any> = MicrocontrollerTemplate.assemble(inventory).also {
        (it[0] as? ItemStack)?.tagCompound?.setByte(Settings.namespace() + "tier", Tier.Two().toByte())
    }

    fun register() {
        IMC.registerAssemblerTemplate(
                "Industrial Microcontroller",
                "oc.template.IndustrialMicrocontrollerTemplate.select",
                "oc.template.IndustrialMicrocontrollerTemplate.validate",
                "oc.template.IndustrialMicrocontrollerTemplate.assemble",
                hostClass(),
                null,
                intArrayOf(
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three()
                ),
                listOf(
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.CPU(), Tier.One()),
                        Pair.of(Slot.Memory(), Tier.One()),
                        Pair.of(Slot.Memory(), Tier.One()),
                        Pair.of(Slot.EEPROM(), Tier.Any())
                )
        )
    }

}