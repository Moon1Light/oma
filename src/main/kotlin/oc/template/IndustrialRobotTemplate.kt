package oc.template

import li.cil.oc.Settings
import li.cil.oc.api.Driver
import li.cil.oc.api.IMC
import li.cil.oc.api.internal.Robot
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.common.Slot
import li.cil.oc.common.Tier
import li.cil.oc.common.template.RobotTemplate
import li.cil.oc.common.template.Template
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import oc.item.Items
import org.apache.commons.lang3.tuple.Pair

internal object IndustrialRobotTemplate : Template() {

    override fun hostClass(): Class<out EnvironmentHost> = Robot::class.java

    override fun caseTier(inventory: IInventory?): Int = Tier.Three()

    @Suppress("UNUSED")
    @JvmStatic
    fun select(stack: ItemStack): Boolean = Items.industrialRobotCase === stack.item

    override fun maxComplexity(inventory: IInventory): Int {
        for (slot in 0 until inventory.sizeInventory - 1) {
            val stack = inventory.getStackInSlot(slot)
            (Driver.driverFor(stack, hostClass()) as? li.cil.oc.api.driver.item.Processor)?.let {
                if (it.tier(stack) >= Tier.Three()) {
                    return 64
                }
            }
        }
        return 0
    }

    @Suppress("UNUSED")
    @JvmStatic
    fun validate(inventory: IInventory): Array<Any> = validateComputer(inventory)

    @Suppress("UNUSED")
    @JvmStatic
    fun assemble(inventory: IInventory): Array<Any> = RobotTemplate.assemble(inventory).also {
        (it[0] as? ItemStack)?.tagCompound?.setByte(Settings.namespace() + "tier", Tier.Three().toByte())
    }

    fun register() {
        IMC.registerAssemblerTemplate(
                "Industrial Robot",
                "oc.template.IndustrialRobotTemplate.select",
                "oc.template.IndustrialRobotTemplate.validate",
                "oc.template.IndustrialRobotTemplate.assemble",
                hostClass(),
                intArrayOf(
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three()
                ),
                intArrayOf(
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three(),
                        Tier.Three()
                ),
                listOf(
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.Card(), Tier.Three()),
                        Pair.of(Slot.CPU(), Tier.Three()),
                        Pair.of(Slot.Memory(), Tier.Three()),
                        Pair.of(Slot.Memory(), Tier.Three()),
                        Pair.of(Slot.EEPROM(), Tier.Any()),
                        Pair.of(Slot.HDD(), Tier.Three())
                )
        )
    }

}