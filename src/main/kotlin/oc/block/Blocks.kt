package oc.block

import oc.init.regBlock
import oc.settings

@Suppress("unused")
object Blocks {

    val antenna by regBlock(settings.antenna.isEnabled) { BlockAntenna() }
    val antennaCell by regBlock(settings.antenna.isEnabled) { BlockAntennaCell() }
    val antennaController by regBlock(settings.antenna.isEnabled) { BlockAntennaController() }
    val chatbox by regBlock(settings.chatbox.isEnabled) { BlockChatBox() }
    val creativeChatbox by regBlock(settings.creativeChatbox.isEnabled) { BlockCreativeChatBox() }
    val worldInterface by regBlock(settings.worldInterface.isEnabled) { BlockWorldInterface() }
    val radar by regBlock(settings.blockRadar.isEnabled) { BlockRadar() }
    val bound by regBlock(true) { BlockBound() }
    val solarPanel by regBlock(settings.blockSolarPanel.isEnabled) { BlockSolarPanel() }
    val lightingGenerator by regBlock(false /*TODO*/) { BlockLightingGenerator() }
    val furnaceGenerator by regBlock(true) { BlockFurnaceGenerator() }

}