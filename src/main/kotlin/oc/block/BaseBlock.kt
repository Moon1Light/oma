package oc.block

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import oc.OMA
import oc.util.OCUtils

abstract class BaseBlock protected constructor (material: Material) : Block(material) {

    init {
        @Suppress("LeakingThis") setCreativeTab(OMA.creativeTab)
        @Suppress("LeakingThis") setHarvestLevel("pickaxe", 0)
        @Suppress("LeakingThis") setHardness(3.5f)
        @Suppress("LeakingThis") setResistance(8f)
    }

    override fun setBlockTextureName(textureName: String): Block {
        return super.setBlockTextureName(OMA.MOD_ID.toLowerCase() + ":" + textureName)
    }

    // Mostly stolen from Sangar
    @SideOnly(Side.CLIENT)
    @Suppress("UNCHECKED_CAST")
    open fun addInformation(stack: ItemStack, player: EntityPlayer, tooltip: MutableList<*>, advanced: Boolean) {
        OCUtils.addTooltip(stack, extendedTooltip, tooltip as MutableList<String>, tooltipData, advanced)
    }

    open val extendedTooltip: String?
        get() = null

    open val tooltipData: Array<Any?>
        get() = arrayOf()
}