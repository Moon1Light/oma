package oc.block

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.World

class BlockAntennaCell : BlockAntenna() {
    init {
        setBlockTextureName("antennaCell")
    }

    override fun isOpaqueCube(): Boolean {
        return false
    }

    override fun isNormalCube(): Boolean {
        return false
    }

    @SideOnly(Side.CLIENT)
    override fun getCollisionBoundingBoxFromPool(world: World, x: Int, y: Int, z: Int): AxisAlignedBB {
        val border = 0.1625
        return AxisAlignedBB.getBoundingBox(x + border, y.toDouble(), z + border,
                x + 1 - border, y + 1.toDouble(), z + 1 - border)
    }

    override fun getSelectedBoundingBoxFromPool(world: World, x: Int, y: Int, z: Int): AxisAlignedBB {
        val meta = world.getBlockMetadata(x, y, z)
        if (meta == 1) {
            val border = 0.1
            return AxisAlignedBB.getBoundingBox(x + border, y.toDouble(), z + border,
                    x + 1 - border, y + 1.toDouble(), z + 1 - border)
        }
        return AxisAlignedBB.getBoundingBox(x.toDouble(), y.toDouble(), z.toDouble(), x + 1.toDouble(),
                y + 1.toDouble(), z + 1.toDouble())
    }

}