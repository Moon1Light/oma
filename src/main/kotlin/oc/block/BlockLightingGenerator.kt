package oc.block

import net.minecraft.block.material.Material
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import oc.tile.TileLightingGenerator

class BlockLightingGenerator: BaseTileBlock(Material.iron) {

    init {
        setBlockName("lighting_generator")
        setBlockTextureName("lighting_generator")
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity? {
        return TileLightingGenerator()
    }

}