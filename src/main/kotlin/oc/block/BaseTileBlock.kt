package oc.block

import net.minecraft.block.Block
import net.minecraft.block.ITileEntityProvider
import net.minecraft.block.material.Material
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World

abstract class BaseTileBlock protected constructor(material: Material) : BaseBlock(material), ITileEntityProvider {

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity? {
        return null
    }

    override fun breakBlock(world: World, x: Int, y: Int, z: Int, block: Block, metadata: Int) {
        world.removeTileEntity(x, y, z)
    }

    override fun onBlockEventReceived(world: World, x: Int, y: Int, z: Int, eventId: Int, eventParam: Int): Boolean {
        return world.getTileEntity(x, y, z)?.receiveClientEvent(eventId, eventParam) ?: false
    }
}