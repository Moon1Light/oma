package oc.block

import li.cil.oc.api.network.Environment
import net.minecraft.block.material.Material
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import oc.tile.TileWorldInterface
import java.util.*

class BlockWorldInterface : BaseTileBlock(Material.iron), IEnvironmentBlock {

    init {
        setBlockTextureName("worldInterface")
    }

    override fun randomDisplayTick(world: World, x: Int, y: Int, z: Int, random: Random) {
        for (l in 0..2) {
            //var d6 = x.toFloat() + random.nextFloat().toDouble()
            val particleY = y.toFloat() + random.nextFloat().toDouble()
            //d6 = z.toFloat() + random.nextFloat().toDouble()
            var particleMovingX: Double
            var particleMovingY: Double
            var particleMovingZ: Double
            val i1 = random.nextInt(2) * 2 - 1
            val j1 = random.nextInt(2) * 2 - 1
            //particleMovingX = (random.nextFloat().toDouble() - 0.5) * 0.125
            particleMovingY = (random.nextFloat().toDouble() - 0.5) * 0.125
            //particleMovingZ = (random.nextFloat().toDouble() - 0.5) * 0.125
            val particleZ = z.toDouble() + 0.5 + 0.25 * j1.toDouble()
            particleMovingZ = random.nextFloat() * 1.0f * j1.toDouble()
            val particleX = x.toDouble() + 0.5 + 0.25 * i1.toDouble()
            particleMovingX = random.nextFloat() * 1.0f * i1.toDouble()
            world.spawnParticle("mobSpell", particleX, particleY, particleZ, particleMovingX, particleMovingY, particleMovingZ)
        }
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileWorldInterface()
    }

    override fun getTileEntityClass(meta: Int): Class<out Environment?>? = TileWorldInterface::class.java
}