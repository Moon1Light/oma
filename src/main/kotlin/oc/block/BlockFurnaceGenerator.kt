package oc.block

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.client.renderer.texture.IIconRegister
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.IIcon
import net.minecraft.util.MathHelper
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import oc.OMA
import oc.tile.TileFurnaceGenerator
import oc.util.BlockNotifyFlags

class BlockFurnaceGenerator internal constructor() : BaseTileBlock(Material.iron) {

    init {
        setHardness(5f)
    }

    @SideOnly(Side.CLIENT)
    private lateinit var iconFront: IIcon
    @SideOnly(Side.CLIENT)
    private lateinit var iconSide: IIcon
    @SideOnly(Side.CLIENT)
    private lateinit var iconTop: IIcon
    @SideOnly(Side.CLIENT)
    private lateinit var iconBurn: IIcon

    @SideOnly(Side.CLIENT)
    override fun registerBlockIcons(iconRegister: IIconRegister) {
        iconFront = iconRegister.registerIcon(OMA.MOD_ID + ":" + "generator_front")
        iconSide = iconRegister.registerIcon(OMA.MOD_ID + ":" + "generator_side")
        iconTop = iconRegister.registerIcon(OMA.MOD_ID + ":" + "generator_top")
        iconBurn = iconRegister.registerIcon(OMA.MOD_ID + ":" + "generator_burn")
    }

    @SideOnly(Side.CLIENT)
    override fun getIcon(side: Int, metadata: Int): IIcon {
        val iFront = if (metadata and 0x8 != 0) iconBurn else iconFront
        val d = metadata and 0x3
        return when {
            side == 0 || side == 1 -> iconTop
            d == 2 && side == 2 -> iFront
            d == 3 && side == 5 -> iFront
            d == 0 && side == 3 -> iFront
            d == 1 && side == 4 -> iFront
            else -> iconSide
        }
    }

    override fun onBlockActivated(world: World, x: Int, y: Int, z: Int, player: EntityPlayer,
                                  par6: Int, par7: Float, par8: Float, par9: Float): Boolean {
        player.openGui(OMA, 0, world, x, y, z)
        return true
    }

    override fun onBlockPlacedBy(world: World, x: Int, y: Int, z: Int, entity: EntityLivingBase, itemStack: ItemStack) {
        val i = ((MathHelper.floor_double((entity.rotationYaw * 4.0f / 360.0f).toDouble() + 2.5) and 3)
                or (world.getBlockMetadata(x, y, z) and 0x8))
        world.setBlockMetadataWithNotify(x, y, z, i, BlockNotifyFlags.SEND_TO_CLIENTS)
    }

    override fun breakBlock(world: World, x: Int, y: Int, z: Int, block: Block, metadata: Int) {
        if (metadata and 0x8 != 0) {
            (world.getTileEntity(x, y, z) as? TileFurnaceGenerator)?.onBreak()
        }
        super.breakBlock(world, x, y, z, block, metadata)
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileFurnaceGenerator()
    }

    override fun getLightValue(world: IBlockAccess, x: Int, y: Int, z: Int): Int {
        return if (world.getBlock(x, y, z) === this && world.getBlockMetadata(x, y, z) and 0x8 != 0) {
            13
        } else super.getLightValue(world, x, y, z)
    }

}