package oc.block

import cpw.mods.fml.relauncher.Side
import cpw.mods.fml.relauncher.SideOnly
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.client.renderer.texture.IIconRegister
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.IIcon
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import oc.OMA
import oc.init.Render
import oc.tile.TileAntennaController
import kotlin.math.max

open class BlockAntenna : BaseBlock(Material.iron) {
    private val sides = arrayOfNulls<IIcon>(6)

    init {
        @Suppress("LeakingThis") setBlockTextureName("antenna")
    }

    override fun getRenderType(): Int {
        return Render.renderIdAntenna
    }

    override fun registerBlockIcons(registry: IIconRegister) {
        for (i in sides.indices) sides[i] = registry.registerIcon(getTextureName())
        sides[0] = registry.registerIcon(OMA.MOD_ID + ":antennaCable")
        sides[1] = registry.registerIcon(OMA.MOD_ID + ":antennaCable")
    }

    override fun getIcon(side: Int, metadata: Int): IIcon {
        return sides[side]!!
    }

    override fun isOpaqueCube(): Boolean {
        return false
    }

    override fun isNormalCube(): Boolean {
        return false
    }

    override fun isBlockSolid(world: IBlockAccess, x: Int, y: Int, z: Int, side: Int): Boolean {
        return side == 0 || side == 1
    }

    @SideOnly(Side.CLIENT)
    override fun getCollisionBoundingBoxFromPool(world: World, x: Int, y: Int, z: Int): AxisAlignedBB {
        val border = 0.1625
        return AxisAlignedBB.getBoundingBox(x + border, y.toDouble(), z + border,
                x + 1 - border, y + 1.toDouble(), z + 1 - border)
    }

    override fun getSelectedBoundingBoxFromPool(world: World, x: Int, y: Int, z: Int): AxisAlignedBB {
        val meta = world.getBlockMetadata(x, y, z)
        if (meta == 1) {
            val border = 0.1
            return AxisAlignedBB.getBoundingBox(x + border, y.toDouble(), z + border,
                    x + 1 - border, y + 1.toDouble(), z + 1 - border)
        }
        return AxisAlignedBB.getBoundingBox(x.toDouble(), y.toDouble(), z.toDouble(), x + 1.toDouble(),
                y + 1.toDouble(), z + 1.toDouble())
    }

    override fun onEntityCollidedWithBlock(world: World, x: Int, y: Int, z: Int, entity: Entity) {
        if (entity is EntityPlayer) {
            // prevent fall damage
            entity.fallDistance = 0f
            // make it a slow decent
            entity.motionY = max(entity.motionY, -0.15)
        }
    }

    override fun onBlockPlacedBy(world: World, x: Int, y: Int, z: Int, entity: EntityLivingBase, stack: ItemStack?) { // search for the controller and notify of placement
        for (i in 0..15) {
            if (notifyController(world, x, y - i, z, true)) {
                break
            }
        }
        super.onBlockPlacedBy(world, x, y, z, entity, stack)
    }

    override fun breakBlock(world: World, x: Int, y: Int, z: Int, block: Block, meta: Int) { // search for the controller and notify of destruction
        for (i in 0..15) {
            if (notifyController(world, x, y - i, z, false)) {
                break
            }
        }
        super.breakBlock(world, x, y, z, block, meta)
    }

    private fun notifyController(world: World, x: Int, y: Int, z: Int, added: Boolean): Boolean {
        if (!world.isRemote) {
            val tile = world.getTileEntity(x, y, z)
            tile?.let {
                if (tile is TileAntennaController) {
                    if (added) {
                        tile.onBlockAdded()
                    } else {
                        tile.onBlockRemoved()
                    }
                    return true
                }
            }
        }
        return false
    }

}