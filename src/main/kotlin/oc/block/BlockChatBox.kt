package oc.block

import li.cil.oc.api.network.Environment
import net.minecraft.block.material.Material
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import oc.settings
import oc.tile.TileChatbox

open class BlockChatBox : BaseTileBlock(Material.iron), IEnvironmentBlock {

    init {
        @Suppress("LeakingThis") setBlockTextureName("chatbox")
        @Suppress("LeakingThis") setHarvestLevel("pickaxe", 0)
        @Suppress("LeakingThis") setHardness(5f)
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileChatbox()
    }

    override val tooltipData: Array<Any?>
        get() = with(settings.chatbox) { arrayOf(maxMessageLength, maxMessageRadius) }

    override fun getTileEntityClass(meta: Int): Class<out Environment?>? = TileChatbox::class.java
}