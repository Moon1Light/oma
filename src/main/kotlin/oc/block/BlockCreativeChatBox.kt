package oc.block

import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import oc.tile.TileCreativeChatbox

class BlockCreativeChatBox : BlockChatBox() {

    init {
        setBlockTextureName("creativeChatbox")
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileCreativeChatbox()
    }

}