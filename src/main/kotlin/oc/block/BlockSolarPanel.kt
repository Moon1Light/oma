package oc.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.ChatComponentText
import net.minecraft.world.World
import oc.OCSettings
import oc.init.IBlockCustomItem
import oc.init.Render
import oc.item.ItemSolarPanel
import oc.settings
import oc.tile.TileSolarPanel
import oc.util.BlockPos
import oc.util.BoundUtils

class BlockSolarPanel : BaseTileBlock(Material.piston), IBlockCustomItem {

    init {
        setBlockTextureName("SolarPanelTop")
        BlockBound.addMapping(this, 1)
    }

    override fun renderAsNormalBlock(): Boolean {
        return false
    }

    override fun getRenderType(): Int {
        return Render.renderSolarPanel
    }

    override fun isOpaqueCube(): Boolean {
        return false
    }

    override fun onBlockActivated(world: World, x: Int, y: Int, z: Int, entityPlayer: EntityPlayer, meta: Int,
                                  xOffset: Float, yOffset: Float, zOffset: Float): Boolean {
        if(world.isRemote) return false

        val currentStack = entityPlayer.inventory.getCurrentItem()
        if (currentStack != null && currentStack.item == OCSettings.ocItem) {
            if (currentStack.itemDamage == OCSettings.wirelessNetworkCard1Item ||
                    currentStack.itemDamage == OCSettings.wirelessNetworkCard2Item) {
                if (currentStack.hasTagCompound()) {
                    val sourceAddress = currentStack.tagCompound.getCompoundTag("oc:data")
                            .getCompoundTag("node").getString("address")
                    val solarPanel = world.getTileEntity(x, y, z) as TileSolarPanel

                    if (!solarPanel.hasNetworkAddress()) {
                        entityPlayer.
                        addChatComponentMessage(ChatComponentText("[${localizedName}] §2attaching successful"))
                        solarPanel.setSourceAddress(sourceAddress)
                    } else {
                        entityPlayer.
                        addChatComponentMessage(ChatComponentText("[${localizedName}] §4was attached"))
                    }
                }
            }
        }
        return true
    }

    override fun canPlaceBlockAt(world: World?, x: Int, y: Int, z: Int): Boolean {
        val axisAlignedBB = AxisAlignedBB.getBoundingBox(-1.0, 0.0, -1.0,
                1.0, 1.0, 1.0)
        return BoundUtils.isEmptyBoundBox(world!!, axisAlignedBB, BlockPos(x, y, z, null)) &&
                super.canPlaceBlockAt(world, x, y, z)
    }

    override fun onBlockAdded(world: World, x: Int, y: Int, z: Int) {
        (world.getTileEntity(x, y, z) as TileSolarPanel).onPlace()
    }

    override fun breakBlock(world: World, x: Int, y: Int, z: Int, block: Block, metadata: Int) {
        val solarPanel = world.getTileEntity(x, y, z) as TileSolarPanel
        solarPanel.onBreak()
        super.breakBlock(world, x, y, z, block, metadata)
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileSolarPanel()
    }

    override val tooltipData: Array<Any?>
        get() = with(settings.blockSolarPanel) { arrayOf(energyPerTick, maxPitch) }

    override val customItem = ItemSolarPanel::class.java

}