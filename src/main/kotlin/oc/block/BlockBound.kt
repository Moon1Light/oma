package oc.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.IIcon
import net.minecraft.util.MovingObjectPosition
import net.minecraft.world.World
import oc.init.IHidden
import oc.tile.TileBound
import java.util.*

class BlockBound : BaseTileBlock(Material.iron), IHidden {

    init {
        setCreativeTab(null)
    }

    override fun getTextureName(): String? {
        return ""
    }

    override fun getRenderType(): Int {
        return -1
    }

    override fun isOpaqueCube(): Boolean {
        return false
    }

    override fun renderAsNormalBlock(): Boolean {
        return false
    }

    override fun hasTileEntity(metadata: Int): Boolean {
        return true
    }

    override fun getIcon(side: Int, metadata: Int): IIcon {
        blockMapping[metadata]?.let {
            return it.getIcon(side, metadata)
        }
        return super.getIcon(side, metadata)
    }

    override fun onBlockActivated(world: World, x: Int, y: Int, z: Int, entityPlayer: EntityPlayer, meta: Int,
                                  xOffset: Float, yOffset: Float, zOffset: Float): Boolean {
        if (world.isRemote) return false
        val tileBound = world.getTileEntity(x, y, z) as TileBound
        val rootBlock = world.getBlock(tileBound.pos.xCoord.toInt(), tileBound.pos.yCoord.toInt(),
                tileBound.pos.zCoord.toInt())
        return rootBlock.onBlockActivated(world, tileBound.pos.xCoord.toInt(), tileBound.pos.yCoord.toInt(),
                tileBound.pos.zCoord.toInt(), entityPlayer, meta, xOffset, yOffset, zOffset)
    }

    override fun getPickBlock(target: MovingObjectPosition, world: World, x: Int, y: Int, z: Int,
                              player: EntityPlayer): ItemStack? {
        val metadata = world.getBlockMetadata(x, y, z)
        return getMapping(metadata)?.getPickBlock(target, world, x, y, z, player)
    }

    override fun breakBlock(world: World, x: Int, y: Int, z: Int, block: Block, metadata: Int) {
        val tileBound = world.getTileEntity(x, y, z) as TileBound
        tileBound.onBreak()
        super.breakBlock(world, x, y, z, block, metadata)
    }

    override fun getItemDropped(metadata: Int, rnd: Random, fortune: Int): Item? {
        return blockMapping[metadata]?.getItemDropped(metadata, rnd, fortune)
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity? {
        return if (world.isRemote) null else TileBound()
    }

    companion object {

        private var blockMapping = hashMapOf<Int, Block>()
        fun addMapping(block: Block, metadata: Int) {
            blockMapping[metadata] = block
        }

        fun getMapping(block: Block): Int = blockMapping.filterValues { it == block }.keys.first().or(0)

        fun getMapping(metadata: Int): Block? = blockMapping[metadata]
    }
}