package oc.block

import li.cil.oc.api.network.Environment
import net.minecraft.block.material.Material
import net.minecraft.entity.EntityLivingBase
import net.minecraft.item.ItemStack
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.MathHelper
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.init.Render
import oc.settings
import oc.tile.TileRadar

class BlockRadar : BaseTileBlock(Material.iron), IEnvironmentBlock {

    init {
        setBlockTextureName("radar_particle")
    }

    override fun onBlockPlacedBy(world: World, x: Int, y: Int, z: Int, entity: EntityLivingBase, itemStack: ItemStack) {
        super.onBlockPlacedBy(world, x, y, z, entity, itemStack)
        val l = MathHelper.floor_double((entity.rotationYaw * 4.0f / 360.0f).toDouble() + 0.5) and 3
        world.setBlockMetadataWithNotify(x, y, z, ForgeDirection.getOrientation(l).opposite.ordinal, 1)
    }

    override fun getRenderType(): Int {
        return Render.renderRadar
    }

    override fun isNormalCube(): Boolean {
        return false
    }

    override fun isOpaqueCube(): Boolean {
        return false
    }

    override fun canRenderInPass(pass: Int): Boolean {
        return true
    }

    override fun renderAsNormalBlock(): Boolean {
        return false
    }

    override fun createNewTileEntity(world: World, metadata: Int): TileEntity {
        return TileRadar()
    }

    override val tooltipData: Array<Any?>
        get() = with(settings.blockRadar) { arrayOf(scanEnergy, maxRange, maxHeight, maxAngle) }

    override fun getTileEntityClass(meta: Int): Class<out Environment?>? = TileRadar::class.java
}