package oc.block

import li.cil.oc.api.driver.EnvironmentProvider
import li.cil.oc.api.network.Environment
import net.minecraft.item.ItemBlock
import net.minecraft.item.ItemStack

object BlockEnvironmentProvider : EnvironmentProvider {
    override fun getEnvironment(stack: ItemStack?): Class<*>? {
        val item = stack?.item as? ItemBlock ?: return null
        val block = item.field_150939_a as? IEnvironmentBlock ?: return null
        return block.getTileEntityClass(stack.itemDamage)
    }
}

interface IEnvironmentBlock {
    fun getTileEntityClass(meta: Int): Class<out Environment?>?
}