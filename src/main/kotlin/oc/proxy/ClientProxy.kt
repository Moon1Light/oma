package oc.proxy

import cpw.mods.fml.common.event.FMLInitializationEvent
import oc.Resources
import oc.init.Render

class ClientProxy : CommonProxy() {

    override fun init(event: FMLInitializationEvent?) {
        super.init(event)
        Resources.init()
        Render.register()
    }

}