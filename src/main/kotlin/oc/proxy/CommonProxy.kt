package oc.proxy

import cpw.mods.fml.common.FMLCommonHandler
import cpw.mods.fml.common.event.FMLInitializationEvent
import cpw.mods.fml.common.event.FMLInterModComms
import cpw.mods.fml.common.event.FMLPostInitializationEvent
import cpw.mods.fml.common.event.FMLPreInitializationEvent
import cpw.mods.fml.common.network.NetworkRegistry
import net.minecraftforge.common.MinecraftForge
import oc.Mods
import oc.OCSettings
import oc.OMA
import oc.event.CommonEvents
import oc.event.FMLEvents
import oc.gui.GuiHandler
import oc.init.*
import oc.settings

open class CommonProxy {

    @Suppress("UNUSED_PARAMETER")
    fun preLoad(event: FMLPreInitializationEvent?) {
        Items.register()
        Blocks.register()
        Tiles.register()
        if (settings.petRenderer.isEnabled) OCPetRenderer.init()
    }

    open fun init(event: FMLInitializationEvent?) {
        OCSettings.load()
        Drivers.register()
        Events.register()
        Recipes.register()
        Cases.register()
        FMLInterModComms.sendMessage(Mods.Ids.waila, "register", "oc.IntegrationWaila.register")
        ThaumcraftPortableHoleBlacklist.init()
        FMLCommonHandler.instance().bus().register(FMLEvents())

        MinecraftForge.EVENT_BUS.register(CommonEvents())
        NetworkRegistry.INSTANCE.registerGuiHandler(OMA.instance(), GuiHandler())
    }

    @Suppress("UNUSED_PARAMETER")
    fun postInit(event: FMLPostInitializationEvent?) {}

}