package oc.driver

import li.cil.oc.api.Network
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.*
import li.cil.oc.util.ExtendedArguments
import li.cil.oc.util.InventoryUtils
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.server.MinecraftServer
import net.minecraft.world.World
import oc.settings
import scala.Option
import kotlin.math.floor
import kotlin.math.sqrt

class DriverTesseract(val host: EnvironmentHost, private val link: Link) : BaseManagedEnvironment() {

    private var isActive = false
    private val agent = host as Agent

    override val node = Network.newNode(this, Visibility.Network)
            .withConnector().withComponent("tesseract").create()!!

    override fun canUpdate(): Boolean {
        return true
    }

    override fun update() {
        if (isActive && !node.tryChangeBuffer(-energyPerTick)) {
            isActive = false
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean")
    fun isActive(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(isActive)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number")
    fun getEnergyCost(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(energyPerTick)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number")
    fun getDistance(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(distanceTo)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(active:boolean)")
    fun setActive(context: Context, arguments: Arguments): Array<Any> {
        isActive = arguments.checkBoolean(0)
        return arrayOf(true)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([count:number=64][,side:number]):number[,string] -- Transfers some items from selected slot into the linked inventory.")
    fun transferItem(context: Context, arguments: Arguments): Array<Any> {
        if (!isActive) return arrayOf(0, "tesseract is deactivated")
        val linkedInventory = inventory ?: return arrayOf(0, "inventory not found")
        val count = ExtendedArguments.extendedArguments(arguments).optItemCount(0, 64)
        val side = ExtendedArguments.extendedArguments(arguments).optSideAny(1, null)
        val agentInventory = agent.mainInventory()
        val stack = agentInventory.getStackInSlot(agent.selectedSlot())
        val oldStackSize = stack.stackSize
        if (stack.stackSize > 0 && count > 0) {
            InventoryUtils.insertIntoInventory(stack, linkedInventory, Option.apply(side),
                    count.coerceAtMost(stack.maxStackSize), false, Option.empty())
        }
        return arrayOf(oldStackSize - stack.stackSize)
    }

    override fun load(nbt: NBTTagCompound) {
        super.load(nbt)
        isActive = nbt.getBoolean("isActive")
    }

    override fun save(nbt: NBTTagCompound) {
        super.save(nbt)
        nbt.setBoolean("isActive", isActive)
    }

    override fun onMessage(message: Message) {
        super.onMessage(message)
        if (message.name() == "computer.stopped") {
            isActive = false
        }
    }

    private val energyPerTick: Double
        get() {
            val world = host.world()
            val inOneWorld = world.provider.dimensionId == link.dimId
            return if (inOneWorld) settings.tesseract.energyPerTickInOneWorld
            else settings.tesseract.energyPerTickInOtherWorld
        }

    private val distanceTo: Double
        get() {
            val world = host.world()
            if (world.provider.dimensionId != link.dimId) {
                return Double.POSITIVE_INFINITY
            }
            val robot = host as Agent
            val deltaX = floor(robot.xPosition()) - link.x
            val deltaY = floor(robot.yPosition()) - link.y
            val deltaZ = floor(robot.zPosition()) - link.z
            return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)
        }

    private val inventory: IInventory?
        get() {
            val world = linkWorld
            if (!settings.tesseract.doChunkLoading && !world.checkChunksExist(
                            link.x, link.y, link.z, link.x, link.y, link.z)) {
                return null
            }
            val entity = world.getTileEntity(link.x, link.y, link.z)
            if (entity is IInventory) {
                return entity
            }
            return null
        }

    private val linkWorld: World = MinecraftServer.getServer().worldServerForDimension(link.dimId)

    class Link(val dimId: Int, val x: Int, val y: Int, val z: Int, val dimName: String) {
        fun save(tag: NBTTagCompound) {
            val linkTag = NBTTagCompound()
            tag.setTag("link", linkTag)
            linkTag.setInteger("dimId", dimId)
            linkTag.setString("dimName", dimName)
            linkTag.setInteger("x", x)
            linkTag.setInteger("y", y)
            linkTag.setInteger("z", z)
        }

        companion object {
            fun load(stack: ItemStack): Link? {
                val tag = stack.tagCompound
                if (tag != null && tag.hasKey("link")) {
                    val linkTag = tag.getCompoundTag("link")
                    val x = linkTag.getInteger("x")
                    val y = linkTag.getInteger("y")
                    val z = linkTag.getInteger("z")
                    return Link(linkTag.getInteger("dimId"), x, y, z, linkTag.getString("dimName"))
                }
                return null
            }

            fun clear(tag: NBTTagCompound) {
                tag.removeTag("link")
            }

            fun isLinked(stack: ItemStack): Boolean {
                val compound = stack.tagCompound
                return compound != null && compound.hasKey("link")
            }
        }

    }

}