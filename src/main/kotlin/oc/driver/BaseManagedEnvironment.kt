package oc.driver

import li.cil.oc.api.network.Node
import li.cil.oc.api.prefab.ManagedEnvironment

abstract class BaseManagedEnvironment : ManagedEnvironment() {

    abstract val node: Node

    final override fun node() = node

}