package oc.driver

import li.cil.oc.api.Network
import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.driver.DeviceInfo.DeviceAttribute
import li.cil.oc.api.driver.DeviceInfo.DeviceClass
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.*
import li.cil.oc.integration.util.ItemCharge
import net.minecraft.nbt.NBTTagCompound
import oc.settings
import oc.util.OCUtils.Vendors

class DriverItemCharger(private val host: EnvironmentHost) : BaseManagedEnvironment(), DeviceInfo {
    private var charge = false

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("item_charger", Visibility.Neighbors).withConnector().create()!!

    private val device by lazy {
        mapOf(DeviceAttribute.Class to DeviceClass.Generic,
                DeviceAttribute.Description to "Item Charger",
                DeviceAttribute.Vendor to Vendors.OMATechnologies,
                DeviceAttribute.Product to "ItemCharger"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    override fun canUpdate(): Boolean {
        return true
    }

    override fun update() {
        super.update()
        if (charge && host.world().totalWorldTime % settings.oc.tickFrequency() == 0.0) {
            val agent = host as Agent
            val stack = agent.mainInventory().getStackInSlot(agent.selectedSlot())
            if (stack != null && stack.stackSize > 0 && ItemCharge.canCharge(stack)) {
                val charge = settings.oc.chargeRateTablet() * settings.oc.tickFrequency()
                if (settings.oc.ignorePower()
                        || charge > 0 && node.globalBuffer() >= charge * 2) {
                    val offered = charge + node.changeBuffer(-charge)
                    val surplus = ItemCharge.charge(stack, offered)
                    node.changeBuffer(surplus)
                }
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([enable:boolean]):boolean -- Activate or deactivate item charging. Returns whether the charger is currently charging item.")
    fun charge(context: Context, arguments: Arguments): Array<Any> {
        if (arguments.count() > 0) {
            charge = arguments.checkBoolean(0)
        }
        return arrayOf(charge)
    }

    override fun load(nbt: NBTTagCompound) {
        super.load(nbt)
        charge = nbt.getBoolean("charge")
    }

    override fun save(nbt: NBTTagCompound) {
        super.save(nbt)
        nbt.setBoolean("charge", charge)
    }

    override fun onMessage(message: Message) {
        super.onMessage(message)
        if (message.name() == "computer.stopped") {
            charge = false
        }
    }

}