package oc.driver

import li.cil.oc.api.driver.DeviceInfo.DeviceAttribute
import li.cil.oc.api.driver.DeviceInfo.DeviceClass
import li.cil.oc.api.event.GeolyzerEvent.Analyze
import li.cil.oc.api.internal.Rotatable
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.server.component.Geolyzer
import li.cil.oc.server.component.UpgradeDatabase
import li.cil.oc.util.BlockPosition
import li.cil.oc.util.DatabaseAccess
import li.cil.oc.util.ExtendedArguments
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.common.util.ForgeDirection
import oc.settings
import oc.util.OCUtils.Vendors
import scala.Function1
import scala.Option
import scala.runtime.AbstractFunction1
import kotlin.math.floor

class DriverAdvancedGeolyzer(host: EnvironmentHost?) : Geolyzer(host) {


    private val device by lazy {
        mapOf(DeviceAttribute.Class to DeviceClass.Generic,
                DeviceAttribute.Description to "Advanced Geolyzer Upgrade",
                DeviceAttribute.Vendor to Vendors.OMATechnologies,
                DeviceAttribute.Product to "Terrain Analyzer MkIII"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    @Callback(doc = "function(side:number[,options:table]):table -- Get some information on a directly adjacent block.")
    override fun analyze(computer: Context, arguments: Arguments): Array<Any?> {
        val ret = super.analyze(computer, arguments) // ret[0] is Map<String, Object> or null
        if (ret[0] is MutableMap<*, *>) {
            @Suppress("UNCHECKED_CAST")
            val data = ret[0] as MutableMap<String, Any>
            val side: ForgeDirection = ExtendedArguments.extendedArguments(arguments).checkSideAny(0)
            val globalSide = if (host() is Rotatable) (host() as Rotatable).toGlobal(side) else side
            // TODO: BlockPos utils
            val globalPos = BlockPosition(
                    floor(host().xPosition()).toInt(),
                    floor(host().yPosition()).toInt(),
                    floor(host().zPosition()).toInt(),
                    Option.apply(host().world()))
                    .offset(globalSide)
            addExtraData(data, globalPos.x(), globalPos.y(), globalPos.z())
        }
        return ret
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(x,y,z:number[,options:table]):table -- Get some information about the block at the given coordinates.")
    fun longRangeAnalyze(context: Context?, arguments: Arguments): Array<Any?> {
        return if (settings.oc.allowItemStackInspection()) {
            val d = Data(arguments)
            if (d.err != null) {
                return arrayOf(null, d.err)
            }
            val options = getOptions(arguments)
            val event = Analyze(host(), options, d.x, d.y, d.z)
            MinecraftForge.EVENT_BUS.post(event)
            if (event.isCanceled) {
                arrayOf<Any?>(null, "scan was canceled")
            } else {
                addExtraData(event.data, d.x, d.y, d.z)
                arrayOf<Any?>(event.data)
            }
        } else {
            arrayOf(null, "not enabled in config")
        }
    }

    private fun addExtraData(data: MutableMap<String, Any>, x: Int, y: Int, z: Int) {
        val world = host().world()
        data["biome"] = world.getBiomeGenForCoords(x, z).biomeName
        data["light"] = world.getBlockLightValue(x, y, z)
        data["isSlimeChunk"] = world.getChunkFromBlockCoords(x, z).getRandomWithSeed(987234911L).nextInt(10) == 0
    }

    private fun getOptions(arguments: Arguments): Map<Any?, Any?> {
        return arguments.optTable(3, HashMap<Any?, Any?>())
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(x,y,z:number, dbAddress:string, dbSlot:number):boolean -- Store an item stack representation of the block at the given coordinates.")
    fun longRangeStore(context: Context?, arguments: Arguments): Array<Any?> {
        val d = Data(arguments)
        if (d.err != null) {
            return arrayOf(null, d.err)
        }
        val block = host().world().getBlock(d.x, d.y, d.z)
        val item = Item.getItemFromBlock(block)
                ?: return arrayOf(null, "block has no registered item representation")
        val metadata = host().world().getBlockMetadata(d.x, d.y, d.z)
        val damage = block.damageDropped(metadata)
        val stack = ItemStack(item, 1, damage)
        val f: Function1<UpgradeDatabase, Array<Any>> = object : AbstractFunction1<UpgradeDatabase, Array<Any>>() {
            override fun apply(database: UpgradeDatabase): Array<Any> {
                val toSlot: Int = ExtendedArguments.extendedArguments(arguments).checkSlot(database.data(), 4)
                val nonEmpty = database.getStackInSlot(toSlot) != null
                database.setStackInSlot(toSlot, stack)
                return arrayOf(nonEmpty)
            }
        }
        return DatabaseAccess.withDatabase(node(), arguments.checkString(3), f)
    }

    private inner class Data internal constructor(arguments: Arguments) {
        val x: Int
        val y: Int
        val z: Int
        var err: String? = null

        init {
            val dx = arguments.checkInteger(0)
            val dy = arguments.checkInteger(1)
            val dz = arguments.checkInteger(2)
            x = floor(host().xPosition()).toInt() + dx
            y = floor(host().yPosition()).toInt() + dy
            z = floor(host().zPosition()).toInt() + dz
            val geolyzerRadius: Int = settings.geolyzer.scanRadius
            require(!(dx > geolyzerRadius || dy > geolyzerRadius || dz > geolyzerRadius)) { "location out of bounds" }
            if (!node().tryChangeBuffer(-settings.geolyzer.scanCost)) {
                err = "not enough energy"
            }
        }
    }
}