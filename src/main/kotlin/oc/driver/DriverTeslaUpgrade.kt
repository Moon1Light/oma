package oc.driver

import li.cil.oc.api.API
import li.cil.oc.api.Network
import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.EntityLivingBase
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.AxisAlignedBB
import net.minecraft.world.WorldServer
import oc.OMA
import oc.settings
import oc.util.OCUtils
import oc.util.TeslaDamage


class DriverTeslaUpgrade(val host: EnvironmentHost) : BaseManagedEnvironment(), DeviceInfo {
    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("tesla").withConnector().create()!!

    private val maxRange = settings.upgradeTesla.maxRange.toDouble()
    private var heat = 0
    private val agent = host as Agent

    private val device by lazy {
        mapOf(DeviceInfo.DeviceAttribute.Class to DeviceInfo.DeviceClass.Generic,
                DeviceInfo.DeviceAttribute.Description to "Tesla",
                DeviceInfo.DeviceAttribute.Vendor to OCUtils.Vendors.OMATechnologies,
                DeviceInfo.DeviceAttribute.Product to "TeslaCoil-15T"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    override fun canUpdate() = true

    private val isOverHeated
        get() = heat > 0

    override fun update() {
        if (isOverHeated) {
            heat--
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- check overheated.")
    fun isOverHeated(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(isOverHeated)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(range: number = MAX_RANGE): boolean -- Attack entities.")
    fun attack(context: Context, arguments: Arguments): Array<Any?>? {
        if (isOverHeated) return arrayOf(false, "tesla overheated")
        val range = getRangeArg(arguments)
        return if (node.tryChangeBuffer(-energyToAttack(range)) || !API.isPowerEnabled) {
            val x = host.xPosition()
            val y = host.yPosition()
            val z = host.zPosition()
            val area = AxisAlignedBB.getBoundingBox(x, y, z, x, y, z).expand(range, range, range)
            val entities = host.world().getEntitiesWithinAABB(EntityLivingBase::class.java, area)
            if (entities.size > 0) {
                val damage: Float = settings.upgradeTesla.damage.toFloat() / entities.size
                entities.forEach {
                    val entity = it as EntityLivingBase
                    if (entity.attackEntityFrom(TeslaDamage(agent.player()), damage)) {
                        with(host.world() as WorldServer) {
                            for (i in 1..25) {
                                func_147487_a("reddust",
                                        entity.posX + rand.nextFloat().toDouble() - 0.5,
                                        entity.posY + (rand.nextFloat() * 2.0f).toDouble() - 1.0,
                                        entity.posZ + rand.nextFloat().toDouble() - 0.5,
                                        0, 1.0, 1.0, 0.0, 1.0)
                            }
                        }
                    }
                }
                host.world().playSoundEffect(host.xPosition(), host.yPosition(), host.zPosition(),
                        OMA.MOD_ID + ":tesla_attack", 1f, 1f)
            }
            heat = settings.upgradeTesla.coolingTicks
            arrayOf(true)
        } else {
            arrayOf(false, "not enough energy.")
        }
    }

    private fun getRangeArg(arguments: Arguments) = arguments.optDouble(0, maxRange)
            .coerceAtMost(maxRange)
            .coerceAtLeast(0.0)

    private fun energyToAttack(range: Double) = range / maxRange * settings.upgradeTesla.energyPerAttack

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(range: number = MAX_RANGE): number")
    fun energyToAttack(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(energyToAttack(getRangeArg(arguments)))
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(): number -- Returns max range.")
    fun getMaxRange(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(maxRange)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(): number -- Returns damage. Damage is distributed among all entities within a radius.")
    fun getDamage(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(settings.upgradeTesla.damage)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(): number -- how many ticks need for cooling.")
    fun getCoolingTicks(context: Context, arguments: Arguments): Array<Any?>? {
        return arrayOf(settings.upgradeTesla.coolingTicks)
    }

    override fun load(nbt: NBTTagCompound) {
        super.load(nbt)
        heat = nbt.getInteger("heat")
    }

    override fun save(nbt: NBTTagCompound) {
        super.save(nbt)
        nbt.setInteger("heat", heat)
    }

}