package oc.driver

import li.cil.oc.api.Network
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.network.Visibility

class DriverContainerAdapter(val host: Agent) : BaseManagedEnvironment() {

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("container_adapter").create()!!
}