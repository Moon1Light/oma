package oc.driver

import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.internal.Tablet
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.machine.Machine
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Message
import li.cil.oc.server.component.UpgradeNavigation
import li.cil.oc.util.BlockPosition
import net.minecraft.nbt.NBTTagCompound
import oc.util.OCUtils

class DriverAdvancedNavigation(val host: EnvironmentHost) : UpgradeNavigation(host) {
    private val device by lazy {
        mapOf(DeviceInfo.DeviceAttribute.Class to DeviceInfo.DeviceClass.Generic,
                DeviceInfo.DeviceAttribute.Description to "Advanced Navigation Upgrade",
                DeviceInfo.DeviceAttribute.Vendor to OCUtils.Vendors.Scrag,
                DeviceInfo.DeviceAttribute.Product to "PathFinder v4"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    @Callback(doc = "function():number, number, number -- Get the current absolute position of the robot.")
    override fun getPosition(context: Context, arguments: Arguments): Array<Any?> {
        return arrayOf(host.xPosition(), host.yPosition(), host.zPosition())
    }

    @Callback(doc = "function():number -- Get the operational range of the navigation upgrade.")
    override fun getRange(context: Context, arguments: Arguments): Array<Any?> {
        return arrayOf(Int.MAX_VALUE)
    }

    override fun onMessage(message: Message) {
        super.onMessage(message)
        if (message.name() == "tablet.use") {
            (message.source().host() as? Machine)?.let { machine ->
                if (machine.host() is Tablet) {
                    if (message.data().size == 8 && message.data()[3] is BlockPosition
                            && message.data()[0] is NBTTagCompound) {
                        val nbt = message.data()[0] as NBTTagCompound
                        val blockPos = message.data()[3] as BlockPosition
                        nbt.setInteger("posX", blockPos.x())
                        nbt.setInteger("posY", blockPos.y())
                        nbt.setInteger("posZ", blockPos.z())
                    }
                }
            }
        }
    }
}