package oc.driver

import li.cil.oc.api.Network
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import li.cil.oc.api.prefab.ManagedEnvironment

open class ManagedTileEntityEnvironment<T>(protected val tileEntity: T, protected val name: String) :
    ManagedEnvironment() {

    private val node by lazy {
        Network.newNode(this, Visibility.Network).withComponent(name, Visibility.Network).create()
    }

    override fun node(): Node = node

}