package oc.driver

import ic2.api.item.ElectricItem
import ic2.api.item.IElectricItem
import li.cil.oc.api.Network
import li.cil.oc.api.internal.Robot
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Visibility
import net.minecraft.nbt.NBTTagCompound
import oc.OCSettings
import kotlin.math.floor

class DriverSolarPanel(val host: EnvironmentHost, chargedTool: Boolean,
                       private val nightEnergyPerTick: Int,
                       private val dayEnergyPerTick: Int) : BaseManagedEnvironment() {

    private val chargedTool: Boolean = chargedTool && host is Robot
    private var isChargeTool = false
    private var ticksUntilCheck = 0
    private var energyPerTick = 0

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("solar_panel").withConnector().create()!!

    override fun canUpdate(): Boolean {
        return true
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(activate:boolean):boolean; Activate or deactivate tool charging. Returns whether the panel is currently charging tool.")
    fun chargeTool(context: Context, arguments: Arguments): Array<Any> {
        if (chargedTool) {
            isChargeTool = arguments.checkBoolean(0)
        }
        return arrayOf(isChargeTool)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number --  Returns the day energy output per tick.")
    fun getDayEnergyPerTick(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(dayEnergyPerTick)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number --  Returns the night energy output per tick.")
    fun getNightEnergyPerTick(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(nightEnergyPerTick)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Returns whether the panel can charge tools.")
    fun canChargeTool(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(chargedTool)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Returns whether the panel is currently generating energy.")
    fun isActive(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(energyPerTick > 0)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Returns whether the panel is currently charging tool.")
    fun isChargeTool(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(isChargeTool)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number --  Returns the energy output per tick.")
    fun getEnergyPerTick(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(energyPerTick)
    }

    private fun updateOutputEnergy() {
        val x = floor(host.xPosition()).toInt()
        val y = floor(host.yPosition()).toInt() + 1
        val z = floor(host.zPosition()).toInt()
        val world = host.world()
        val canSeeSky = !world.provider.hasNoSky && world.canBlockSeeTheSky(x, y, z)
        val isSunVisible = canSeeSky && world.isDaytime && !world.isRaining && !world.isThundering
        energyPerTick = if (canSeeSky) {
            if (isSunVisible) dayEnergyPerTick else nightEnergyPerTick
        } else {
            0
        }
    }

    override fun update() {
        --ticksUntilCheck
        if (ticksUntilCheck <= 0) {
            ticksUntilCheck = checkPeriod
            updateOutputEnergy()
        }
        if (energyPerTick > 0) {
            if (!isChargeTool) {
                node.tryChangeBuffer(energyPerTick.toDouble())
            } else {
                val robot = host as Robot
                val inventory = robot.equipmentInventory()
                val tool = inventory.getStackInSlot(0)
                if (tool != null && tool.item is IElectricItem) {
                    ElectricItem.manager.charge(tool, toEu(energyPerTick.toDouble()), Int.MAX_VALUE, true, false)
                }
            }
        }
    }

    override fun load(nbt: NBTTagCompound) {
        super.load(nbt)
        isChargeTool = nbt.getBoolean("isChargeTool") && chargedTool
    }

    override fun save(nbt: NBTTagCompound) {
        super.save(nbt)
        nbt.setBoolean("isChargeTool", isChargeTool)
    }

    companion object {
        private const val checkPeriod = 100
        private fun toEu(value: Double): Double {
            return value / OCSettings.ratioIndustrialCraft2
        }
    }

}