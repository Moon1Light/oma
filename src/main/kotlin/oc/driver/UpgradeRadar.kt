package oc.driver

import li.cil.oc.api.Network
import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.driver.DeviceInfo.DeviceAttribute
import li.cil.oc.api.driver.DeviceInfo.DeviceClass
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Visibility
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLiving
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.player.EntityPlayer
import oc.settings
import oc.util.OCUtils.Vendors
import oc.util.RadarUtils

class UpgradeRadar(val host: EnvironmentHost) : BaseManagedEnvironment(), DeviceInfo {

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("radar").withConnector().create()!!

    private val device by lazy {
        mapOf(DeviceAttribute.Class to DeviceClass.Generic,
                DeviceAttribute.Description to "Radar",
                DeviceAttribute.Vendor to Vendors.OMATechnologies,
                DeviceAttribute.Product to "RAD-X5"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    private fun getEntities(
            context: Context, args: Arguments, energy: Double, eClass: Class<out Entity?>?): Array<Any?> {
        val distance = args.optInteger(0, settings.upgradeRadar.maxRange)
                .coerceAtMost(settings.upgradeRadar.maxRange)
                .coerceAtLeast(1)
        if (node.tryChangeBuffer(-energy * distance)) {
            val entities = RadarUtils.getEntities(
                    host.world(), host.xPosition(), host.yPosition(), host.zPosition(), distance, eClass)
            context.pause(settings.upgradeRadar.delay)
            return arrayOf(entities.toTypedArray())
        }
        return arrayOf(null, "not enough energy")
    }

    @Callback(doc = "function([distance:number]):table -- Returns a list of all entities (players and mobs) detected within the specified or the maximum range", direct = true)
    fun getEntities(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, settings.upgradeRadar.entityScanEnergy, EntityLivingBase::class.java)
    }

    @Callback(doc = "function([distance:number]):table -- Returns a list of all players detected within the specified or the maximum range", direct = true)
    fun getPlayers(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, settings.upgradeRadar.playerScanEnergy, EntityPlayer::class.java)
    }

    @Callback(doc = "function([distance:number]):table -- Returns a list of all mobs detected within the specified or the maximum range", direct = true)
    fun getMobs(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, settings.upgradeRadar.mobScanEnergy, EntityLiving::class.java)
    }

    @Callback(doc = "function([distance:number]):table -- Returns a list of all items detected within the specified or the maximum range", direct = true)
    fun getItems(context: Context, args: Arguments): Array<Any?> {
        return getEntities(context, args, settings.upgradeRadar.itemScanEnergy, EntityItem::class.java)
    }

}