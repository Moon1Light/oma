package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityLiquidHeatExchanger
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverEntityLiquidHeatExchanger : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityLiquidHeatExchanger::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityLiquidHeatExchanger)

    class Environment(tileEntity: TileEntityLiquidHeatExchanger) : IMachineActive,
            ManagedTileEntityEnvironment<TileEntityLiquidHeatExchanger>(tileEntity, "liquid_heat_exchanger") {

        override val isActive: Boolean
            get() = tileEntity.active

    }

}