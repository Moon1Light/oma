package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityAdvMiner
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverAdvMiner : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityAdvMiner::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityAdvMiner)

    class Environment(tileEntity: TileEntityAdvMiner) :
            ManagedTileEntityEnvironment<TileEntityAdvMiner>(tileEntity, "ic2_advanced_miner") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun restart(context: Context, args: Arguments): Array<Any>? {
            tileEntity.onNetworkEvent(null, 0)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean")
        fun isBlacklist(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.blacklist)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean")
        fun isSilkTouch(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.silktouch)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(boolean)")
        fun setBlacklist(context: Context, args: Arguments): Array<Any>? {
            if (!tileEntity.active) {
                tileEntity.blacklist = args.checkBoolean(0)
            }
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(boolean)")
        fun setSilkTouch(context: Context, args: Arguments): Array<Any>? {
            if (!tileEntity.active) {
                tileEntity.silktouch = args.checkBoolean(0)
            }
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number, number, number")
        fun getMinePos(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.xcounter, tileEntity.getminelayer(), tileEntity.zcounter)
        }

    }

}