package oc.driver.ic2

import ic2.core.item.ItemCrystalMemory
import ic2.core.uu.UuIndex
import li.cil.oc.api.driver.Converter
import net.minecraft.item.ItemStack

object ConvertPattern : Converter {
    override fun convert(value: Any, output: MutableMap<Any, Any>) {
        if (value is ItemStack) {
            if (value.item is ItemCrystalMemory) {
                val itemCrystalMemory = value.item as ItemCrystalMemory
                val recorded = itemCrystalMemory.readItemStack(value)
                output["uuMatter"] = UuIndex.instance.getInBuckets(recorded)
                output["recorded"] = recorded
            }
        }
    }
}