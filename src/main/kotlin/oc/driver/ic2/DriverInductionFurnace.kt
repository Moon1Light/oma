package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityInduction
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverInductionFurnace : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityInduction::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityInduction)

    class Environment internal constructor(tileEntity: TileEntityInduction) : IMachineWorkProgress<Int>, IMachineHeat,
            ManagedTileEntityEnvironment<TileEntityInduction>(tileEntity, "ic2_induction_furnace") {

        override val workProgress: Int
            get() = tileEntity.progress.toInt()

        override val workMaxProgress: Int = 4000

        override val progress: Double
            get() = tileEntity.progress.toDouble() / workMaxProgress

        override val heat: Int
            get() = tileEntity.heat.toInt()

        override val maxHeat: Int
            get() = TileEntityInduction.maxHeat.toInt()

    }

}