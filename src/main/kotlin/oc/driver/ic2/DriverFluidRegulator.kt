package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityFluidRegulator
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment
import oc.util.Reflection

object DriverFluidRegulator : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityFluidRegulator::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityFluidRegulator)

    class Environment(tileEntity: TileEntityFluidRegulator) :
            ManagedTileEntityEnvironment<TileEntityFluidRegulator>(tileEntity, "fluid_regulator") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number,number -- Get the fluid output rate (mb/t, mode).")
        fun getOutput(context: Context, args: Arguments): Array<Any?> {
            return arrayOf(tileEntity.getoutputmb(), Reflection.getInternal(tileEntity, "mode"))
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(amount:number, mode:number=1) -- Set the fluid output rate (mb/t, mode).")
        fun setOutput(context: Context, args: Arguments): Array<Any>? {
            val output = args.checkInteger(0).coerceAtMost(1000).coerceAtLeast(0)
            val mode = args.optInteger(1, 1)
            require(mode in 0..1) { "invalid mode" }
            tileEntity.onNetworkEvent(null, 1002 - mode)
            tileEntity.onNetworkEvent(null, output - tileEntity.getoutputmb())
            return null
        }

        companion object {
            private val modes = mapOf(
                    "sec" to 0,
                    "tick" to 1
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table -- Get the modes.")
        fun modes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(modes)
        }
    }
}