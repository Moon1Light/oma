package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityReplicator
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverReplicator : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityReplicator::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityReplicator)

    class Environment(tileEntity: TileEntityReplicator) : ManagedTileEntityEnvironment<TileEntityReplicator>(tileEntity, "ic2_replicator") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table,number")
        fun getPattern(context: Context, args: Arguments): Array<Any?> {
            tileEntity.refreshInfo()
            return if (tileEntity.pattern == null) {
                DriverPatternStorage.NO_SELECTED_PATTERN_ERROR
            } else arrayOf(tileEntity.pattern, tileEntity.patternUu)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getPatternCount(context: Context, args: Arguments): Array<Any> {
            tileEntity.refreshInfo()
            return if (tileEntity.patternStorage == null) {
                arrayOf(0)
            } else arrayOf(tileEntity.maxIndex)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(index:number):number")
        fun selectPattern(context: Context?, args: Arguments): Array<Any> {
            tileEntity.refreshInfo()
            val index = args.optInteger(0, tileEntity.index + 1)
            require(!(index < 1 || index > tileEntity.maxIndex)) { "invalid index" }
            if (index != tileEntity.index + 1) {
                tileEntity.index = index - 1
                tileEntity.refreshInfo()
            }
            return arrayOf(tileEntity.index + 1)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(repeat:boolean=false)")
        fun start(context: Context?, args: Arguments): Array<Any>? {
            val repeat = args.optBoolean(0, false)
            tileEntity.onNetworkEvent(null, if (repeat) 5 else 4)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun stop(context: Context, args: Arguments): Array<Any>? {
            tileEntity.onNetworkEvent(null, 3)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getProcessedUu(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.uuProcessed)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getProgress(context: Context, args: Arguments): Array<Any> {
            return if (tileEntity.patternUu != 0.0) {
                arrayOf(tileEntity.uuProcessed / tileEntity.patternUu)
            } else arrayOf(0)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean")
        fun hasPatternStorage(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.patternStorage != null)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getMode(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.mode.ordinal)
        }

        companion object {
            private val modes = mapOf(
                    "stopped" to 0,
                    "single" to 1,
                    "continuous" to 2
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table")
        fun modes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(modes)
        }

    }

}