package oc.driver.ic2

import codechicken.core.ReflectionManager
import ic2.core.block.machine.tileentity.TileEntityStandardMachine
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverStandardMachine : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityStandardMachine::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityStandardMachine)

    class Environment(tileEntity: TileEntityStandardMachine) : IMachineWorkProgress<Int>, IMachine,
            ManagedTileEntityEnvironment<TileEntityStandardMachine>(tileEntity, "ic2_standard_machine") {

        override val workProgress: Int
            get() = ReflectionManager.getField(
                    TileEntityStandardMachine::class.java, Short::class.javaPrimitiveType, tileEntity, "progress").toInt()

        override val workMaxProgress: Int
            get() = tileEntity.operationLength

        override val progress: Double
            get() = tileEntity.progress.toDouble()

        override val energyConsume: Int
            get() = tileEntity.energyConsume

        override val operationsPerTick: Int
            get() = tileEntity.operationsPerTick

        override val operationLength: Int
            get() = tileEntity.operationLength

    }

}