package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityElectricMachine
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverElectricMachine : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityElectricMachine::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityElectricMachine)

    class Environment internal constructor(tileEntity: TileEntityElectricMachine) : IMachineActive,
            ManagedTileEntityEnvironment<TileEntityElectricMachine>(tileEntity, "ic2_electric_machine") {

        override val isActive: Boolean
            get() = tileEntity.active

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Returns the maximum amount of stored energy.")
        fun getMaxEnergyStored(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.maxEnergy)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Returns the total amount of stored energy.")
        fun getEnergyStored(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.energy)
        }

    }

}