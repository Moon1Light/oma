package oc.driver.ic2

import ic2.api.item.IBlockCuttingBlade
import li.cil.oc.api.driver.Converter
import net.minecraft.item.ItemStack

object ConvertBlockCuttingBlade: Converter {
    override fun convert(value: Any, output: MutableMap<Any, Any>) {
        if (value is ItemStack) {
            val item = value.item
            if (item is IBlockCuttingBlade) {
                output["hardness"] = item.gethardness()
            }
        }
    }
}