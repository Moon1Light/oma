package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityLathe
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.ManagedEnvironment
import li.cil.oc.api.prefab.DriverSidedTileEntity
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverLathe : DriverSidedTileEntity() {
    override fun getTileEntityClass(): Class<*> = TileEntityLathe::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityLathe)

    class Environment internal constructor(tileEntity: TileEntityLathe) :
            ManagedTileEntityEnvironment<TileEntityLathe>(tileEntity, "ic2_lathe") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function([dontChkPower: boolean = false]): boolean")
        fun canWork(context: Context, args: Arguments): Array<Any> {
            val power = args.optBoolean(0, false)
            return arrayOf(tileEntity.canWork(power))
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(pos: int): boolean")
        fun process(context: Context, args: Arguments): Array<Any> {
            val pos = args.checkInteger(0) - 1
            require(pos in 0..4) { "invalid pos" }
            return arrayOf(tileEntity.process(pos))
        }
    }
}