package oc.driver.ic2

import ic2.api.crops.Crops
import ic2.core.item.ItemCropSeed
import li.cil.oc.api.Network
import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.driver.DeviceInfo.DeviceAttribute
import li.cil.oc.api.driver.DeviceInfo.DeviceClass
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Visibility
import net.minecraft.world.biome.BiomeGenBase
import oc.driver.BaseManagedEnvironment
import oc.settings
import oc.util.OCUtils.Vendors
import kotlin.math.floor

class UpgradeCropnalyzer(val host: EnvironmentHost) : BaseManagedEnvironment(), DeviceInfo {

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("cropnalyzer", Visibility.Neighbors).withConnector().create()!!

    private val device by lazy {
        mapOf(DeviceAttribute.Class to DeviceClass.Generic,
                DeviceAttribute.Description to "Cropnalyzer",
                DeviceAttribute.Vendor to Vendors.IC2,
                DeviceAttribute.Product to "Cropnalyzer-Adapter-X1"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Analyzes the seeds into selected slot. Returns true if seeds are analyzed")
    fun analyze(context: Context, arguments: Arguments?): Array<Any> {
        val agent = host as Agent
        val stack = agent.mainInventory().getStackInSlot(agent.selectedSlot())
        if (stack != null && stack.item is ItemCropSeed) {
            val scanLevel = ItemCropSeed.getScannedFromStack(stack).toInt().coerceAtLeast(0)
            if (scanLevel < 4) {
                if (!node.tryChangeBuffer((-settings.cropnalyzer.scanCost[scanLevel]).toDouble())) {
                    return arrayOf(false, "not enough energy")
                }
                ItemCropSeed.incrementScannedOfStack(stack)
            }
            return arrayOf(true)
        }
        return arrayOf(false, "not seeds")
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns the humidity bonus for a biome.")
    fun getHumidityBiomeBonus(context: Context, arguments: Arguments?): Array<Any> {
        return arrayOf(Crops.instance.getHumidityBiomeBonus(biome))
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():number -- Returns the nutrient bonus for a biome.")
    fun getNutrientBiomeBonus(context: Context, arguments: Arguments?): Array<Any> {
        return arrayOf(Crops.instance.getNutrientBiomeBonus(biome))
    }

    private val biome: BiomeGenBase = host.world()
            .getBiomeGenForCoords(
                    floor(host.xPosition()).toInt(), floor(host.zPosition()).toInt())

}