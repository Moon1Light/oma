package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityPump
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverPump : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityPump::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityPump)

    class Environment internal constructor(tileEntity: TileEntityPump) : IMachineWorkProgress<Int>, IMachine,
            ManagedTileEntityEnvironment<TileEntityPump>(tileEntity, "ic2_pump") {

        override val workProgress: Int
            get() = tileEntity.progress.toInt()

        override val workMaxProgress: Int
            get() = tileEntity.operationLength

        override val progress: Double
            get() = tileEntity.guiProgress.toDouble()

        override val energyConsume: Int
            get() = tileEntity.energyConsume

        override val operationsPerTick: Int
            get() = tileEntity.operationsPerTick

        override val operationLength: Int
            get() = tileEntity.operationLength

    }

}