package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntitySteamGenerator
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment
import oc.util.Reflection

object DriverSteamGenerator : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntitySteamGenerator::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntitySteamGenerator)

    class Environment(tileEntity: TileEntitySteamGenerator) :
            ManagedTileEntityEnvironment<TileEntitySteamGenerator>(tileEntity, "steam_generator") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's calcification.")
        fun getCalcification(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getcalcification())
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's heat.")
        fun getSystemHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getsystemheat())
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's heat.")
        fun getMaxSystemHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(Reflection.getInternal(tileEntity, "maxsystemheat") ?: 0)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's pressure valve.")
        fun getPressureValve(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getpressurevalve())
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function() -- Set the boiler's pressure valve.")
        fun setPressureValve(context: Context, args: Arguments): Array<Any>? {
            val pressureValve = args.checkInteger(0).coerceAtMost(300).coerceAtLeast(0)
            Reflection.setInternal(tileEntity, "pressurevalve", pressureValve)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's water input (mb/t).")
        fun getWaterInput(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getinputmb())
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Set the boiler's water input (mb/t).")
        fun setWaterInput(context: Context, args: Arguments): Array<Any>? {
            val waterInput = args.checkInteger(0).coerceAtMost(1000).coerceAtLeast(0)
            Reflection.setInternal(tileEntity, "inputmb", waterInput)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number,number -- Get the boiler's fluid output (mb/t, type).")
        fun getOutput(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getoutputmb(), Reflection.getInternal(tileEntity, "outputtyp") ?: 0)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the boiler's heat input.")
        fun getHeatInput(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.getheatinput())
        }

        companion object {
            private val fluids = mapOf(
                    "water" to 0,
                    "destiwater" to 1,
                    "steam" to 2,
                    "hotsteam" to 3
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table -- Get the fluid types.")
        fun outputFluidTypes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(fluids)
        }

    }

}