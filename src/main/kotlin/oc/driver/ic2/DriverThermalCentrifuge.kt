package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityCentrifuge
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverThermalCentrifuge : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityCentrifuge::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityCentrifuge)

    class Environment internal constructor(tileEntity: TileEntityCentrifuge) : IMachineHeat,
            ManagedTileEntityEnvironment<TileEntityCentrifuge>(tileEntity, "ic2_thermal_centrifuge") {

        override val heat: Int
            get() = tileEntity.heat.toInt()

        override val maxHeat: Int
            get() = tileEntity.maxHeat.toInt()

    }

}