package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityPatternStorage
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import li.cil.oc.server.component.UpgradeDatabase
import li.cil.oc.util.DatabaseAccess
import li.cil.oc.util.ExtendedArguments
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment
import scala.Function1
import scala.runtime.AbstractFunction1

object DriverPatternStorage : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityPatternStorage::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityPatternStorage)

    class Environment(tileEntity: TileEntityPatternStorage) :
            ManagedTileEntityEnvironment<TileEntityPatternStorage>(tileEntity, "ic2_pattern_storage") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table,number")
        fun getPattern(context: Context, args: Arguments): Array<Any?> {
            return if (tileEntity.pattern == null) {
                NO_SELECTED_PATTERN_ERROR
            } else arrayOf(tileEntity.pattern, tileEntity.patternUu)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getPatternCount(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.patterns.size)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(index:number):number")
        fun selectPattern(context: Context, args: Arguments): Array<Any> {
            val index = args.optInteger(0, tileEntity.index + 1)
            require(!(index < 1 || index > tileEntity.patterns.size)) { "invalid index" }
            if (index != tileEntity.index + 1) {
                tileEntity.index = index - 1
                tileEntity.refreshInfo()
            }
            return arrayOf(tileEntity.index + 1)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun exportToCrystal(context: Context, args: Arguments): Array<Any>? {
            tileEntity.onNetworkEvent(null, 2)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun importFromCrystal(context: Context, args: Arguments): Array<Any>? {
            tileEntity.onNetworkEvent(null, 3)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(databaseAddress:string, databaseSlot:number):boolean")
        fun store(context: Context?, args: Arguments): Array<Any?> {
            if (tileEntity.pattern == null) {
                return NO_SELECTED_PATTERN_ERROR
            }
            val stack = tileEntity.patterns[tileEntity.index]
            val f: Function1<UpgradeDatabase, Array<Any>> =
                    object : AbstractFunction1<UpgradeDatabase, Array<Any>>() {
                        override fun apply(database: UpgradeDatabase): Array<Any> {
                            val toSlot: Int = ExtendedArguments.extendedArguments(args).checkSlot(database.data(), 4)
                            val nonEmpty = database.getStackInSlot(toSlot) != null
                            database.setStackInSlot(toSlot, stack)
                            return arrayOf(nonEmpty)
                        }
                    }
            return DatabaseAccess.withDatabase(node(), args.checkString(0), f)
        }

    }

    val NO_SELECTED_PATTERN_ERROR: Array<Any?> = arrayOf(null, "no selected pattern")

}