package oc.driver.ic2

import ic2.api.crops.Crops
import ic2.core.item.ItemCropSeed
import li.cil.oc.api.driver.Converter
import net.minecraft.item.ItemStack
import java.util.*

object ConvertCropSeedItem : Converter {
    override fun convert(value: Any, output: MutableMap<Any, Any>) {
        (value as? ItemStack)?.let { stack ->
            val item = stack.item
            if (item is ItemCropSeed && stack.tagCompound != null) {
                val crop: MutableMap<String, Any> = HashMap()
                val cropCard = Crops.instance.getCropCard(stack)
                val scanLevel = ItemCropSeed.getScannedFromStack(stack).toInt()
                crop["scanLevel"] = scanLevel
                if (scanLevel == 0) {
                    crop["name"] = "unknown"
                } else if (scanLevel > 0 && cropCard != null) {
                    crop["name"] = cropCard.owner() + ":" + cropCard.name()
                    if (scanLevel >= 2) {
                        crop["tier"] = cropCard.tier()
                        crop["discoveredBy"] = cropCard.discoveredBy()
                    }
                    if (scanLevel >= 3) {
                        crop["attributes"] = cropCard.attributes()
                    }
                    if (scanLevel >= 4) {
                        val stat: MutableMap<String, Any> = HashMap()
                        stat["growth"] = ItemCropSeed.getGrowthFromStack(stack)
                        stat["gain"] = ItemCropSeed.getGainFromStack(stack)
                        stat["resistance"] = ItemCropSeed.getResistanceFromStack(stack)
                        crop["stat"] = stat
                    }
                } else {
                    crop["name"] = "invalid"
                }
                output["crop"] = crop
            }
        }
    }
}