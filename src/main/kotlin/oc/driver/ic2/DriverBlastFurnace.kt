package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityBlastFurnace
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverBlastFurnace : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityBlastFurnace::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityBlastFurnace)

    class Environment(tileEntity: TileEntityBlastFurnace) : IMachineWorkProgress<Int>, IMachineActive, IMachineHeat,
            ManagedTileEntityEnvironment<TileEntityBlastFurnace>(tileEntity, "ic2_blast_furnace") {

        override val isActive: Boolean
            get() = tileEntity.active

        override val workProgress: Int
            get() = tileEntity.progress

        override val workMaxProgress: Int
            get() = tileEntity.maxprogress

        override val progress: Double
            get() = tileEntity.progress.toDouble() / tileEntity.maxprogress

        override val heat: Int
            get() = tileEntity.heat

        override val maxHeat: Int
            get() = TileEntityBlastFurnace.maxHeat

    }

}