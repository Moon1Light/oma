package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityCanner
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverCanningMachine : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityCanner::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityCanner)

    class Environment internal constructor(tileEntity: TileEntityCanner) :
            ManagedTileEntityEnvironment<TileEntityCanner>(tileEntity, "ic2_canning_machine") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function() -- Switch tanks.")
        fun switchTanks(context: Context, args: Arguments): Array<Any>? {
            tileEntity.onNetworkEvent(null, 1)
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the mode.")
        fun getMode(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.mode.ordinal)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(number) -- Set the mode.")
        fun setMode(context: Context, args: Arguments): Array<Any>? {
            val mode = args.checkInteger(0)
            require(mode in 0..3) { "invalid mode" }
            tileEntity.mode = TileEntityCanner.Mode.values()[mode]
            return null
        }

        companion object {
            private val modes = mapOf(
                    "bottleSolid" to 0,
                    "emptyLiquid" to 1,
                    "bottleLiquid" to 2,
                    "enrichLiquid" to 3
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table -- Returns available modes.")
        fun modes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(modes)
        }

    }

}