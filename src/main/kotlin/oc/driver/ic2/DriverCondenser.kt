package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityCondenser
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverCondenser : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityCondenser::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityCondenser)

    class Environment(tileEntity: TileEntityCondenser) : IMachineWorkProgress<Int>,
            ManagedTileEntityEnvironment<TileEntityCondenser>(tileEntity, "condenser") {

        override val workProgress: Int
            get() = tileEntity.progress

        override val workMaxProgress: Int
            get() = tileEntity.maxprogress

        override val progress: Double
            get() = tileEntity.progress.toDouble() / tileEntity.maxprogress

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getVents(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.vents)
        }

    }

}