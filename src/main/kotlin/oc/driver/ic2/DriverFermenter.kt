package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityFermenter
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment
import oc.util.Reflection.getInternal

object DriverFermenter : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityFermenter::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityFermenter)

    class Environment(tileEntity: TileEntityFermenter) : IMachineActive,
            ManagedTileEntityEnvironment<TileEntityFermenter>(tileEntity, "fermenter") {

        override val isActive: Boolean
            get() = tileEntity.active

        // сколько биомассы будет ферментировано за цикл
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getNeedAmountBiomassPerRun(context: Context, args: Arguments): Array<Any> {
            return arrayOf(getInternal(tileEntity, "needamountbio_run") as Int)
        }

        // сколько биогаза будет получено за цикл
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getOutputAmountBiogasPerRun(context: Context, args: Arguments): Array<Any> {
            return arrayOf(getInternal(tileEntity, "outputamountgas_run") as Int)
        }

        private val needFermentedBiomassPerFertilizer: Int
            get() = getInternal(tileEntity, "maxprogress") as Int

        // Сколько биомассы необходимо ферментировать для получения удобрения
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getNeedFermentedBiomassPerFertilizer(context: Context, args: Arguments): Array<Any> {
            return arrayOf(needFermentedBiomassPerFertilizer)
        }

        // Сколько биомассы было ферментировано
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getFermentedBiomass(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.progress.toDouble())
        }

        // текуший нагрев
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(getInternal(tileEntity, "heatbuffer") as Int)
        }

        // температура до которой нужно нагреть биомассу для выполнения ферментации
        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getNeedHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(getInternal(tileEntity, "maxheatbuffer") as Int)
        }

    }

}