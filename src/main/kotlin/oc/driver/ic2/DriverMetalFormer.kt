package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityMetalFormer
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverMetalFormer : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityMetalFormer::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityMetalFormer)

    class Environment internal constructor(tileEntity: TileEntityMetalFormer) :
            ManagedTileEntityEnvironment<TileEntityMetalFormer>(tileEntity, "ic2_metal_former") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Returns the mode.")
        fun getMode(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.mode)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(number) -- Sets the mode.")
        fun setMode(context: Context, args: Arguments): Array<Any>? {
            val mode = args.checkInteger(0)
            require(mode in 0..2) { "invalid mode" }
            tileEntity.mode = mode
            return null
        }

        companion object {
            val modes = mapOf(
                    "extruding" to 0,
                    "rolling" to 1,
                    "cutting" to 2
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table -- Returns available modes.")
        fun modes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(modes)
        }

    }

}