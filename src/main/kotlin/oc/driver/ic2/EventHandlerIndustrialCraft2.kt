package oc.driver.ic2

import cpw.mods.fml.common.eventhandler.SubscribeEvent
import ic2.api.crops.ICropTile
import ic2.core.crop.TileEntityCrop
import li.cil.oc.api.event.GeolyzerEvent.Analyze
import java.util.*

class EventHandlerIndustrialCraft2 {
    @SubscribeEvent
    fun onGeolyzerAnalyze(e: Analyze) {
        val world = e.host.world()
        val tile = world.getTileEntity(e.x, e.y, e.z)
        if (tile is ICropTile) {
            val cropTile = tile as ICropTile
            val cropCard = cropTile.crop
            if (cropCard != null) {
                val data: MutableMap<String, Any> = HashMap()
                // basic information
                data["name"] = cropCard.owner() + ":" + cropCard.name()
                data["discoveredBy"] = cropCard.discoveredBy()
                data["size"] = cropTile.size
                data["maxSize"] = cropCard.maxSize()
                data["nutrientStorage"] = cropTile.nutrientStorage
                data["waterStorage"] = cropTile.hydrationStorage
                data["weedExStorage"] = cropTile.weedExStorage
                if (cropTile is TileEntityCrop) {
                    data["growthPoint"] = cropTile.growthPoints
                } else {
                    data["growthPoint"] = 0
                }
                data["growthDuration"] = cropCard.growthDuration(cropTile)
                // more information
                data["lightLevel"] = cropTile.lightLevel
                data["optimalHarvestSize"] = cropCard.getOptimalHavestSize(cropTile)
                data["isWeed"] = cropCard.isWeed(cropTile)
                data["canGrow"] = cropCard.canGrow(cropTile)
                data["canCross"] = cropCard.canCross(cropTile)
                data["canBeHarvested"] = cropCard.canBeHarvested(cropTile)
                data["humidity"] = cropTile.humidity
                data["nutrients"] = cropTile.nutrients
                data["airQuality"] = cropTile.airQuality
                e.data["crop"] = data
                e.data["growth"] = cropTile.size / cropCard.maxSize().toDouble()
            }
        }
    }
}