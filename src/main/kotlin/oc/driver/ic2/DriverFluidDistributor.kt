package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityFluidDistributor
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.ManagedEnvironment
import li.cil.oc.api.prefab.DriverSidedTileEntity
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverFluidDistributor : DriverSidedTileEntity() {
    override fun getTileEntityClass(): Class<*> = TileEntityFluidDistributor::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
        Environment(world.getTileEntity(x, y, z) as TileEntityFluidDistributor)

    class Environment internal constructor(tileEntity: TileEntityFluidDistributor) :
        ManagedTileEntityEnvironment<TileEntityFluidDistributor>(tileEntity, "ic2_fluid_distributor") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function([mode: number])")
        fun setMode(context: Context, args: Arguments): Array<Any>? {
            val mode = args.checkInteger(0)
            require(mode in 0..1) { "invalid mode" }
            if (mode == 0 && tileEntity.active || mode == 1 && !tileEntity.active) {
                tileEntity.active = !tileEntity.active
            }
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(): number")
        fun getMode(context: Context, args: Arguments): Array<Any> {
            val mode = if (tileEntity.active) 1 else 0
            return arrayOf(mode)
        }

        companion object {
            private val modes = mapOf(
                "distribute" to 0,
                "concentrate" to 1
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table -- Get the modes.")
        fun modes(context: Context, args: Arguments): Array<Any> {
            return arrayOf(modes)
        }
    }
}