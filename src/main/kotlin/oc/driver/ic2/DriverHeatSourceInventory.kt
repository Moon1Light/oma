package oc.driver.ic2

import ic2.core.block.TileEntityHeatSourceInventory
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverHeatSourceInventory : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityHeatSourceInventory::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityHeatSourceInventory)

    class Environment(tileEntity: TileEntityHeatSourceInventory) :
            ManagedTileEntityEnvironment<TileEntityHeatSourceInventory>(tileEntity, "heat_source") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getTransmitHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.gettransmitHeat())
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getMaxHeatEmittedPerTick(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.maxHeatEmittedPerTick)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getHeat(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.heatBuffer)
        }

    }

}