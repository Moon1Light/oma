package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityScanner
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverScanner : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityScanner::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityScanner)

    class Environment internal constructor(tileEntity: TileEntityScanner) :
            ManagedTileEntityEnvironment<TileEntityScanner>(tileEntity, "ic2_scanner") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun record(context: Context, args: Arguments): Array<Any>? {
            if (tileEntity.isDone) {
                tileEntity.onNetworkEvent(null, 1)
            }
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function()")
        fun reset(context: Context, args: Arguments): Array<Any>? {
            tileEntity.reset()
            return null
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- Get the machine's progress.")
        fun getProgress(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.progress.toDouble() / 3300)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean")
        fun isDone(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.isDone)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getState(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.state.ordinal)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getPatternUu(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.patternUu)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean")
        fun hasPatternStorage(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.patternStorage != null)
        }

        companion object {
            private val states = mapOf(
                    "idle" to 0,
                    "scanning" to 1,
                    "completed" to 2,
                    "failed" to 3,
                    "noStorage" to 4,
                    "noEnergy" to 5,
                    "transferError" to 6,
                    "alreadyRecorded" to 7
            )
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():table")
        fun states(context: Context, args: Arguments): Array<Any> {
            return arrayOf(states)
        }

    }

}