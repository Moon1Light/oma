package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityIronFurnace
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverIronFurnace : DriverSidedTileEntity() {

    override fun getTileEntityClass(): Class<*> = TileEntityIronFurnace::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityIronFurnace)

    class Environment internal constructor(tileEntity: TileEntityIronFurnace) : IMachineWorkProgress<Int>,
            ManagedTileEntityEnvironment<TileEntityIronFurnace>(tileEntity, "ic2_iron_furnace") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- The number of ticks that the furnace will keep burning " +
                "from the last consumed fuel.")
        fun getBurnTime(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.fuel)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- The number of ticks that the current item has been cooking for.")
        fun getCookTime(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.progress)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number -- The number of ticks that the currently burning fuel lasts in total.")
        fun getCurrentItemBurnTime(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.maxFuel)
        }

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():boolean -- Get whether the furnace is currently active.")
        fun isBurning(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.isBurning)
        }

        override val workProgress: Int
            get() = tileEntity.progress.toInt()

        override val workMaxProgress: Int
            get() = tileEntity.operationLength.toInt()

        override val progress: Double
            get() = tileEntity.progress.toDouble() / tileEntity.operationLength


        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function():number")
        fun getOperationLength(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.operationLength)
        }

    }

}