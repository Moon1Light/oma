package oc.driver.ic2

import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context

interface IMachineWorkProgress<T> {

    val workProgress: T
    val workMaxProgress: T
    val progress: Double

    @Callback(doc = "function():boolean -- Returns true if the machine currently has work to do.")
    fun hasWork(context: Context, args: Arguments): Array<Any> {
        return arrayOf(workProgress != 0)
    }

    @Callback(doc = "function():number -- Returns the current progress of this machine.")
    fun getWorkProgress(context: Context, args: Arguments): Array<in Any> {
        return arrayOf(workProgress)
    }

    @Callback(doc = "function():number -- Returns the max progress of this machine.")
    fun getWorkMaxProgress(context: Context, args: Arguments): Array<in Any> {
        return arrayOf(workMaxProgress)
    }

    @Callback(doc = "function():number -- Returns the machine's progress.")
    fun getProgress(context: Context, args: Arguments): Array<Any> {
        return arrayOf(progress)
    }

}

interface IMachineActive {

    val isActive: Boolean

    @Callback(doc = "function():boolean -- Returns whether the machine is currently active.")
    fun isActive(context: Context, args: Arguments): Array<Any> {
        return arrayOf(isActive)
    }

}

interface IMachineHeat {

    val heat: Int
    val maxHeat: Int

    @Callback(doc = "function():number -- Returns the machine's heat.")
    fun getHeat(context: Context, args: Arguments): Array<Any> {
        return arrayOf(heat)
    }

    @Callback(doc = "function():number -- Returns the machine's max heat.")
    fun getMaxHeat(context: Context, args: Arguments): Array<Any> {
        return arrayOf(maxHeat)
    }

}

interface IMachine {

    val energyConsume: Int
    val operationsPerTick: Int
    val operationLength: Int

    @Callback(doc = "function():number -- Returns the amount of energy consumed by the machine per tick.")
    fun getConsumedEnergy(context: Context, args: Arguments): Array<Any> {
        return arrayOf(energyConsume)
    }

    @Callback(doc = "function():number -- Returns the number of operations per tick.")
    fun getOperationsPerTick(context: Context, args: Arguments): Array<Any> {
        return arrayOf(operationsPerTick)
    }

    @Callback(doc = "function():number -- Returns the number of ticks per operation.")
    fun getOperationLength(context: Context, args: Arguments): Array<Any> {
        return arrayOf(operationLength)
    }

}