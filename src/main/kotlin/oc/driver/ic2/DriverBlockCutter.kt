package oc.driver.ic2

import ic2.core.block.machine.tileentity.TileEntityBlockCutter
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.prefab.DriverSidedTileEntity
import li.cil.oc.api.prefab.ManagedEnvironment
import net.minecraft.world.World
import net.minecraftforge.common.util.ForgeDirection
import oc.driver.ManagedTileEntityEnvironment

object DriverBlockCutter : DriverSidedTileEntity() {
    override fun getTileEntityClass(): Class<*> = TileEntityBlockCutter::class.java

    override fun createEnvironment(world: World, x: Int, y: Int, z: Int, side: ForgeDirection): ManagedEnvironment =
            Environment(world.getTileEntity(x, y, z) as TileEntityBlockCutter)

    class Environment internal constructor(tileEntity: TileEntityBlockCutter) :
            ManagedTileEntityEnvironment<TileEntityBlockCutter>(tileEntity, "ic2_block_cutter") {

        @Suppress("UNUSED_PARAMETER")
        @Callback(doc = "function(): boolean")
        fun isBladeTooWeak(context: Context, args: Arguments): Array<Any> {
            return arrayOf(tileEntity.isbladetoweak())
        }
    }
}