package oc.driver

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import li.cil.oc.api.Network
import li.cil.oc.api.driver.DeviceInfo
import li.cil.oc.api.driver.DeviceInfo.DeviceAttribute
import li.cil.oc.api.driver.DeviceInfo.DeviceClass
import li.cil.oc.api.internal.Agent
import li.cil.oc.api.machine.Arguments
import li.cil.oc.api.machine.Callback
import li.cil.oc.api.machine.Context
import li.cil.oc.api.network.EnvironmentHost
import li.cil.oc.api.network.Message
import li.cil.oc.api.network.Node
import li.cil.oc.api.network.Visibility
import li.cil.oc.util.ExtendedArguments
import li.cil.oc.util.InventoryUtils
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.common.util.ForgeDirection
import net.minecraftforge.fluids.FluidContainerRegistry
import net.minecraftforge.fluids.IFluidTank
import oc.settings
import oc.util.OCUtils.Vendors
import scala.Option
import kotlin.math.min

class UpgradeEnderlink(host: EnvironmentHost) : BaseManagedEnvironment(), DeviceInfo {
    private var name: String? = null
    private var isPublic = false
    private var remoteName: String? = null
    private var remoteOwner: String? = null
    private val agent = host as Agent

    override val node = Network.newNode(this, Visibility.Network)
            .withComponent("enderlink", Visibility.Neighbors).withConnector().create()!!

    private val device by lazy {
        mapOf(DeviceAttribute.Class to DeviceClass.Generic,
                DeviceAttribute.Description to "Enderlink",
                DeviceAttribute.Vendor to Vendors.Scrag,
                DeviceAttribute.Product to "Enderlink v1"
        )
    }

    override fun getDeviceInfo(): Map<String, String> {
        return device
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([name:string[,owner:string]]):string,string -- Returns the currently remote enderlink; sets the remote enderlink if specified.")
    fun remote(context: Context, arguments: Arguments): Array<Any?> {
        if (arguments.count() > 0) {
            val name = if (arguments.checkAny(0) == null) null else arguments.checkString(0)
            val owner = if (name == null) null else arguments.optString(1, ownerName)
            remoteName = name
            remoteOwner = owner
        }
        return arrayOf(remoteName, remoteOwner)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Returns whether the remote enderlink is available")
    fun remoteAvailable(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(remote != null)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([count:number=64]):number[,string] -- Transfers some items from selected slot into the remote enderlink.")
    fun transferItem(context: Context, arguments: Arguments): Array<Any> {
        val count = ExtendedArguments.extendedArguments(arguments).optItemCount(0, 64)
        val remote = remote ?: return arrayOf(0, "unavailable")
        if (!node.tryChangeBuffer(-settings.enderlink.transferCost)) {
            return arrayOf(0, "not enough energy")
        }
        val inventory = agent.mainInventory()
        val stack = inventory.getStackInSlot(agent.selectedSlot())
        if (remote !== this && stack != null && stack.stackSize > 0 && count > 0) {
            val stackSize = stack.stackSize
            if (remote.insertItem(stack, count.coerceAtMost(stackSize))) {
                if (stack.stackSize == 0) {
                    inventory.setInventorySlotContents(agent.selectedSlot(), null)
                } else {
                    inventory.markDirty()
                }
                return arrayOf(stackSize - stack.stackSize)
            }
        }
        return arrayOf(0)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function([amount:number=1000]):number[,string] -- Transfers some fluids from selected tank into the remote enderlink.")
    fun transferFluid(context: Context, arguments: Arguments): Array<Any> {
        val amount: Int = ExtendedArguments.extendedArguments(arguments).optFluidCount(0, FluidContainerRegistry.BUCKET_VOLUME)
        val remote = remote ?: return arrayOf(0, "unavailable")
        if (!node.tryChangeBuffer(-settings.enderlink.transferCost)) {
            return arrayOf(0, "not enough energy")
        }
        val tank = agent.tank().getFluidTank(agent.selectedTank())
        return if (remote !== this && tank != null) {
            arrayOf(remote.insertFluid(tank, min(amount, tank.fluidAmount)))
        } else arrayOf(0)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():string -- Returns the input channel name.")
    fun getName(context: Context, arguments: Arguments): Array<Any?> {
        return arrayOf(name)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(): -- Returns whether the input channel is public")
    fun isPublic(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(isPublic)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Returns whether the input channel is open")
    fun isOpen(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(isOpen)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():string -- Returns the input channel owner (The owner is the player who installed the robot/drone).")
    fun getOwner(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(ownerName)
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function(name:string[,isPublic:boolean=false]):boolean -- Opens the input channel.")
    fun open(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(open(arguments.checkString(0), arguments.optBoolean(1, false)))
    }

    @Suppress("UNUSED_PARAMETER")
    @Callback(doc = "function():boolean -- Closes the input channel")
    fun close(context: Context, arguments: Arguments): Array<Any> {
        return arrayOf(close())
    }

    override fun load(nbt: NBTTagCompound) {
        super.load(nbt)
        if (nbt.hasKey("name")) {
            name = nbt.getString("name")
        }
        if (nbt.hasKey("remoteName") && nbt.hasKey("remoteOwner")) {
            remoteName = nbt.getString("remoteName")
            remoteOwner = nbt.getString("remoteOwner")
        }
    }

    override fun onConnect(node: Node) {
        if (node === this.node && isOpen) {
            reg()
        }
    }

    override fun onDisconnect(node: Node) {
        if (node === this.node && isOpen) {
            unreg()
        }
    }

    override fun save(nbt: NBTTagCompound) {
        super.save(nbt)
        if (name != null) {
            nbt.setString("name", name)
        } else {
            nbt.removeTag("name")
        }
        if (remoteName != null) {
            nbt.setString("remoteName", remoteName)
            nbt.setString("remoteOwner", remoteOwner)
        } else {
            nbt.removeTag("remoteName")
            nbt.removeTag("remoteOwner")
        }
    }

    override fun onMessage(message: Message) {
        super.onMessage(message)
        if (message.name() == "computer.stopped") {
            close()
            remoteName = null
            remoteOwner = null
        }
    }

    private fun open(name: String, isPublic: Boolean): Boolean {
        if (isOpen) {
            return false
        }
        this.name = name
        this.isPublic = isPublic
        reg()
        return true
    }

    private fun close(): Boolean {
        if (isOpen) {
            unreg()
            name = null
            isPublic = false
            return true
        }
        return false
    }

    private fun insertItem(stack: ItemStack, count: Int): Boolean {
        val inventory = agent.mainInventory()
        return InventoryUtils.insertIntoInventorySlot(
                stack, inventory, Option.apply(ForgeDirection.UP), agent.selectedSlot(), count, false)
    }

    private fun insertFluid(source: IFluidTank, amount: Int): Int {
        val sink = agent.tank().getFluidTank(agent.selectedTank())
        val drained = source.drain(amount, false)
        val filled = sink.fill(drained, false)
        return sink.fill(source.drain(filled, true), true)
    }

    private fun reg() {
        enderlinks!!.put(key, this)
    }

    private fun unreg() {
        enderlinks!!.remove(key, this)
    }

    private val isOpen: Boolean = name != null

    private val ownerName: String = agent.ownerName()

    private val key: String = ownerName + name

    private val remote: UpgradeEnderlink?
        get() {
            if (remoteName == null) {
                return null
            }
            val remote = enderlinks!![remoteOwner + remoteName].stream().findAny().orElse(null)
            return if (remote != null && (remote.isPublic || remote.ownerName == ownerName)) {
                remote
            } else null
        }

    companion object {
        private var enderlinks: Multimap<String, UpgradeEnderlink>? = null

        fun init() {
            enderlinks = HashMultimap.create()
        }

        fun cleanup() {
            enderlinks!!.clear()
            enderlinks = null
        }
    }

}