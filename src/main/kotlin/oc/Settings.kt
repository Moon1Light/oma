package oc

import net.minecraftforge.common.config.Configuration
import java.io.File

class Settings {

    val oc = li.cil.oc.Settings.get()!!

    class SolarPanel internal constructor(name: String, tier: Int,
                                          dayPerTick: Int, nightPerTick: Int,
                                          chargeTool: Boolean) : StuffWithTier(name, tier) {
        val dayPerTick = getInt("day_per_tick", "Day power generation (per tick).",
                dayPerTick,
                0)
        val nightPerTick = getInt("night_per_tick", "Night power generation (per tick).",
                nightPerTick,
                0)
        val chargeTool = getBoolean("charge_tool", "Allow the solar panel to charge tools.", chargeTool)
    }

    val advancedSolarPanel = SolarPanel("advanced_solar_panel", 2, 2, 1, false)
    val hybridSolarPanel = SolarPanel("hybrid_solar_panel", 3, 16, 2, false)
    val ultimateSolarPanel = SolarPanel("ultimate_solar_panel", 3, 128, 16, true)
    val quantumSolarPanel = SolarPanel("quantum_solar_panel", 3, 1024, 128, true)

    val ironInventory = StuffWithTier("iron_inventory", 2)
    val diamondInventory = StuffWithTier("diamond_inventory", 3)

    class Geolyzer : StuffWithTier("advanced_geolyzer", 3) {
        val scanRadius = getInt("scan_radius", "The range, in blocks, in which the Geolyzer can scan blocks.",
                15,
                0)
        val scanCost = getDouble("scan_cost", "The range, in blocks, in which the Geolyzer can scan blocks.",
                10.0,
                0.0)
    }

    val geolyzer = Geolyzer()

    class Tesseract : StuffWithTier("tesseract", 3) {
        val energyPerTickInOneWorld = getDouble("one_per_tick",
                "How much energy does the tesseract consume per tick when linked inventory is in the same world.",
                10.0, 0.0)
        val energyPerTickInOtherWorld = getDouble("other_per_tick",
                "How much energy does the tesseract consume per tick when linked inventory is in the another world.",
                20.0, 0.0)
        val doChunkLoading = getBoolean("do_chunk_loading",
                "Allow the tesseract to load the chunk in which the linked inventory is located.",
                true)
    }

    val tesseract = Tesseract()

    val navigation = StuffWithTier("advanced_navigation", 1)

    val itemCharger = StuffWithTier("item_charger", 2)

    class Enderlink : StuffWithTier("enderlink", 2) {
        val transferCost = getDouble("transfer_cost",
                "How much energy is consumed when the Enderlink transfers item/fluid.",
                100.0, 0.0)
    }

    val enderlink = Enderlink()

    class Cropnalyzer : StuffWithTier("cropnalyzer", 2) {
        private val defaultScanCost = intArrayOf(0, 40, 360, 3600)

        val scanCost = getIntArray("scan_cost",
                "[Default: ${defaultScanCost[0]} ${defaultScanCost[1]} ${defaultScanCost[2]} ${defaultScanCost[3]}]",
                defaultScanCost)

        private val _allowGeolyzerAnalyze = getBoolean("allow_geolyzer_analyze", "Allow geolyzer to get a crop data",
                true)

        private val _allowItemStackInspection = getBoolean("allow_item_stack_inspection",
                "Allow inventory controller upgrade to get a crop data.",
                true)

        val allowGeolyzerAnalyze by lazy { Mods.industrialCraft2() && _allowGeolyzerAnalyze }
        val allowItemStackInspection by lazy { Mods.industrialCraft2() && _allowItemStackInspection }
        override val isEnabled: Boolean by lazy { super.isEnabled && Mods.industrialCraft2() }
    }

    val cropnalyzer = Cropnalyzer()

    class UpgradeRadar : StuffWithTier("upgrade_radar", 2) {
        val maxRange = getInt("max_range", "The maximum range of the Radar.",
                15,
                1, 256)

        val entityScanEnergy = getDouble("entity_scan_energy",
                "How much energy is consumed when the Radar scans entities (Per block).",
                45.0, 0.0)

        val playerScanEnergy = getDouble("player_scan_energy",
                "How much energy is consumed when the Radar scans players (Per block).",
                30.0, 0.0)

        val mobScanEnergy = getDouble("mob_scan_energy",
                "How much energy is consumed when the Radar scans mobs (Per block).",
                30.0, 0.0)

        val itemScanEnergy = getDouble("item_scan_energy",
                "How much energy is consumed when the Radar scans items (Per block).",
                60.0, 0.0)

        val delay: Double = getDouble("delay", "Time in seconds to pause execution after scanning.",
                0.5)
    }

    val upgradeRadar = UpgradeRadar()

    val containerAdapter = StuffWithTier("upgrade_container_adapter", 3)

    class Scanner : FeatureWithRecipe("scanner") {
        val range = getInt("range", "", 20, minValue = 0) //TODO: description
        val energyBufferSize = getInt("energy_buffer_size", "", 40000, minValue = 0) //TODO: description
        val scanCost = getInt("scan_cost", "", 4000, minValue = 0) //TODO: description
    }

    class TeslaUpgrade : StuffWithTier("upgrade_tesla", 3) {
        val energyPerAttack = getInt("energy_per_attack", "", 10000, minValue = 0).toDouble() //TODO: description
        val maxRange = getInt("maxRange", "", 10, minValue = 0, renamedFrom = "range") //TODO: description
        val damage = getInt("damage", "", 10, minValue = 0) //TODO: description
        val coolingTicks = getInt("cooling_ticks", "", 100, minValue = 0) //TODO: description
    }

    val upgradeTesla = TeslaUpgrade()

    val scanner = Scanner()

    class ChatBox : FeatureWithRecipe("chatbox") {
        val maxMessageLength = getInt("max_message_length", "", 128,
                0)
        val maxMessageRadius = getInt("max_message_radius", "", 16,
                0)
        val isLogging = getBoolean("is_logging", "Logging to console, where the location of the chatbox.",
                true)
        val messagePattern = getString("message_pattern", "Chatbox say message pattern.", "\${message}")
        val commandPrefix = getString("command_prefix", "Chatbox command prefix.", "#")
    }

    val chatbox = ChatBox()

    val creativeChatbox = Feature("creative_chatbox")

    val worldInterface = Feature("world_interface")

    class BlockAntenna(name: String) : Feature(name) {
        val messageSpeed: Float = getDouble("messageSpeed", "", 100.0).toFloat()
        val minDistance: Float = getDouble("minDistance", "", 1000.0).toFloat()
    }

    val antenna = BlockAntenna("antenna")

    class BlockRadar : FeatureWithRecipe("block_radar") {
        val maxRange = getInt("max_range", "The maximum range of the Radar.", 64,
                0)
        val scanEnergy = getDouble("scan_energy", "How much energy is consumed when the Radar scans (per block).",
                1.0)
        val maxAngle = getInt("max_angle", "", 45, 0, 180)
        val maxHeight = getInt("max_height", "", 64, 1, 256)
        val delay: Double = getDouble("delay", "Time in seconds to pause execution after scanning.",
                0.5)
    }

    val blockRadar = BlockRadar()

    class BlockSolarPanel : FeatureWithRecipe("block_solar_panel") {
        val defaultPort = getInt("default_port", "", 443, 0, 0xffff)
        val maxPitch = getInt("max_pitch", "", 40, 0, 40)
        val energyPerTick = getInt("energy_per_tick", "", 256, minValue = 0)
        override val isEnabled: Boolean by lazy { super.isEnabled && Mods.industrialCraft2() }
    }

    val blockSolarPanel = BlockSolarPanel()

    class FurnaceGenerator : FeatureWithRecipe("furnace_generator") {
        val efficiency = getDouble("efficiency", "", 1.0, minValue = 0.0)
    }

    val furnaceGenerator = FurnaceGenerator()

    val integrationIndustrialCraft2 = cfg.getBoolean("IndustrialCraft2", "integration", true, "")

    class PetRenderer : Feature("pet_renderer") {
        val entitledPlayers = getStringArray("entitled_players", "Format: uuid r g b\nExample: 0e6d26bf-76f9-3201-9031-608614f7139b 0.2 0.53 0.66", emptyArray())
    }

    val petRenderer = PetRenderer()

    class Ic2Drivers : Feature("ic2_drivers") {
        val standardMachine = getBoolean("standard_machine", "", true)
        val electricMachine = getBoolean("electric_machine", "", true)
        val replicator = getBoolean("replicator", "", true)
        val advancedMiner = getBoolean("advanced_miner", "", true)
        val blastFurnace = getBoolean("blast_furnace", "", true)
        val canningMachine = getBoolean("canning_machine", "", true)
        val condenser = getBoolean("condenser", "", true)
        val liquidHeatExchanger = getBoolean("liquid_heat_exchanger", "", true)
        val fermenter = getBoolean("fermenter", "", true)
        val fluidRegulator = getBoolean("fluid_regulator", "", true)
        val heatSourceInventory = getBoolean("heat_source_inventory", "", true)
        val inductionFurnace = getBoolean("induction_furnace", "", true)
        val ironFurnace = getBoolean("iron_furnace", "", true)
        val metalFormer = getBoolean("metal_former", "", true)
        val pump = getBoolean("pump", "", true)
        val steamGenerator = getBoolean("steam_generator", "", true)
        val thermalCentrifuge = getBoolean("thermal_centrifuge", "", true)
        val lathe = getBoolean("lathe", "", true)
        val blockCutter = getBoolean("block_cutter", "", true)
        val fluidDistributor = getBoolean("fluid_distributor", "", true)
    }

    val ic2Drivers = Ic2Drivers()

    val industrialMicrocontrollerCase = FeatureWithRecipe("industrial_microcontroller_case")
    val industrialDroneCase = FeatureWithRecipe("industrial_drone_case")
    val industrialRobotCase = FeatureWithRecipe("industrial_robot_case")

    open class Feature internal constructor(name: String, experimental: Boolean = false) {
        private val enabled: Boolean = if (experimental) {
            cfg.getCategory("enabled").remove(name)
            cfg.getBoolean(name, "experimental", false, "")
        } else {
            cfg.getCategory("experimental").remove(name)
            cfg.getBoolean(name, "enabled", true, "")
        }
        open val isEnabled = enabled
        private val categoryName = name

        private fun <T> removeOld(oldName: String, get: () -> T): T? {
            if (cfg.hasCategory(categoryName) && cfg.getCategory(categoryName).containsKey(oldName)) {
                val v = get()
                cfg.getCategory(categoryName).remove(oldName)
                return v
            }
            return null
        }

        protected fun getInt(name: String, comment: String, defaultValue: Int,
                             minValue: Int = Int.MIN_VALUE, maxValue: Int = Int.MAX_VALUE,
                             renamedFrom: String? = null): Int {
            val v = renamedFrom?.let {
                removeOld(renamedFrom) { getInt(renamedFrom, comment, defaultValue, minValue, maxValue) }
            }
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            cfgProp.setMinValue(minValue)
            cfgProp.setMaxValue(maxValue)
            cfgProp.comment = "$comment [range: $minValue ~ $maxValue, default: $defaultValue]"
            v?.let { cfgProp.setValue(v) }
            return cfgProp.getInt(defaultValue).coerceAtLeast(minValue).coerceAtMost(maxValue)
        }

        protected fun getDouble(name: String, comment: String, defaultValue: Double,
                                minValue: Double = Double.MIN_VALUE, maxValue: Double = Double.MAX_VALUE): Double {
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            cfgProp.setMinValue(minValue)
            cfgProp.setMaxValue(maxValue)
            cfgProp.comment += "[default: $defaultValue]"
            return cfgProp.getDouble(defaultValue).coerceAtLeast(minValue).coerceAtMost(maxValue)
        }

        protected fun getBoolean(name: String, comment: String, defaultValue: Boolean): Boolean {
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            return cfgProp.getBoolean(defaultValue)
        }

        protected fun getString(name: String, comment: String, defaultValue: String): String {
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            return cfgProp.string
        }

        protected fun getIntArray(name: String, comment: String, defaultValue: IntArray): IntArray {
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            return cfgProp.intList
        }

        protected fun getStringArray(name: String, comment: String, defaultValue: Array<String>): Array<String> {
            val cfgProp = cfg.get(categoryName, name, defaultValue, comment)
            return cfgProp.stringList
        }
    }

    open class FeatureWithRecipe internal constructor(name: String) : Feature(name) {
        private val recipe = cfg.getBoolean(name, "recipe", true, "")
        open fun hasRecipe(): Boolean {
            return isEnabled && recipe
        }
    }

    open class StuffWithTier internal constructor(name: String, tier: Int) : FeatureWithRecipe(name) {
        val tier = cfg.getInt("tier", name, tier, 1, 3, "")
    }

    companion object {
        private lateinit var cfg: Configuration
        private lateinit var settings: Settings
        fun get() = settings

        fun init(file: File) {
            cfg = Configuration(file)
            cfg.load()
            settings = Settings()
            cfg.save()
        }
    }

}

val settings by lazy {
    Settings.get()
}